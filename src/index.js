import React from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View
} from 'react-native';
import { Provider } from 'react-redux';
import App from './app';
import createStore from './createStore';

export default class eTheraApp extends React.Component {

    render() {
        const store = createStore();
        return (
            <Provider store={store}>
              <App />
            </Provider>
        );
    }
}

AppRegistry.registerComponent('eTheraApp', () => eTheraApp);
