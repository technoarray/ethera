import React, { Component } from 'react';
import {AsyncStorage,StyleSheet,Text,View,Image,TouchableOpacity,Platform,ScrollView,Alert,Dimensions,Picker} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions } from 'react-navigation'
import LinearGradient from 'react-native-linear-gradient';
import Header from '../common/signupHeader'
import * as commonFunctions from '../../utils/CommonFunctions'
import Swiper from 'react-native-swiper';
import FusionCharts from 'react-native-fusioncharts';
import Spinner from 'react-native-loading-spinner-overlay';
import RNPickerSelect from 'react-native-picker-select';

var Constant = require('../../api/ApiRules').Constant;
var WebServices = require('../../api/ApiRules').WebServices;
let _that

export default class ChartScreen extends Component {
    constructor (props) {
      super(props)
      this.state = {
      uid:'',
      title:'Oxygen Saturation',
      isVisible: false,
      Modal_Visibility: false,
      modeIndex: 0,
      modeValue: 'daily',
      type_item: undefined,
      items: [
          {
              label: 'Daily',
              value: 'daily',
          },
          {
              label: 'Weekly',
              value: 'weekly',
          },
          {
              label: 'Monthly',
              value: 'monthly',
          },
      ],
      type: 'line',
      width: '300',
      height: '300',
      dataFormat: 'json',
      dataSource: {
        chart: {
          caption: "",
          theme: 'fint'
        },
        data: [

        ]
      },
      dataSource2: {
        chart: {
          caption: "",
          theme: 'fint'
        },
        data: [

        ]
      },
      dataSource3: {
        chart: {
          caption: "",
          theme: 'fint'
        },
        data: [

        ]
      },
      events: {
        dataplotclick: (e, a) => {
          console.log(`You clicked on ${e.data.categoryLabel, a}`);
        }
      }
      },
      _that = this;

      this.libraryPath = Platform.select({
      // Specify fusioncharts.html file location
      ios: require('../../../assets/fusioncharts.html'),
      android: { uri: 'file:///android_asset/fusioncharts.html' }
      });

      }

  componentDidMount() {
    setTimeout(() => {
        this.setState({type_item: 'daily' });
    }, 1000);
    AsyncStorage.getItem('loggedIn').then((value) => {
        var loggedIn = JSON.parse(value);
        if (loggedIn) {
            AsyncStorage.getItem('UserData').then((UserData) => {
                const data = JSON.parse(UserData)
                //console.log(val)
                var uid=data.id;
                _that.setState({uid:  uid});
                _that.validationAndApiParameter('chartdata');
            })

        }
    })
  }

  validationAndApiParameter(apiname) {
      //console.log(apiname)
      //console.log('uid='+_that.state.uid)
      if(apiname == 'chartdata'){
        var data = {
            uid: this.state.uid
        };
          _that.setState({isVisible: true});
          this.postToApiCalling('POST', 'chartdata', Constant.URL_Chartdata,data);
      }
    }

    postToApiCalling(method, apiKey, apiUrl, data) {
      //console.log(apiUrl)
        new Promise(function(resolve, reject) {
            if (method == 'POST') {
                resolve(WebServices.callWebService(apiUrl, data));
            } else {
                resolve(WebServices.callWebService_GET(apiUrl, data));
            }
        }).then((jsonRes) => {
            //console.log(jsonRes)
              _that.setState({ isVisible: false })
            if ((!jsonRes) || (jsonRes.code == 0)) {
              setTimeout(() => {
                    Alert.alert(jsonRes.message);
                }, 200);
            } else {
                if (jsonRes.code == 1) {
                    _that.apiSuccessfullResponse(apiKey, jsonRes)
                }
            }
        }).catch((error) => {
            console.log("ERROR" + error);
            _that.setState({ isVisible: false })
            setTimeout(() => {
                //Alert.alert("Server Error");
            }, 200);
        })
    }

    apiSuccessfullResponse(apiKey, jsonRes) {
    //  console.log(jsonRes);
      if (apiKey == 'chartdata') {
        const jdata = jsonRes
        console.log(jsonRes)
        const { labels, datasets } = jdata.oxy_chart[this.state.modeIndex];
        // os
        const ds = { ...this.state.dataSource };
        ds.data = [];

        for (let i = 0; i < labels.length; i++) {
          const obj = {
            label: labels[i],
            value: datasets[0].data[i]
          };
          ds.data.push(obj);
        }

        // HR
        const { labels2, datasets2 } = jdata.heart_chart[this.state.modeIndex];
        const ds2 = { ...this.state.dataSource2 };
        ds2.data = [];

        for (let i = 0; i < labels2.length; i++) {
          const obj = {
            label: labels2[i],
            value: datasets2[0].data[i]
          };
          ds2.data.push(obj);
        }

        // TMP
        const { labels3, datasets3 } = jdata.temp_chart[this.state.modeIndex];
        const ds3 = { ...this.state.dataSource3};
        ds3.data = [];

        for (let i = 0; i < labels3.length; i++) {
          const obj = {
            label: labels3[i],
            value: datasets3[0].data[i]
          };
          ds3.data.push(obj);
        }


        this.setState({
          dataSource: ds,
          dataSource2: ds2,
          dataSource3: ds3
        });
      }
    }

  pickerbtn(){
    console.log("hello");
  }
  updateAlert = (visible) => {
        this.setState({Modal_Visibility: !this.state.Modal_Visibility});
  }

  onSwipe = (index) => {
   if(index==0){
      this.setState({title:'Oxygen Saturation'})
    }
    else if(index==1){
      this.setState({title:'Heart Rate'})
    }
    else if(index==2){
      this.setState({title:'Temperature'})
    }
  }


  render() {
    const headerProp = {
      title: this.state.title,
      screens: 'ChartScreen',
      qus_content:'',
      qus_audio:'https://houseofvirtruve.com/audio/policies.mp3'
    };

    return (
      <View style={styles.container}>
      <Header info={headerProp} navigation={_that.props.navigation} updateAlert={this.updateAlert}/>

      <Swiper style={[styles.wrapper,this.state.Modal_Visibility ? {backgroundColor: 'rgba(0,0,0,0.4)',zIndex:5} : '#ffffff']}
        showsButtons={true} onIndexChanged={this.onSwipe}>

        <View style={styles.slide1}>
              <View style={styles.titlebox}>
                <Text style={styles.content}>Please Rotate Your Device to Landscape Mode</Text>
              </View>
              <View style={styles.mainchart}>

              <View style={styles.pickerStyleView}>
              <RNPickerSelect
                  placeholder={{
                      label: 'Select a Type...',
                      value: null,
                      color: '#9EA0A4',
                  }}
                  items={this.state.items}
                  onValueChange={(value) => {
                    if(value=='daily'){index=0}
                    if(value=='weekly'){index=1}
                    if(value=='monthly'){index=2}
                       this.setState({
                           type_item: value,modeValue: value, modeIndex: index
                       });
                       console.log(this.state.modeIndex)
                       this.validationAndApiParameter('chartdata');
                     }}

                  style={{ ...pickerSelectStyles }}
                  value={this.state.type_item}
              />
              </View>

              <FusionCharts
                type={this.state.type}
                width={this.state.width}
                height={this.state.height}
                dataFormat={this.state.dataFormat}
                dataSource={this.state.dataSource}
                events={this.state.events}
                style={{marginTop: 20}}
                libraryPath={this.libraryPath}
              />

              </View>
              <View style={styles.boxvalue}>
                    {/*<Text style={styles.valuetitle}>Oxygen Saturation - </Text>
                    <Text style={styles.valuedate}>Friday,10 Aug. 15:32:46</Text>*/}
              </View>
          </View>
          <View style={styles.slide2}>

              <View style={styles.titlebox}>
              {/*<Text style={styles.title}>Charts</Text>
              <View style={styles.hr} />*/}
                <Text style={styles.content}>Please Rotate Your Device to Landscape Mode</Text>
              </View >
              <View style={styles.mainchart}>
              <View style={styles.pickerStyleView}>
              <RNPickerSelect
                  placeholder={{
                      label: 'Select a Type...',
                      value: null,
                      color: '#9EA0A4',
                  }}
                  items={this.state.items}
                  onValueChange={(value) => {
                    if(value=='daily'){index=0}
                    if(value=='weekly'){index=1}
                    if(value=='monthly'){index=2}
                       this.setState({
                           type_item: value,modeValue: value, modeIndex: index
                       });
                       console.log(this.state.modeIndex)
                       this.validationAndApiParameter('chartdata');
                     }}

                  style={{ ...pickerSelectStyles }}
                  value={this.state.type_item}
              />
              </View>

              <FusionCharts
                type={this.state.type}
                width={this.state.width}
                height={this.state.height}
                dataFormat={this.state.dataFormat}
                dataSource={this.state.dataSource2}
                events={this.state.events}
                style={{marginTop: 20}}
                libraryPath={this.libraryPath}
              />
            </View>
              <View style={styles.boxvalue}>
                    {/*<Text style={styles.valuetitle}>Heart Rate -</Text>
                    <Text style={styles.valuedate}>Friday,10 Aug. 15:32:46</Text> */}
              </View>

          </View>
          <View style={styles.slide3}>
              <View style={styles.titlebox}>

                <Text style={styles.content}>Please Rotate Your Device to Landscape Mode</Text>
              </View >
              <View style={styles.mainchart}>
              <View style={styles.pickerStyleView}>
              <RNPickerSelect
                  placeholder={{
                      label: 'Select a Type...',
                      value: null,
                      color: '#9EA0A4',
                  }}
                  items={this.state.items}
                  onValueChange={(value) => {
                    if(value=='daily'){index=0}
                    if(value=='weekly'){index=1}
                    if(value=='monthly'){index=2}
                       this.setState({
                           type_item: value,modeValue: value, modeIndex: index
                       });
                       console.log(this.state.modeIndex)
                       this.validationAndApiParameter('chartdata');
                     }}

                  style={{ ...pickerSelectStyles }}
                  value={this.state.type_item}
              />
              </View>
              <FusionCharts
                type={this.state.type}
                width={this.state.width}
                height={this.state.height}
                dataFormat={this.state.dataFormat}
                dataSource={this.state.dataSource3}
                events={this.state.events}
                style={{marginTop: 20}}
                libraryPath={this.libraryPath}
              />
              </View>
              <View style={styles.boxvalue}>
                  {/*  <Text style={styles.valuetitle}>Temperature - </Text>
                  <Text style={styles.valuedate}>Friday,10 Aug. 15:32:46</Text>*/}
              </View>
          </View>
      </Swiper>
      <Spinner visible={this.state.isVisible}  />
</View>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
  },

  content: {
      flex: 1,
      backgroundColor: '#ffffff',
      height:'70%',
  },
  titlebox:{
    flex:1.5,
    flexDirection:'column',
    alignItems:'center',
    paddingTop:AppSizes.ResponsiveSize.Padding(3),
    paddingLeft:AppSizes.ResponsiveSize.Padding(3),
    paddingRight:AppSizes.ResponsiveSize.Padding(3),

  },

hr:{
  borderBottomWidth:2.5,
  borderBottomColor:AppColors.primary,
  marginBottom:AppSizes.ResponsiveSize.Padding(0.5),
  width:30

},
title:{
  textAlign:'center',
  fontSize:AppSizes.ResponsiveSize.Sizes(20),
  color:AppColors.primary,
  paddingBottom:AppSizes.ResponsiveSize.Padding(0),


},
content:{
  color:AppColors.smalltext,
  justifyContent:'center',
  fontSize:AppSizes.ResponsiveSize.Sizes(14),
  fontWeight:'bold',
  alignItems:'center',
  textAlign:'center'
},
charts:{
  flex:5,
  width:AppSizes.screen.width
},
slide1: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  slide2: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  slide3: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  boxvalue:{
    flex:3,
    flexDirection:'column',
    alignItems:'center',

  },
 valuetitle:{
   fontSize:AppSizes.ResponsiveSize.Sizes(22),
   color:AppColors.primary,
   fontWeight:'700',
   textTransform:'uppercase',
   paddingBottom:AppSizes.ResponsiveSize.Padding(0),
 },
 valuedate:{
   fontSize:AppSizes.ResponsiveSize.Sizes(12),
   color:AppColors.primary,
   fontWeight:'700',
   paddingBottom:AppSizes.ResponsiveSize.Padding(0),
 },
 mainchart:{
   flex:2,
   justifyContent:'center',
   alignItems:'center',
},
 pickerStyleView: {
  borderColor:'#4eb4c4',
   borderWidth: 0,
   borderRadius:25,
   backgroundColor:'#4eb4c4',
   color:'#fff',
   width: AppSizes.screen.width/2.5,
   paddingLeft:AppSizes.ResponsiveSize.Padding(2),
   marginTop: 20
 },
 pickerStyle: {
   color:'#fff',

 }
});

const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
      paddingTop: 13,
        paddingHorizontal: 10,
        paddingBottom: 12,
      fontSize:AppSizes.ResponsiveSize.Sizes(16),
        color: '#fff',
    },
    inputAndroid: {
      fontSize:AppSizes.ResponsiveSize.Sizes(16),
        color: '#fff',
    },
});
