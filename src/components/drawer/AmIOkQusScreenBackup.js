import React, { Component } from 'react';
import {AsyncStorage,StyleSheet,Text,View,Image,TouchableOpacity,Platform,Alert,ScrollView,TextInput,Button,StatusBar,Modal} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions } from 'react-navigation'
import Spacer from '../common/Spacer'
import Spinner from 'react-native-loading-spinner-overlay';
import Header from '../common/signupHeader'
import CommonButton from '../common/CommonButton'
import Swiper from 'react-native-swiper';
import Animbutton from './animAnsbutton'
import AnimSinglebutton from './animSingleAnsOptions';
import RNPickerSelect from 'react-native-picker-select';
import { jsonData } from '../data/Question';
import { pickerData } from '../data/pickerData';
import Picker from 'react-native-q-picker';
import Sound from 'react-native-sound'
import Orientation from 'react-native-orientation'
import LinearGradient from 'react-native-linear-gradient';

import * as commonFunctions from '../../utils/CommonFunctions'

var Constant = require('../../api/ApiRules').Constant;
var WebServices = require('../../api/ApiRules').WebServices;

/* Images */
//var thankyou_icon = require('../../themes/Images/thank-you.png');
var logo = require('../../themes/Images/logo.png');
var back_icon = require( '../../themes/Images/left-whitearrow.png')
var oxy_icon = require('../../themes/Images/oxy.png');
var temp_icon = require('../../themes/Images/temp.png');
var heartRate_icon = require('../../themes/Images/heart_rate.png');
var qus_icon = require('../../themes/Images/que_icon_196.png');
var mice=require('../../themes/Images/mice.png')
var mice_off=require('../../themes/Images/mice-off.png')

var help_audio1=require('../../themes/sound/amiokQus1Help.mp3')
var help_audio2=require('../../themes/sound/amiokQus2Help.mp3')
var help_audio3=require('../../themes/sound/amiokQus3Help.mp3')
var help_audio4=require('../../themes/sound/amiokQus4Help.mp3')

let _that
let arrnew = []
let ans_track = []
let qus_result = new Array();
let final_arr = new Array();
let symptom_data = []
let triage_data = []
let vital_data = []
let exacerb_data = []
let profile_data=[]
var final_exacerb_data=[]
let final_triage_data = []
let final_symptom_data =[]
let final_vital_data = []
let final_med_data = []
let final_profile_data = []
let python_result=[]
let python_array=[]
let options_arr=[]
let vitals=[]
let vitals_data=[]
let vitals_api=[]
var current_ans=''
var move_last=0;
const MyStatusBar = ({backgroundColor, ...props}) => (
  <View style={[styles.statusBar, { backgroundColor }]}>
    <StatusBar translucent backgroundColor={backgroundColor} {...props} />
  </View>
);

var oxygen_data = [];
var heart_data = [];
var fev_data= [];
var temperature_data= [];

//oxygen_data
for( var i = 100; i >73; i--){
  var numbers = {
      name: i+'%',
      id: i
  };
  oxygen_data.push(numbers);
}
//console.log(oxygen_data);

//heart_data
for( var j = 30; j <151; j++){
  var numbers = {
      name: j+' bpm',
      id: j
  };
  heart_data.push(numbers);
}

//fev_data
for( var k = 0; k <7; k++){
  var numbers = {
      name: k+'L',
      id: k
  };
  fev_data.push(numbers);
}

//temperature_data
/*for( var l = 97.0; l <106.0; l++){
  var numbers = {
      name: l,
      id: l
  };
  temperature_data.push(numbers);
}*/

//console.log(temperature_data);

export default class AmIOkQusScreen extends Component {
  constructor (props) {
      super(props)
      this.qno = this.props.navigation.state.params.qus_no
      this.score = 0
      this.type ='qus'
      this.inputRefs = {};

      const jdata = jsonData.amiokqus.qus1
      arrnew = Object.keys(jdata).map( function(k) { return jdata[k] });

      this.state = {
        question : arrnew[this.qno].question,
        title : arrnew[this.qno].title,
        options : arrnew[this.qno].options,
        suboption : arrnew[this.qno].suboption,
        type : arrnew[this.qno].type,
        modal_title:arrnew[this.qno].modal_title,
        countCheck : [],
        dataStorage:[],
        qus_no : 0,
        uid:'',
        isVisible: false,
        Modal_Visibility: false,
        status:'ans',
        oxygen_sat: 0,
        items1: oxygen_data,
        heart_rate: 0,
        items2: heart_data,
        temperature: 0,
        items3: pickerData.temperature_data,
        temperature_val:'',
        screentype:'',
        fev: 0,
        items4: fev_data,
        python_result:[],
        python_array:[],
        baseline_heart_rate:'',
        baseline_oxygen_sat:'',
        baseline_heart_rate_count:0,
        baseline_oxygen_sat_count:0,
        api:0,
        Modal_Visibility: false,
        audioStatus: false,
        audioImg: mice,
  },
    _that = this;
  }

  playTrack = () => {
    this.setState({audioStatus: !this.state.audioStatus})
    if(this.state.audioStatus==true){
      if(this.qno==0){
        help_audio=help_audio1;
      }
      else if(this.qno==1){
        help_audio=help_audio2;
      }
      else if(this.qno==2){
        help_audio=help_audio3;
      }
      else{
        help_audio=help_audio4;
      }

      this.track = new Sound(help_audio,  (e) => {
        if (e) {
          console.log('error loading track:', e)
        } else {

            this.setState({audioImg: mice_off});
            this.track.play((success) => {
                 if (success) {
                     this.stop();
                 }
             });
          }
        })
      }
        else {
          this.stop();
        }
      }

  stop() {
     if (!this.track) return;
     this.track.stop();
     this.track.release();
     this.track = null;
     this.setState({audioStatus: false});
     this.setState({audioImg: mice});
 }

  Show_Custom_Alert(visible) {
    this.stop();
    this.setState({Modal_Visibility: visible});
  }

componentDidMount() {
  Orientation.lockToPortrait();
  AsyncStorage.getItem('loggedIn').then((value) => {
      var loggedIn = JSON.parse(value);
      if (loggedIn) {
          AsyncStorage.getItem('UserData').then((UserData) => {
              const data = JSON.parse(UserData)
              //console.log(val)
              var uid=data.id;
              _that.setState({uid:  uid});
              _that.validationAndApiParameter('profile_result');
          })
     }
  })
}

validationAndApiParameter(apiname) {
    //console.log('uid='+_that.state.uid)
    if(apiname == 'python_triage'){
      final_triage_data.push(triage_data)
      //console.log(final_triage_data)
      var data = {
          X_test: final_triage_data
      };
      python_array.push({
          name: 'triage',
          value: data,
      });
        _that.setState({ python_array: python_array })
      console.log('python_triage');
      console.log(data);
        _that.setState({isVisible: true});
        this.postToApiCalling('POST_1', apiname, Constant.URL_triage,data);
    }
    else if(apiname == 'python_exacerb'){
      final_exacerb_data.push(exacerb_data)
      var data = {
          X_test: final_exacerb_data
      };
      python_array.push({
          name: 'exacerb',
          value: data,
      });
        _that.setState({ python_array: python_array })
        console.log('python_exacerb');
        console.log(data);
        _that.setState({isVisible: true});
        this.postToApiCalling('POST_1', apiname, Constant.URL_exacerb,data);
    }
    else if(apiname == 'python_symptom'){
      var j=0;
      for(i=35;i<=58;i++){
        symptom_data[j]=triage_data[i]
        j++
      }
      final_symptom_data.push(symptom_data)
      var data = {
          X_test: final_symptom_data
      };
      python_array.push({
          name: 'symptom',
          value: data,
      });
        _that.setState({ python_array: python_array })
        console.log('python_symptom');
        console.log(data);
        //_that.setState({isVisible: true});
        this.postToApiCalling('POST_1', apiname, Constant.URL_symptom,data);
    }
    else if(apiname == 'python_vital'){
      const { oxygen_sat, heart_rate, temperature, fev} = this.state
        var j=0;
        for(i=59;i<=106;i++){
          vital_data[j]=triage_data[i]
          j++
        }
        final_vital_data.push(vital_data)
        var data = {
            X_test: final_vital_data
        };
        python_array.push({
            name: 'vital',
            value: data,
        });
          _that.setState({ python_array: python_array })
          console.log('python_vital');
          console.log(data);
        //  _that.setState({isVisible: true});
          this.postToApiCalling('POST_1', apiname, Constant.URL_vital,data);

    }
    else if(apiname == 'python_profile'){
        final_profile_data.push(profile_data)
        var data = {
            X_test: final_profile_data
        };
        python_array.push({
            name: 'profile',
            value: data,
        });
          _that.setState({ python_array: python_array })
          console.log('python_profile');
          console.log(data);
        //  _that.setState({isVisible: true});
        this.postToApiCalling('POST_1', apiname, Constant.URL_profile,data);

    }
    else if(apiname == 'python_med'){

      final_med_data.push(triage_data)
      var data = {
          X_test: final_med_data
      };
      python_array.push({
          name: 'med',
          value: data,
      });
        _that.setState({ python_array: python_array })
        console.log('python_med');
        console.log(data);
      //  _that.setState({isVisible: true});
        this.postToApiCalling('POST_1', apiname, Constant.URL_med,data);
    }
   else if(apiname == 'web_triage'){
      //options_arr.sort(function(a,b) {
      //    return a.order - b.order;
      //});
      console.log('web_triage')
      qus_result=[]
      var json_options_arr = JSON.stringify(options_arr);
      var json_python_result = JSON.stringify(this.state.python_result);
      var json_python_array = JSON.stringify(this.state.python_array);
      var json_qus_result=JSON.stringify(qus_result)
      var json_vitals=JSON.stringify(vitals)
      var json_vitals_data=JSON.stringify(vitals_data)
      var data = {
          uid: this.state.uid,
          score_result : json_python_result,
          score_array : json_python_array,
          variable : json_options_arr,
          qus_result : json_qus_result,
          vitals: json_vitals,
          vitals_data: json_vitals_data
      };
      console.log(data);
      //  _that.setState({isVisible: true});
        this.postToApiCalling('POST', apiname, Constant.URL_saveVariable,data);
    }
    else if(apiname == 'profile_result'){
      var data = {
          uid: this.state.uid,
      };
    //  console.log(data)
        _that.setState({isVisible: true});
        this.postToApiCalling('POST', apiname, Constant.URL_showVarData,data);
    }
  }

postToApiCalling(method, apiKey, apiUrl, data) {
  //console.log(apiUrl +" "+ apiKey)
    new Promise(function(resolve, reject) {
        if (method == 'POST') {
            resolve(WebServices.callWebService(apiUrl, data));
        } else {
          //console.log('callWebService_POST')
            resolve(WebServices.callWebService_POST(apiUrl, data));
        }
    }).then((jsonRes) => {
        //console.log(jsonRes)

        if(apiKey=="python_triage" || apiKey=="python_symptom" || apiKey=="python_vital" || apiKey=="python_med" || apiKey=="python_exacerb"|| apiKey=="python_profile"){
          _that.apiSuccessfullResponse(apiKey, jsonRes)
        }
        else{
          if ((!jsonRes) || (jsonRes.code == 0)) {
            setTimeout(() => {
                  Alert.alert(jsonRes.message);
              }, 200);
          } else {
              if (jsonRes.code == 1) {
                  _that.apiSuccessfullResponse(apiKey, jsonRes)
              }
          }
        }
    }).catch((error) => {
        console.log("ERROR" + error);
        _that.setState({ isVisible: false })
        setTimeout(() => {
            Alert.alert("Server Error");
        }, 200);
    })
}

apiSuccessfullResponse(apiKey, jsonRes) {
  //console.log(jsonRes);

  if(apiKey=="python_triage"){
    //console.log("python_triage result="+jsonRes);
    python_result.push({
        name: 'triage',
        status:'',
        value: jsonRes,
    });
    _that.setState({ python_result: python_result })
    _that.validationAndApiParameter('python_symptom');
  }
  else if(apiKey=="python_symptom"){
  //  console.log("python_symptom result="+jsonRes);
  python_result.push({
      name: 'symptom',
      status:'',
      value: jsonRes,
  });
    _that.setState({ python_result: python_result })
    //console.log(this.state.python_result)
    _that.validationAndApiParameter('python_med');
  }

  else if(apiKey=="python_med"){
  //  console.log("python_symptom result="+jsonRes);
  jsonRes = jsonRes.slice(1,-1);
  jsonRes = jsonRes.replace(/ +/g, ',');
  python_result.push({
      name: 'med',
      status:'',
      value: jsonRes,
  });
    _that.setState({ python_result: python_result })
  //  console.log(this.state.python_result)
    _that.validationAndApiParameter('python_exacerb');
  }
  else if(apiKey=="python_exacerb"){
  //  console.log("python_symptom result="+jsonRes);
  jsonRes = jsonRes.slice(1,-1);
  jsonRes = jsonRes.replace(/ +/g, ',');
  python_result.push({
      name: 'exacerb',
      status:'',
      value: jsonRes,
  });
    _that.setState({ python_result: python_result })
  //  console.log(this.state.python_result)
    _that.validationAndApiParameter('python_vital');
  }
  else if(apiKey=="python_vital"){
  //  console.log("python_symptom result="+jsonRes);
  var vital_status=0
  if(jsonRes==1){vital_status='Low'}
  else if(jsonRes==2){vital_status='Medium Low'}
  else if(jsonRes==3){vital_status='Medium'}
  else if(jsonRes==4){vital_status='Medium High'}
  else if(jsonRes==5){vital_status='High'}
  python_result.push({
      name: 'vital',
      status:vital_status,
      value: jsonRes,
  });
    _that.setState({ python_result: python_result })
    //console.log(this.state.python_result)
    _that.validationAndApiParameter('python_profile');
  }
  else if(apiKey=="python_profile"){
  //  console.log("python_symptom result="+jsonRes);
  python_result.push({
      name: 'profile',
      status:'',
      value: jsonRes,
  });
    _that.setState({ python_result: python_result })
    //console.log(this.state.python_result)
    _that.validationAndApiParameter('web_triage');
  }

  else if(apiKey=="web_triage"){
      _that.setState({ isVisible: false })

    vitals_api.push({name: 'oxygen_sat',value:jsonRes.vitals.oxygen_sat});
    vitals_api.push({name: 'heart_rate',value:jsonRes.vitals.heart_rate});
    vitals_api.push({name: 'temperature',value:jsonRes.vitals.temperature});
  //  console.log(vitals_api)
    AsyncStorage.setItem('vitals', JSON.stringify(vitals_api));

    arrnew = []
    ans_track = []
    qus_result = new Array();
    final_arr = new Array();
    symptom_data = []
    triage_data = []
    vital_data = []
    exacerb_data = []
    profile_data=[]
    final_exacerb_data=[]
    final_triage_data = []
    final_symptom_data =[]
    final_vital_data = []
    final_med_data = []
    final_profile_data=[]
    python_result=[]
    python_array=[]
    options_arr=[]
    vitals=[]
    vitals_api=[]
    vitals_data=[]
    current_ans=''
    move_last=0;

    //console.log(jsonRes);


    if(this.state.screentype=="MedicalInAttentionScreen"){
      _that.props.navigation.navigate('MedicalInAttentionScreen');
    }
    else if(this.state.screentype=="MedicalAttentionScreen"){
      _that.props.navigation.navigate('MedicalAttentionScreen');
    }
    //_that.validationAndApiParameter('python_symptom');
  }

  if(apiKey=="profile_result"){
    _that.setState({ isVisible: false })

    var_baseline_oxygen_sat=parseInt(jsonRes.baseline_oxygen_sat);
    var_baseline_heart_rate=parseInt(jsonRes.baseline_heart_rate);
    var_baseline_heart_rate_count=parseInt(jsonRes.baseline_heart_rate_count);
    var_baseline_oxygen_sat_count=parseInt(jsonRes.baseline_oxygen_sat_count);

  //  console.log( var_baseline_oxygen_sat+" "+var_baseline_heart_rate)
    this.setState({baseline_oxygen_sat:var_baseline_oxygen_sat,
                  baseline_heart_rate:var_baseline_heart_rate,
                  baseline_heart_rate_count:var_baseline_heart_rate_count,
                  baseline_oxygen_sat_count:var_baseline_oxygen_sat_count
    })
  //  console.log(jsonRes)
    result=jsonRes.data
    for(i=0;i<=106;i++){
      if(i<=34){
        mean=JSON.parse(jsonRes.mean[i])
        std=JSON.parse(jsonRes.std[i])
        value=JSON.parse(result[i])

        profile_data[i]=value
        triage_data[i]=value
        exacerb_data[i]=(value-mean)/std
      }
      else{
        triage_data[i]=0;
        exacerb_data[i]=0;
      }
    }
//console.log('final' +triage_data)
  }
}

  updateAlert = () => {
        this.setState({Modal_Visibility: !this.state.Modal_Visibility});
  }

  prev= () =>{
    //console.log(this.qno)
    if(this.qno > 0){
      //console.log(this.qno)
      if(this.qno==2){
        _that.setState({
          oxygen_sat : '',
          heart_rate : '',
          temperature : '',
        });
      }
      this.qno--
      this.setState({ qus_no: this.qno })
      this.setState({ question: arrnew[this.qno].question,qus_id: arrnew[this.qno].qus_id, options: arrnew[this.qno].options, type: arrnew[this.qno].type,suboption : arrnew[this.qno].suboption,modal_title : arrnew[this.qno].modal_title})
    }
  }
  next= () =>{
    console.log(profile_data)
    current_ans=0

    const { countCheck} = this.state
    ans_track =[];
    //console.log('count'+countCheck)
    if(this.state.qus_no==0){
      if(countCheck.indexOf('rws_1') != -1){
        move_last=1
      }
      else{
        move_last=0
      }
    }
    //console.log("move_last "+move_last)
    if(this.state.countCheck.length <= 0 && this.state.qus_no!=1){
      Alert.alert("Invalid Value","Missing or blank value is not allowed")
    }
    else if(this.qno == 1){
          const { oxygen_sat, heart_rate, temperature, fev, baseline_oxygen_sat,baseline_heart_rate_count,baseline_oxygen_sat_count,temperature_val, baseline_heart_rate } = this.state
          var status=0;
          vitals_data.push({name: 'oxygen_sat',value: this.state.oxygen_sat});
          vitals_data.push({name: 'heart_rate',value: this.state.heart_rate});
          vitals_data.push({name: 'temperature',value: this.state.temperature});

          if ((oxygen_sat <= 86 && oxygen_sat != 0)) {
            status=1;
            vitals.push({name: 'oxygen_sat',value: 0, base_value:94});
          }
          else{
            vitals.push({name: 'oxygen_sat',value: this.state.oxygen_sat, base_value:94});
          }
          if( heart_rate > 120){
            status=1;
            vitals.push({name: 'heart_rate',value: 0, base_value:80});
          }
          else{
            vitals.push({name: 'heart_rate',value: this.state.heart_rate, base_value:80});
          }

          if(temperature > 104){
            status=1;
            vitals.push({name: 'temperature',value: 0, base_value:98.6});
          }
          else{
            vitals.push({name: 'temperature',value: this.state.temperature, base_value:98.6});
          }

          // Current heart rate - baseline heart rate
          diff_hr=heart_rate-baseline_heart_rate
          //console.log(diff_hr)
          if( heart_rate == 0 || baseline_heart_rate ==0){
            triage_data[59]= 1    //(hrdiff_nan)
            triage_data[60]= 0    //(hrdiff_(20, inf])
            triage_data[61]= 0    //(hrdiff_(10, 20])
            triage_data[62]= 0    //(hrdiff_(0, 10])
            triage_data[63]= 0    //(hrdiff_(-10, 0])
            triage_data[64]= 0    //(hrdiff_(-inf, -10])

            exacerb_data[59]=(1-0.544269835185895)/0.498036325675762
            exacerb_data[60]=(0-0.03717899578382522)/0.18920020627983342
            exacerb_data[61]=(0-0.12571866615561517)/0.3315320242988122
            exacerb_data[62]=(0-0.1391337677270985)/0.3460860621365158
            exacerb_data[63]=(0-0.1241855116903028)/0.3297930720565859
            exacerb_data[64]=(0-0.02951322345726332)/0.1692400457882973
         }
          else if(diff_hr > 20){
            triage_data[59]= 0    //(hrdiff_nan)
            triage_data[60]= 1    //(hrdiff_(20, inf])
            triage_data[61]= 0    //(hrdiff_(10, 20])
            triage_data[62]= 0    //(hrdiff_(0, 10])
            triage_data[63]= 0    //(hrdiff_(-10, 0])
            triage_data[64]= 0    //(hrdiff_(-inf, -10])

            exacerb_data[59]=(0-0.544269835185895)/0.498036325675762
            exacerb_data[60]=(1-0.03717899578382522)/0.18920020627983342
            exacerb_data[61]=(0-0.12571866615561517)/0.3315320242988122
            exacerb_data[62]=(0-0.1391337677270985)/0.3460860621365158
            exacerb_data[63]=(0-0.1241855116903028)/0.3297930720565859
            exacerb_data[64]=(0-0.02951322345726332)/0.1692400457882973
          }
          else if(diff_hr > 10 && diff_hr  <= 20){
            triage_data[59]= 0    //(hrdiff_nan)
            triage_data[60]= 0    //(hrdiff_(20, inf])
            triage_data[61]= 1    //(hrdiff_(10, 20])
            triage_data[62]= 0    //(hrdiff_(0, 10])
            triage_data[63]= 0    //(hrdiff_(-10, 0])
            triage_data[64]= 0    //(hrdiff_(-inf, -10])

            exacerb_data[59]=(0-0.544269835185895)/0.498036325675762
            exacerb_data[60]=(0-0.03717899578382522)/0.18920020627983342
            exacerb_data[61]=(1-0.12571866615561517)/0.3315320242988122
            exacerb_data[62]=(0-0.1391337677270985)/0.3460860621365158
            exacerb_data[63]=(0-0.1241855116903028)/0.3297930720565859
            exacerb_data[64]=(0-0.02951322345726332)/0.1692400457882973
          }
          else if(diff_hr > 0 && diff_hr  <= 10){
            triage_data[59]= 0    //(hrdiff_nan)
            triage_data[60]= 0    //(hrdiff_(20, inf])
            triage_data[61]= 0    //(hrdiff_(10, 20])
            triage_data[62]= 1    //(hrdiff_(0, 10])
            triage_data[63]= 0    //(hrdiff_(-10, 0])
            triage_data[64]= 0    //(hrdiff_(-inf, -10])

            exacerb_data[59]=(0-0.544269835185895)/0.498036325675762
            exacerb_data[60]=(0-0.03717899578382522)/0.18920020627983342
            exacerb_data[61]=(0-0.12571866615561517)/0.3315320242988122
            exacerb_data[62]=(1-0.1391337677270985)/0.3460860621365158
            exacerb_data[63]=(0-0.1241855116903028)/0.3297930720565859
            exacerb_data[64]=(0-0.02951322345726332)/0.1692400457882973
          }
          else if(diff_hr > -10 && diff_hr  <= 0){
            triage_data[59]= 0    //(hrdiff_nan)
            triage_data[60]= 0    //(hrdiff_(20, inf])
            triage_data[61]= 0    //(hrdiff_(10, 20])
            triage_data[62]= 0    //(hrdiff_(0, 10])
            triage_data[63]= 1    //(hrdiff_(-10, 0])
            triage_data[64]= 0    //(hrdiff_(-inf, -10])

            exacerb_data[59]=(0-0.544269835185895)/0.498036325675762
            exacerb_data[60]=(0-0.03717899578382522)/0.18920020627983342
            exacerb_data[61]=(0-0.12571866615561517)/0.3315320242988122
            exacerb_data[62]=(0-0.1391337677270985)/0.3460860621365158
            exacerb_data[63]=(1-0.1241855116903028)/0.3297930720565859
            exacerb_data[64]=(0-0.02951322345726332)/0.1692400457882973
          }
          else if(diff_hr <= -10){
            triage_data[59]= 0    //(hrdiff_nan)
            triage_data[60]= 0    //(hrdiff_(20, inf])
            triage_data[61]= 0    //(hrdiff_(10, 20])
            triage_data[62]= 0    //(hrdiff_(0, 10])
            triage_data[63]= 0    //(hrdiff_(-10, 0])
            triage_data[64]= 1    //(hrdiff_(-inf, -10])

            exacerb_data[59]=(0-0.544269835185895)/0.498036325675762
            exacerb_data[60]=(0-0.03717899578382522)/0.18920020627983342
            exacerb_data[61]=(0-0.12571866615561517)/0.3315320242988122
            exacerb_data[62]=(0-0.1391337677270985)/0.3460860621365158
            exacerb_data[63]=(0-0.1241855116903028)/0.3297930720565859
            exacerb_data[64]=(1-0.02951322345726332)/0.1692400457882973
          }


          //(current heart rate)
          //console.log(heart_rate)
          if( heart_rate == 0){
            triage_data[65]= 1    //(curhr_nan)
            triage_data[66]= 0   //(curhr_(120, inf])
            triage_data[67]= 0    //(curhr_(110, 120])
            triage_data[68]= 0    //(curhr_(100, 110])
            triage_data[69]= 0    //(curhr_(90, 100])
            triage_data[70]= 0   //(curhr_(80, 90])
            triage_data[71]= 0    //(curhr_(60, 80])
            triage_data[72]= 0    //(curhr_(0, 60])

            exacerb_data[65]=(1-0.3418934457646608)/0.4743440918867102
            exacerb_data[66]=(0-0.0026830203142966655)/0.05172834538519221
            exacerb_data[67]=(0-0.018397853583748562)/0.13438516498207498
            exacerb_data[68]=(0-0.07244154848600996)/0.2592176123240067
            exacerb_data[69]=(0-0.15638175546186278)/0.363216880142607
            exacerb_data[70]=(0-0.19777692602529706)/0.3983230015418156
            exacerb_data[71]=(0-0.19816021464162514)/0.39861352708458125
            exacerb_data[72]=(0-0.012265235722499043)/0.11006725087495632
          }
          else if(heart_rate > 120){
            triage_data[65]= 0    //(curhr_nan)
            triage_data[66]= 1   //(curhr_(120, inf])
            triage_data[67]= 0    //(curhr_(110, 120])
            triage_data[68]= 0    //(curhr_(100, 110])
            triage_data[69]= 0    //(curhr_(90, 100])
            triage_data[70]= 0   //(curhr_(80, 90])
            triage_data[71]= 0    //(curhr_(60, 80])
            triage_data[72]= 0    //(curhr_(0, 60])

            exacerb_data[65]=(0-0.3418934457646608)/0.4743440918867102
            exacerb_data[66]=(1-0.0026830203142966655)/0.05172834538519221
            exacerb_data[67]=(0-0.018397853583748562)/0.13438516498207498
            exacerb_data[68]=(0-0.07244154848600996)/0.2592176123240067
            exacerb_data[69]=(0-0.15638175546186278)/0.363216880142607
            exacerb_data[70]=(0-0.19777692602529706)/0.3983230015418156
            exacerb_data[71]=(0-0.19816021464162514)/0.39861352708458125
            exacerb_data[72]=(0-0.012265235722499043)/0.11006725087495632
          }
          else if(heart_rate > 110 && heart_rate  <= 120){
            triage_data[65]= 0    //(curhr_nan)
            triage_data[66]= 0   //(curhr_(120, inf])
            triage_data[67]= 1    //(curhr_(110, 120])
            triage_data[68]= 0    //(curhr_(100, 110])
            triage_data[69]= 0    //(curhr_(90, 100])
            triage_data[70]= 0   //(curhr_(80, 90])
            triage_data[71]= 0    //(curhr_(60, 80])
            triage_data[72]= 0    //(curhr_(0, 60])

            exacerb_data[65]=(0-0.3418934457646608)/0.4743440918867102
            exacerb_data[66]=(0-0.0026830203142966655)/0.05172834538519221
            exacerb_data[67]=(1-0.018397853583748562)/0.13438516498207498
            exacerb_data[68]=(0-0.07244154848600996)/0.2592176123240067
            exacerb_data[69]=(0-0.15638175546186278)/0.363216880142607
            exacerb_data[70]=(0-0.19777692602529706)/0.3983230015418156
            exacerb_data[71]=(0-0.19816021464162514)/0.39861352708458125
            exacerb_data[72]=(0-0.012265235722499043)/0.11006725087495632
          }
          else if(heart_rate > 100 && heart_rate  <= 110){
            triage_data[65]= 0    //(curhr_nan)
            triage_data[66]= 0   //(curhr_(120, inf])
            triage_data[67]= 0    //(curhr_(110, 120])
            triage_data[68]= 1    //(curhr_(100, 110])
            triage_data[69]= 0    //(curhr_(90, 100])
            triage_data[70]= 0   //(curhr_(80, 90])
            triage_data[71]= 0    //(curhr_(60, 80])
            triage_data[72]= 0    //(curhr_(0, 60])

            exacerb_data[65]=(0-0.3418934457646608)/0.4743440918867102
            exacerb_data[66]=(0-0.0026830203142966655)/0.05172834538519221
            exacerb_data[67]=(0-0.018397853583748562)/0.13438516498207498
            exacerb_data[68]=(1-0.07244154848600996)/0.2592176123240067
            exacerb_data[69]=(0-0.15638175546186278)/0.363216880142607
            exacerb_data[70]=(0-0.19777692602529706)/0.3983230015418156
            exacerb_data[71]=(0-0.19816021464162514)/0.39861352708458125
            exacerb_data[72]=(0-0.012265235722499043)/0.11006725087495632
          }
          else if(heart_rate > 90 && heart_rate  <= 100){
            triage_data[65]= 0    //(curhr_nan)
            triage_data[66]= 0   //(curhr_(120, inf])
            triage_data[67]= 0    //(curhr_(110, 120])
            triage_data[68]= 0    //(curhr_(100, 110])
            triage_data[69]= 1    //(curhr_(90, 100])
            triage_data[70]= 0   //(curhr_(80, 90])
            triage_data[71]= 0    //(curhr_(60, 80])
            triage_data[72]= 0    //(curhr_(0, 60])

            exacerb_data[65]=(0-0.3418934457646608)/0.4743440918867102
            exacerb_data[66]=(0-0.0026830203142966655)/0.05172834538519221
            exacerb_data[67]=(0-0.018397853583748562)/0.13438516498207498
            exacerb_data[68]=(0-0.07244154848600996)/0.2592176123240067
            exacerb_data[69]=(1-0.15638175546186278)/0.363216880142607
            exacerb_data[70]=(0-0.19777692602529706)/0.3983230015418156
            exacerb_data[71]=(0-0.19816021464162514)/0.39861352708458125
            exacerb_data[72]=(0-0.012265235722499043)/0.11006725087495632
          }
          else if(heart_rate > 80 && heart_rate  <= 90){
            triage_data[65]= 0    //(curhr_nan)
            triage_data[66]= 0   //(curhr_(120, inf])
            triage_data[67]= 0    //(curhr_(110, 120])
            triage_data[68]= 0    //(curhr_(100, 110])
            triage_data[69]= 0    //(curhr_(90, 100])
            triage_data[70]= 1   //(curhr_(80, 90])
            triage_data[71]= 0    //(curhr_(60, 80])
            triage_data[72]= 0    //(curhr_(0, 60])

            exacerb_data[65]=(0-0.3418934457646608)/0.4743440918867102
            exacerb_data[66]=(0-0.0026830203142966655)/0.05172834538519221
            exacerb_data[67]=(0-0.018397853583748562)/0.13438516498207498
            exacerb_data[68]=(0-0.07244154848600996)/0.2592176123240067
            exacerb_data[69]=(0-0.15638175546186278)/0.363216880142607
            exacerb_data[70]=(1-0.19777692602529706)/0.3983230015418156
            exacerb_data[71]=(0-0.19816021464162514)/0.39861352708458125
            exacerb_data[72]=(0-0.012265235722499043)/0.11006725087495632
          }
          else if(heart_rate > 60 && heart_rate  <= 80){
            triage_data[65]= 0    //(curhr_nan)
            triage_data[66]= 0   //(curhr_(120, inf])
            triage_data[67]= 0    //(curhr_(110, 120])
            triage_data[68]= 0    //(curhr_(100, 110])
            triage_data[69]= 0    //(curhr_(90, 100])
            triage_data[70]= 0   //(curhr_(80, 90])
            triage_data[71]= 1    //(curhr_(60, 80])
            triage_data[72]= 0    //(curhr_(0, 60])

            exacerb_data[65]=(0-0.3418934457646608)/0.4743440918867102
            exacerb_data[66]=(0-0.0026830203142966655)/0.05172834538519221
            exacerb_data[67]=(0-0.018397853583748562)/0.13438516498207498
            exacerb_data[68]=(0-0.07244154848600996)/0.2592176123240067
            exacerb_data[69]=(0-0.15638175546186278)/0.363216880142607
            exacerb_data[70]=(0-0.19777692602529706)/0.3983230015418156
            exacerb_data[71]=(1-0.19816021464162514)/0.39861352708458125
            exacerb_data[72]=(0-0.012265235722499043)/0.11006725087495632
          }
          else if(heart_rate > 0 && heart_rate  <= 60){
            triage_data[65]= 0    //(curhr_nan)
            triage_data[66]= 0   //(curhr_(120, inf])
            triage_data[67]= 0    //(curhr_(110, 120])
            triage_data[68]= 0    //(curhr_(100, 110])
            triage_data[69]= 0    //(curhr_(90, 100])
            triage_data[70]= 0   //(curhr_(80, 90])
            triage_data[71]= 0    //(curhr_(60, 80])
            triage_data[72]= 1    //(curhr_(0, 60])

            exacerb_data[65]=(0-0.3418934457646608)/0.4743440918867102
            exacerb_data[66]=(0-0.0026830203142966655)/0.05172834538519221
            exacerb_data[67]=(0-0.018397853583748562)/0.13438516498207498
            exacerb_data[68]=(0-0.07244154848600996)/0.2592176123240067
            exacerb_data[69]=(0-0.15638175546186278)/0.363216880142607
            exacerb_data[70]=(0-0.19777692602529706)/0.3983230015418156
            exacerb_data[71]=(0-0.19816021464162514)/0.39861352708458125
            exacerb_data[72]=(1-0.012265235722499043)/0.11006725087495632
          }

          //(oxygen difference)
          diff_oxygen_sat=oxygen_sat-baseline_oxygen_sat
          //console.log(diff_oxygen_sat)
          if( oxygen_sat == 0 || baseline_oxygen_sat==0){
            triage_data[73]= 1    // (ox_diff_nan)            | when curox_nan | ox_diff = current - baseline
            triage_data[74]= 0    // (ox_diff_l_m4_box_l_93)  | diff < -4 and base <= 93
            triage_data[75]= 0    // (ox_diff_m4_m2_box_l_93) | -4 <= diff < -2 and base <=93
            triage_data[76]= 0    // (ox_diff_l_m2_box_g_93)  | diff < -2 and base > 93
            triage_data[77]= 0    // (ox_diff_m2_box_l_90)    | diff == -2 and base <= 90
            triage_data[78]= 0    // (ox_diff_m2_box_g_90)    | diff == -2 and base > 90
            triage_data[79]= 0    // (ox_diff_m1_box_l_92e94) | diff == -1 and (base <= 92 | base == 94)
            triage_data[80]= 0    // (ox_diff_m1_box_g_94e93) | diff == -1 and (base == 93 | base > 94)
            triage_data[81]= 0    // (ox_diff_g_m1)           | diff > -1

            exacerb_data[73]=(1-0.5454197010348792)/0.49793277734841096
            exacerb_data[74]=(0-0.019547719432732848)/0.1384399006706954
            exacerb_data[75]=(0-0.1084706784208509)/0.31097393836746395
            exacerb_data[76]=(0-0.07359141433499425)/0.2611048028496784
            exacerb_data[77]=(0-0.0038328861632809506)/0.06179154591803219
            exacerb_data[78]=(0-0.07589114603296282)/0.264823865968999
            exacerb_data[79]=(0-0.024530471444998085)/0.1546891315370418
            exacerb_data[80]=(0-0.017247987734764277)/0.13019406535578223
            exacerb_data[81]=(0-0.1314679954005366)/0.33791147004193434
         }
         else if(diff_oxygen_sat < -4 && baseline_oxygen_sat <= 93){
           triage_data[73]= 0    // (ox_diff_nan)            | when curox_nan | ox_diff = current - baseline
           triage_data[74]= 1    // (ox_diff_l_m4_box_l_93)  | diff < -4 and base <= 93
           triage_data[75]= 0    // (ox_diff_m4_m2_box_l_93) | -4 <= diff < -2 and base <=93
           triage_data[76]= 0    // (ox_diff_l_m2_box_g_93)  | diff < -2 and base > 93
           triage_data[77]= 0    // (ox_diff_m2_box_l_90)    | diff == -2 and base <= 90
           triage_data[78]= 0    // (ox_diff_m2_box_g_90)    | diff == -2 and base > 90
           triage_data[79]= 0    // (ox_diff_m1_box_l_92e94) | diff == -1 and (base <= 92 | base == 94)
           triage_data[80]= 0    // (ox_diff_m1_box_g_94e93) | diff == -1 and (base == 93 | base > 94)
           triage_data[81]= 0    // (ox_diff_g_m1)           | diff > -1

           exacerb_data[73]=(0-0.5454197010348792)/0.49793277734841096
           exacerb_data[74]=(1-0.019547719432732848)/0.1384399006706954
           exacerb_data[75]=(0-0.1084706784208509)/0.31097393836746395
           exacerb_data[76]=(0-0.07359141433499425)/0.2611048028496784
           exacerb_data[77]=(0-0.0038328861632809506)/0.06179154591803219
           exacerb_data[78]=(0-0.07589114603296282)/0.264823865968999
           exacerb_data[79]=(0-0.024530471444998085)/0.1546891315370418
           exacerb_data[80]=(0-0.017247987734764277)/0.13019406535578223
           exacerb_data[81]=(0-0.1314679954005366)/0.33791147004193434
         }
         else if((diff_oxygen_sat >= -4 && diff_oxygen_sat < -2) && baseline_oxygen_sat <= 93){
           triage_data[73]= 0    // (ox_diff_nan)            | when curox_nan | ox_diff = current - baseline
           triage_data[74]= 0    // (ox_diff_l_m4_box_l_93)  | diff < -4 and base <= 93
           triage_data[75]= 1    // (ox_diff_m4_m2_box_l_93) | -4 <= diff < -2 and base <=93
           triage_data[76]= 0    // (ox_diff_l_m2_box_g_93)  | diff < -2 and base > 93
           triage_data[77]= 0    // (ox_diff_m2_box_l_90)    | diff == -2 and base <= 90
           triage_data[78]= 0    // (ox_diff_m2_box_g_90)    | diff == -2 and base > 90
           triage_data[79]= 0    // (ox_diff_m1_box_l_92e94) | diff == -1 and (base <= 92 | base == 94)
           triage_data[80]= 0    // (ox_diff_m1_box_g_94e93) | diff == -1 and (base == 93 | base > 94)
           triage_data[81]= 0    // (ox_diff_g_m1)           | diff > -1

           exacerb_data[73]=(0-0.5454197010348792)/0.49793277734841096
           exacerb_data[74]=(0-0.019547719432732848)/0.1384399006706954
           exacerb_data[75]=(1-0.1084706784208509)/0.31097393836746395
           exacerb_data[76]=(0-0.07359141433499425)/0.2611048028496784
           exacerb_data[77]=(0-0.0038328861632809506)/0.06179154591803219
           exacerb_data[78]=(0-0.07589114603296282)/0.264823865968999
           exacerb_data[79]=(0-0.024530471444998085)/0.1546891315370418
           exacerb_data[80]=(0-0.017247987734764277)/0.13019406535578223
           exacerb_data[81]=(0-0.1314679954005366)/0.33791147004193434
         }
         else if(diff_oxygen_sat < -2 && baseline_oxygen_sat > 93){
           triage_data[73]= 0    // (ox_diff_nan)            | when curox_nan | ox_diff = current - baseline
           triage_data[74]= 0    // (ox_diff_l_m4_box_l_93)  | diff < -4 and base <= 93
           triage_data[75]= 0    // (ox_diff_m4_m2_box_l_93) | -4 <= diff < -2 and base <=93
           triage_data[76]= 1    // (ox_diff_l_m2_box_g_93)  | diff < -2 and base > 93
           triage_data[77]= 0    // (ox_diff_m2_box_l_90)    | diff == -2 and base <= 90
           triage_data[78]= 0    // (ox_diff_m2_box_g_90)    | diff == -2 and base > 90
           triage_data[79]= 0    // (ox_diff_m1_box_l_92e94) | diff == -1 and (base <= 92 | base == 94)
           triage_data[80]= 0    // (ox_diff_m1_box_g_94e93) | diff == -1 and (base == 93 | base > 94)
           triage_data[81]= 0    // (ox_diff_g_m1)           | diff > -1

           exacerb_data[73]=(0-0.5454197010348792)/0.49793277734841096
           exacerb_data[74]=(0-0.019547719432732848)/0.1384399006706954
           exacerb_data[75]=(0-0.1084706784208509)/0.31097393836746395
           exacerb_data[76]=(1-0.07359141433499425)/0.2611048028496784
           exacerb_data[77]=(0-0.0038328861632809506)/0.06179154591803219
           exacerb_data[78]=(0-0.07589114603296282)/0.264823865968999
           exacerb_data[79]=(0-0.024530471444998085)/0.1546891315370418
           exacerb_data[80]=(0-0.017247987734764277)/0.13019406535578223
           exacerb_data[81]=(0-0.1314679954005366)/0.33791147004193434
         }
         else if(diff_oxygen_sat == -2 && baseline_oxygen_sat <= 90){
           triage_data[73]= 0    // (ox_diff_nan)            | when curox_nan | ox_diff = current - baseline
           triage_data[74]= 0    // (ox_diff_l_m4_box_l_93)  | diff < -4 and base <= 93
           triage_data[75]= 0    // (ox_diff_m4_m2_box_l_93) | -4 <= diff < -2 and base <=93
           triage_data[76]= 0    // (ox_diff_l_m2_box_g_93)  | diff < -2 and base > 93
           triage_data[77]= 1    // (ox_diff_m2_box_l_90)    | diff == -2 and base <= 90
           triage_data[78]= 0    // (ox_diff_m2_box_g_90)    | diff == -2 and base > 90
           triage_data[79]= 0    // (ox_diff_m1_box_l_92e94) | diff == -1 and (base <= 92 | base == 94)
           triage_data[80]= 0    // (ox_diff_m1_box_g_94e93) | diff == -1 and (base == 93 | base > 94)
           triage_data[81]= 0    // (ox_diff_g_m1)           | diff > -1

           exacerb_data[73]=(0-0.5454197010348792)/0.49793277734841096
           exacerb_data[74]=(0-0.019547719432732848)/0.1384399006706954
           exacerb_data[75]=(0-0.1084706784208509)/0.31097393836746395
           exacerb_data[76]=(0-0.07359141433499425)/0.2611048028496784
           exacerb_data[77]=(1-0.0038328861632809506)/0.06179154591803219
           exacerb_data[78]=(0-0.07589114603296282)/0.264823865968999
           exacerb_data[79]=(0-0.024530471444998085)/0.1546891315370418
           exacerb_data[80]=(0-0.017247987734764277)/0.13019406535578223
           exacerb_data[81]=(0-0.1314679954005366)/0.33791147004193434
         }
         else if(diff_oxygen_sat == -2 && baseline_oxygen_sat > 90){
           triage_data[73]= 0    // (ox_diff_nan)            | when curox_nan | ox_diff = current - baseline
           triage_data[74]= 0    // (ox_diff_l_m4_box_l_93)  | diff < -4 and base <= 93
           triage_data[75]= 0    // (ox_diff_m4_m2_box_l_93) | -4 <= diff < -2 and base <=93
           triage_data[76]= 0    // (ox_diff_l_m2_box_g_93)  | diff < -2 and base > 93
           triage_data[77]= 0    // (ox_diff_m2_box_l_90)    | diff == -2 and base <= 90
           triage_data[78]= 1    // (ox_diff_m2_box_g_90)    | diff == -2 and base > 90
           triage_data[79]= 0    // (ox_diff_m1_box_l_92e94) | diff == -1 and (base <= 92 | base == 94)
           triage_data[80]= 0    // (ox_diff_m1_box_g_94e93) | diff == -1 and (base == 93 | base > 94)
           triage_data[81]= 0    // (ox_diff_g_m1)           | diff > -1

           exacerb_data[73]=(1-0.5454197010348792)/0.49793277734841096
           exacerb_data[74]=(0-0.019547719432732848)/0.1384399006706954
           exacerb_data[75]=(0-0.1084706784208509)/0.31097393836746395
           exacerb_data[76]=(0-0.07359141433499425)/0.2611048028496784
           exacerb_data[77]=(0-0.0038328861632809506)/0.06179154591803219
           exacerb_data[78]=(1-0.07589114603296282)/0.264823865968999
           exacerb_data[79]=(0-0.024530471444998085)/0.1546891315370418
           exacerb_data[80]=(0-0.017247987734764277)/0.13019406535578223
           exacerb_data[81]=(0-0.1314679954005366)/0.33791147004193434
         }
         else if(diff_oxygen_sat == -1 && (baseline_oxygen_sat <= 92 || baseline_oxygen_sat == 94)){
           triage_data[73]= 0    // (ox_diff_nan)            | when curox_nan | ox_diff = current - baseline
           triage_data[74]= 0    // (ox_diff_l_m4_box_l_93)  | diff < -4 and base <= 93
           triage_data[75]= 0    // (ox_diff_m4_m2_box_l_93) | -4 <= diff < -2 and base <=93
           triage_data[76]= 0    // (ox_diff_l_m2_box_g_93)  | diff < -2 and base > 93
           triage_data[77]= 0    // (ox_diff_m2_box_l_90)    | diff == -2 and base <= 90
           triage_data[78]= 0    // (ox_diff_m2_box_g_90)    | diff == -2 and base > 90
           triage_data[79]= 1    // (ox_diff_m1_box_l_92e94) | diff == -1 and (base <= 92 | base == 94)
           triage_data[80]= 0    // (ox_diff_m1_box_g_94e93) | diff == -1 and (base == 93 | base > 94)
           triage_data[81]= 0    // (ox_diff_g_m1)           | diff > -1

           exacerb_data[73]=(0-0.5454197010348792)/0.49793277734841096
           exacerb_data[74]=(0-0.019547719432732848)/0.1384399006706954
           exacerb_data[75]=(0-0.1084706784208509)/0.31097393836746395
           exacerb_data[76]=(0-0.07359141433499425)/0.2611048028496784
           exacerb_data[77]=(0-0.0038328861632809506)/0.06179154591803219
           exacerb_data[78]=(0-0.07589114603296282)/0.264823865968999
           exacerb_data[79]=(1-0.024530471444998085)/0.1546891315370418
           exacerb_data[80]=(0-0.017247987734764277)/0.13019406535578223
           exacerb_data[81]=(0-0.1314679954005366)/0.33791147004193434
         }
         else if(diff_oxygen_sat == -1 && (baseline_oxygen_sat == 93 || baseline_oxygen_sat > 94)){
           triage_data[73]= 0    // (ox_diff_nan)            | when curox_nan | ox_diff = current - baseline
           triage_data[74]= 0    // (ox_diff_l_m4_box_l_93)  | diff < -4 and base <= 93
           triage_data[75]= 0    // (ox_diff_m4_m2_box_l_93) | -4 <= diff < -2 and base <=93
           triage_data[76]= 0    // (ox_diff_l_m2_box_g_93)  | diff < -2 and base > 93
           triage_data[77]= 0    // (ox_diff_m2_box_l_90)    | diff == -2 and base <= 90
           triage_data[78]= 0    // (ox_diff_m2_box_g_90)    | diff == -2 and base > 90
           triage_data[79]= 0    // (ox_diff_m1_box_l_92e94) | diff == -1 and (base <= 92 | base == 94)
           triage_data[80]= 1    // (ox_diff_m1_box_g_94e93) | diff == -1 and (base == 93 | base > 94)
           triage_data[81]= 0    // (ox_diff_g_m1)           | diff > -1

           exacerb_data[73]=(0-0.5454197010348792)/0.49793277734841096
           exacerb_data[74]=(0-0.019547719432732848)/0.1384399006706954
           exacerb_data[75]=(0-0.1084706784208509)/0.31097393836746395
           exacerb_data[76]=(0-0.07359141433499425)/0.2611048028496784
           exacerb_data[77]=(0-0.0038328861632809506)/0.06179154591803219
           exacerb_data[78]=(0-0.07589114603296282)/0.264823865968999
           exacerb_data[79]=(0-0.024530471444998085)/0.1546891315370418
           exacerb_data[80]=(1-0.017247987734764277)/0.13019406535578223
           exacerb_data[81]=(0-0.1314679954005366)/0.33791147004193434
         }
         else if(diff_oxygen_sat > -1){
           triage_data[73]= 0    // (ox_diff_nan)            | when curox_nan | ox_diff = current - baseline
           triage_data[74]= 0    // (ox_diff_l_m4_box_l_93)  | diff < -4 and base <= 93
           triage_data[75]= 0    // (ox_diff_m4_m2_box_l_93) | -4 <= diff < -2 and base <=93
           triage_data[76]= 0    // (ox_diff_l_m2_box_g_93)  | diff < -2 and base > 93
           triage_data[77]= 0    // (ox_diff_m2_box_l_90)    | diff == -2 and base <= 90
           triage_data[78]= 0    // (ox_diff_m2_box_g_90)    | diff == -2 and base > 90
           triage_data[79]= 0    // (ox_diff_m1_box_l_92e94) | diff == -1 and (base <= 92 | base == 94)
           triage_data[80]= 0    // (ox_diff_m1_box_g_94e93) | diff == -1 and (base == 93 | base > 94)
           triage_data[81]= 1    // (ox_diff_g_m1)           | diff > -1

           exacerb_data[73]=(0-0.5454197010348792)/0.49793277734841096
           exacerb_data[74]=(0-0.019547719432732848)/0.1384399006706954
           exacerb_data[75]=(0-0.1084706784208509)/0.31097393836746395
           exacerb_data[76]=(0-0.07359141433499425)/0.2611048028496784
           exacerb_data[77]=(0-0.0038328861632809506)/0.06179154591803219
           exacerb_data[78]=(0-0.07589114603296282)/0.264823865968999
           exacerb_data[79]=(0-0.024530471444998085)/0.1546891315370418
           exacerb_data[80]=(0-0.017247987734764277)/0.13019406535578223
           exacerb_data[81]=(1-0.1314679954005366)/0.33791147004193434
         }

          //(current oxygen saturation)
          if( oxygen_sat == 0){
            triage_data[82]= 1    // (curox_nan)
            triage_data[83]= 0    // (curox_(93, 100] )
            triage_data[84]= 0    // (curox_(91, 93])
            triage_data[85]= 0    // (curox_(89, 91])
            triage_data[86]= 0    // (curox_(87, 89])
            triage_data[87]= 0    // (curox_(85, 87])
            triage_data[88]= 0    // (curox_(0, 85])


            exacerb_data[82]=(1-0.3545419701034879)/0.4783742902149168
            exacerb_data[83]=(0-0.10387121502491375)/0.10387121502491375
            exacerb_data[84]=(0-0.17209658873131467)/0.3774643729921533
            exacerb_data[85]=(0-0.18129551552318895)/0.385262834405773
            exacerb_data[86]=(0-0.14373323112303565)/0.3508190265563823
            exacerb_data[87]=(0-0.039862016098121886)/0.1956349553906841
            exacerb_data[88]=(0-0.0045994633959371405)/0.06766319776958947
          }
          else if(oxygen_sat > 93 && oxygen_sat  <= 100){
            triage_data[82]= 0    // (curox_nan)
            triage_data[83]= 1    // (curox_(93, 100] )
            triage_data[84]= 0    // (curox_(91, 93])
            triage_data[85]= 0    // (curox_(89, 91])
            triage_data[86]= 0    // (curox_(87, 89])
            triage_data[87]= 0    // (curox_(85, 87])
            triage_data[88]= 0    // (curox_(0, 85])

            exacerb_data[82]=(0-0.3545419701034879)/0.4783742902149168
            exacerb_data[83]=(1-0.10387121502491375)/0.10387121502491375
            exacerb_data[84]=(0-0.17209658873131467)/0.3774643729921533
            exacerb_data[85]=(0-0.18129551552318895)/0.385262834405773
            exacerb_data[86]=(0-0.14373323112303565)/0.3508190265563823
            exacerb_data[87]=(0-0.039862016098121886)/0.1956349553906841
            exacerb_data[88]=(0-0.0045994633959371405)/0.06766319776958947
          }
          else if(oxygen_sat > 91 && oxygen_sat  <= 93){
            triage_data[82]= 0    // (curox_nan)
            triage_data[83]= 0    // (curox_(93, 100] )
            triage_data[84]= 1    // (curox_(91, 93])
            triage_data[85]= 0    // (curox_(89, 91])
            triage_data[86]= 0    // (curox_(87, 89])
            triage_data[87]= 0    // (curox_(85, 87])
            triage_data[88]= 0    // (curox_(0, 85])

            exacerb_data[82]=(0-0.3545419701034879)/0.4783742902149168
            exacerb_data[83]=(0-0.10387121502491375)/0.10387121502491375
            exacerb_data[84]=(1-0.17209658873131467)/0.3774643729921533
            exacerb_data[85]=(0-0.18129551552318895)/0.385262834405773
            exacerb_data[86]=(0-0.14373323112303565)/0.3508190265563823
            exacerb_data[87]=(0-0.039862016098121886)/0.1956349553906841
            exacerb_data[88]=(0-0.0045994633959371405)/0.06766319776958947
          }
          else if(oxygen_sat > 89 && oxygen_sat  <= 91){
            triage_data[82]= 0    // (curox_nan)
            triage_data[83]= 0    // (curox_(93, 100] )
            triage_data[84]= 0    // (curox_(91, 93])
            triage_data[85]= 1    // (curox_(89, 91])
            triage_data[86]= 0    // (curox_(87, 89])
            triage_data[87]= 0    // (curox_(85, 87])
            triage_data[88]= 0    // (curox_(0, 85])

            exacerb_data[82]=(0-0.3545419701034879)/0.4783742902149168
            exacerb_data[83]=(0-0.10387121502491375)/0.10387121502491375
            exacerb_data[84]=(0-0.17209658873131467)/0.3774643729921533
            exacerb_data[85]=(1-0.18129551552318895)/0.385262834405773
            exacerb_data[86]=(0-0.14373323112303565)/0.3508190265563823
            exacerb_data[87]=(0-0.039862016098121886)/0.1956349553906841
            exacerb_data[88]=(0-0.0045994633959371405)/0.06766319776958947
          }
          else if(oxygen_sat > 87 && oxygen_sat  <= 89){
            triage_data[82]= 0    // (curox_nan)
            triage_data[83]= 0    // (curox_(93, 100] )
            triage_data[84]= 0    // (curox_(91, 93])
            triage_data[85]= 0    // (curox_(89, 91])
            triage_data[86]= 1    // (curox_(87, 89])
            triage_data[87]= 0    // (curox_(85, 87])
            triage_data[88]= 0    // (curox_(0, 85])

            exacerb_data[82]=(0-0.3545419701034879)/0.4783742902149168
            exacerb_data[83]=(0-0.10387121502491375)/0.10387121502491375
            exacerb_data[84]=(0-0.17209658873131467)/0.3774643729921533
            exacerb_data[85]=(0-0.18129551552318895)/0.385262834405773
            exacerb_data[86]=(1-0.14373323112303565)/0.3508190265563823
            exacerb_data[87]=(0-0.039862016098121886)/0.1956349553906841
            exacerb_data[88]=(0-0.0045994633959371405)/0.06766319776958947
          }
          else if(oxygen_sat > 85 && oxygen_sat  <= 87){
            triage_data[82]= 0    // (curox_nan)
            triage_data[83]= 0    // (curox_(93, 100] )
            triage_data[84]= 0    // (curox_(91, 93])
            triage_data[85]= 0    // (curox_(89, 91])
            triage_data[86]= 0    // (curox_(87, 89])
            triage_data[87]= 1    // (curox_(85, 87])
            triage_data[88]= 0    // (curox_(0, 85])

            exacerb_data[82]=(0-0.3545419701034879)/0.4783742902149168
            exacerb_data[83]=(0-0.10387121502491375)/0.10387121502491375
            exacerb_data[84]=(0-0.17209658873131467)/0.3774643729921533
            exacerb_data[85]=(0-0.18129551552318895)/0.385262834405773
            exacerb_data[86]=(0-0.14373323112303565)/0.3508190265563823
            exacerb_data[87]=(1-0.039862016098121886)/0.1956349553906841
            exacerb_data[88]=(0-0.0045994633959371405)/0.06766319776958947
          }
          else if(oxygen_sat > 0 && oxygen_sat  <= 85){
            triage_data[82]= 0    // (curox_nan)
            triage_data[83]= 0    // (curox_(93, 100] )
            triage_data[84]= 0    // (curox_(91, 93])
            triage_data[85]= 0    // (curox_(89, 91])
            triage_data[86]= 0    // (curox_(87, 89])
            triage_data[87]= 0    // (curox_(85, 87])
            triage_data[88]= 1    // (curox_(0, 85])

            exacerb_data[82]=(0-0.3545419701034879)/0.4783742902149168
            exacerb_data[83]=(0-0.10387121502491375)/0.10387121502491375
            exacerb_data[84]=(0-0.17209658873131467)/0.3774643729921533
            exacerb_data[85]=(0-0.18129551552318895)/0.385262834405773
            exacerb_data[86]=(0-0.14373323112303565)/0.3508190265563823
            exacerb_data[87]=(0-0.039862016098121886)/0.1956349553906841
            exacerb_data[88]=(1-0.0045994633959371405)/0.06766319776958947
          }

          //(fever difference)
          triage_data[89]= 1    // (fev_diff_nan)
          triage_data[90]= 0    // (fev_diff_(10, 100])
          triage_data[91]= 0    // (fev_diff_(0, 10])
          triage_data[92]= 0    // (fev_diff_(-5, 0])
          triage_data[93]= 0    // (fev_diff_(-15, -5])
          triage_data[94]= 0    // (fev_diff_(-100, -15])

          exacerb_data[89]=(1-0.5538520505940974)/0.4970914972586141
          exacerb_data[90]=(0-0.008815638175546186)/0.0934768564892083
          exacerb_data[91]=(0-0.11345343043311613)/0.3171462589343822
          exacerb_data[92]=(0-0.10540436949022614)/0.30707375072219056
          exacerb_data[93]=(0-0.19164430816404754)/0.39359467388719976
          exacerb_data[94]=(0-0.026830203142966653)/0.16158695288380742

          //(current fever)
          triage_data[95]= 1    // (curfev_nan)
          triage_data[96]= 0    // (curfev_(90, 100])
          triage_data[97]= 0    // (curfev_(70, 90])
          triage_data[98]= 0    // (curfev_(60, 70])
          triage_data[99]= 0    // (curfev_(50, 60] )
          triage_data[100]= 0   // (curfev_(30, 50])
          triage_data[101]= 0   // (curfev_(20, 30])
          triage_data[102]= 0   // (curfev_(0, 20])

          exacerb_data[95]=(1-0.3480260636259103)/0.4763443320361482
          exacerb_data[96]=(0-0.03296282100421617)/0.17853927701113884
          exacerb_data[97]=(0-0.09773859716366425)/0.2969608792217305
          exacerb_data[98]=(0-0.07167497125335377)/0.2579489673349841
          exacerb_data[99]=(0-0.09428899961671139)/0.2922303614753114
          exacerb_data[100]=(0-0.20735914143349943)/0.40541500699587013
          exacerb_data[101]=(0.09505557684936758)/0.29329168784539655
          exacerb_data[102]=(0-0.052893829053277115)/0.2238215179587517

          //(temperature)
        //  console.log("temp" +temperature)
          if(temperature_val==0){
            triage_data[103]= 1   // (temp_nan)
            triage_data[104]= 0   // (temp_(101.6, 120])
            triage_data[105]= 0   // (temp_(100.4, 101.6])
            triage_data[106]= 0   // (temp_(97, 100.4])

            exacerb_data[103]=(1-0.34266002299731696)/0.4745989165988427
            exacerb_data[104]=(0-0.008815638175546186)/0.0934768564892083
            exacerb_data[105]=(0-0.040628593330778076)/0.19742824198867884
            exacerb_data[106]=(0-0.6078957454963587)/0.2238215179587517
          }
          else if(temperature_val > 47 && temperature_val  <= 120){
            triage_data[103]= 0   // (temp_nan)
            triage_data[104]= 1   // (temp_(101.6, 120])
            triage_data[105]= 0   // (temp_(100.4, 101.6])
            triage_data[106]= 0   // (temp_(97, 100.4])

            exacerb_data[103]=(0-0.34266002299731696)/0.4745989165988427
            exacerb_data[104]=(1-0.008815638175546186)/0.0934768564892083
            exacerb_data[105]=(0-0.040628593330778076)/0.19742824198867884
            exacerb_data[106]=(0-0.6078957454963587)/0.2238215179587517
          }
          else if(temperature_val  > 35 && temperature_val  <= 47){
            triage_data[103]= 0  // (temp_nan)
            triage_data[104]= 0   // (temp_(101.6, 120])
            triage_data[105]= 1   // (temp_(100.4, 101.6])
            triage_data[106]= 0   // (temp_(97, 100.4])

            exacerb_data[103]=(0-0.34266002299731696)/0.4745989165988427
            exacerb_data[104]=(0-0.008815638175546186)/0.0934768564892083
            exacerb_data[105]=(1-0.040628593330778076)/0.19742824198867884
            exacerb_data[106]=(0-0.6078957454963587)/0.2238215179587517
          }
          else if(temperature_val  > 1 && temperature_val  <= 35){
            triage_data[103]= 0   // (temp_nan)
            triage_data[104]= 0   // (temp_(101.6, 120])
            triage_data[105]= 0   // (temp_(100.4, 101.6])
            triage_data[106]= 1   // (temp_(97, 100.4])

            exacerb_data[103]=(0-0.34266002299731696)/0.4745989165988427
            exacerb_data[104]=(0-0.008815638175546186)/0.0934768564892083
            exacerb_data[105]=(0-0.040628593330778076)/0.19742824198867884
            exacerb_data[106]=(1-0.6078957454963587)/0.2238215179587517
          }

          profile_data[35]= 1   //(basefev_nan)
          profile_data[36]= 0   //(basefev_(70, 100])
          profile_data[37]= 0   //(basefev_(50, 70])
          profile_data[38]= 0   //(basefev_(30, 50])
          profile_data[39]= 0   //(basefev_(0, 30])

          baseline_profile_oxygen_sat=((baseline_oxygen_sat*baseline_oxygen_sat_count)+oxygen_sat)/(baseline_oxygen_sat_count+1);
          baseline_profile_heart_rate=((baseline_heart_rate*baseline_heart_rate_count)+heart_rate)/(baseline_heart_rate_count+1);;

          if(heart_rate==0){
            profile_data[40]= 1  // (basehr_nan)
            profile_data[41]= 0   //(basehr_(100, 250])
            profile_data[42]= 0   //(basehr_(80, 100])
            profile_data[43]= 0   //(basehr_(60, 80])
            profile_data[44]= 0   //(basehr_(0, 60])

            options_arr.push({var_name: 'basehr_nan',value: 1,order:40});
            options_arr.push({var_name: 'basehr_100_250',value: 0,order:41});
            options_arr.push({var_name: 'basehr_80_100',value: 0,order:42});
            options_arr.push({var_name: 'basehr_60_80',value: 0,order:43});
            options_arr.push({var_name: 'basehr_0_60',value: 0,order:44});
          }
          else if(baseline_profile_heart_rate  >= 100 && baseline_profile_heart_rate  < 250){
            profile_data[40]= 0  // (basehr_nan)
            profile_data[41]= 1   //(basehr_(100, 250])
            profile_data[42]= 0   //(basehr_(80, 100])
            profile_data[43]= 0   //(basehr_(60, 80])
            profile_data[44]= 0   //(basehr_(0, 60])

            options_arr.push({var_name: 'basehr_nan',value: 0,order:40});
            options_arr.push({var_name: 'basehr_100_250',value: 1,order:41});
            options_arr.push({var_name: 'basehr_80_100',value: 0,order:42});
            options_arr.push({var_name: 'basehr_60_80',value: 0,order:43});
            options_arr.push({var_name: 'basehr_0_60',value: 0,order:44});
          }
          else if(baseline_profile_heart_rate  >= 80 && baseline_profile_heart_rate  < 100){
            profile_data[40]= 0  // (basehr_nan)
            profile_data[41]= 0   //(basehr_(100, 250])
            profile_data[42]= 1   //(basehr_(80, 100])
            profile_data[43]= 0   //(basehr_(60, 80])
            profile_data[44]= 0   //(basehr_(0, 60])

            options_arr.push({var_name: 'basehr_nan',value: 0,order:40});
            options_arr.push({var_name: 'basehr_100_250',value: 0,order:41});
            options_arr.push({var_name: 'basehr_80_100',value: 1,order:42});
            options_arr.push({var_name: 'basehr_60_80',value: 0,order:43});
            options_arr.push({var_name: 'basehr_0_60',value: 0,order:44});
          }
          else if(baseline_profile_heart_rate  >= 60 && baseline_profile_heart_rate  < 80){
            profile_data[40]= 0  // (basehr_nan)
            profile_data[41]= 0   //(basehr_(100, 250])
            profile_data[42]= 0   //(basehr_(80, 100])
            profile_data[43]= 1   //(basehr_(60, 80])
            profile_data[44]= 0   //(basehr_(0, 60])

            options_arr.push({var_name: 'basehr_nan',value: 0,order:40});
            options_arr.push({var_name: 'basehr_100_250',value: 0,order:41});
            options_arr.push({var_name: 'basehr_80_100',value: 0,order:42});
            options_arr.push({var_name: 'basehr_60_80',value: 1,order:43});
            options_arr.push({var_name: 'basehr_0_60',value: 0,order:44});
          }
          else if(baseline_profile_heart_rate  >= 0 && baseline_profile_heart_rate  < 60){
            profile_data[40]= 0  // (basehr_nan)
            profile_data[41]= 0   //(basehr_(100, 250])
            profile_data[42]= 0   //(basehr_(80, 100])
            profile_data[43]= 0   //(basehr_(60, 80])
            profile_data[44]= 1   //(basehr_(0, 60])

            options_arr.push({var_name: 'basehr_nan',value: 0,order:40});
            options_arr.push({var_name: 'basehr_100_250',value: 0,order:41});
            options_arr.push({var_name: 'basehr_80_100',value: 0,order:42});
            options_arr.push({var_name: 'basehr_60_80',value: 0,order:43});
            options_arr.push({var_name: 'basehr_0_60',value: 1,order:44});
          }


          if(oxygen_sat==0){
            profile_data[45]= 1   //(baseox_nan)
            profile_data[46]= 0   //(baseox_(93, 100])
            profile_data[47]= 0   //(baseox_(91, 93])
            profile_data[48]= 0   //(baseox_(89, 91])
            profile_data[49]= 0   //(baseox_(85, 89])
            profile_data[50]= 0   //(baseox_(0, 85])

            options_arr.push({var_name: 'baseox_nan',value: 1,order:45});
            options_arr.push({var_name: 'baseox_93_100',value: 0,order:46});
            options_arr.push({var_name: 'baseox_91_93',value: 0,order:47});
            options_arr.push({var_name: 'baseox_89_91',value: 0,order:48});
            options_arr.push({var_name: 'baseox_85_89',value: 0,order:49});
            options_arr.push({var_name: 'baseox_0_85',value: 0,order:50});
          }
          else if(baseline_profile_oxygen_sat  >= 93 && baseline_profile_oxygen_sat  < 100){
            profile_data[45]= 0   //(baseox_nan)
            profile_data[46]= 1   //(baseox_(93, 100])
            profile_data[47]= 0   //(baseox_(91, 93])
            profile_data[48]= 0   //(baseox_(89, 91])
            profile_data[49]= 0   //(baseox_(85, 89])
            profile_data[50]= 0   //(baseox_(0, 85])

            options_arr.push({var_name: 'baseox_nan',value: 0,order:45});
            options_arr.push({var_name: 'baseox_93_100',value: 1,order:46});
            options_arr.push({var_name: 'baseox_91_93',value: 0,order:47});
            options_arr.push({var_name: 'baseox_89_91',value: 0,order:48});
            options_arr.push({var_name: 'baseox_85_89',value: 0,order:49});
            options_arr.push({var_name: 'baseox_0_85',value: 0,order:50});
          }
          else if(baseline_profile_oxygen_sat  >= 91 && baseline_profile_oxygen_sat  < 93){
            profile_data[45]= 0   //(baseox_nan)
            profile_data[46]= 0   //(baseox_(93, 100])
            profile_data[47]= 1   //(baseox_(91, 93])
            profile_data[48]= 0   //(baseox_(89, 91])
            profile_data[49]= 0   //(baseox_(85, 89])
            profile_data[50]= 0   //(baseox_(0, 85])

            options_arr.push({var_name: 'baseox_nan',value: 0,order:45});
            options_arr.push({var_name: 'baseox_93_100',value: 0,order:46});
            options_arr.push({var_name: 'baseox_91_93',value: 1,order:47});
            options_arr.push({var_name: 'baseox_89_91',value: 0,order:48});
            options_arr.push({var_name: 'baseox_85_89',value: 0,order:49});
            options_arr.push({var_name: 'baseox_0_85',value: 0,order:50});
          }
          else if(baseline_profile_oxygen_sat  >= 89 && baseline_profile_oxygen_sat  < 91){
            profile_data[45]= 0   //(baseox_nan)
            profile_data[46]= 0   //(baseox_(93, 100])
            profile_data[47]= 0   //(baseox_(91, 93])
            profile_data[48]= 1   //(baseox_(89, 91])
            profile_data[49]= 0   //(baseox_(85, 89])
            profile_data[50]= 0   //(baseox_(0, 85])

            options_arr.push({var_name: 'baseox_nan',value: 0,order:45});
            options_arr.push({var_name: 'baseox_93_100',value: 0,order:46});
            options_arr.push({var_name: 'baseox_91_93',value: 0,order:47});
            options_arr.push({var_name: 'baseox_89_91',value: 1,order:48});
            options_arr.push({var_name: 'baseox_85_89',value: 0,order:49});
            options_arr.push({var_name: 'baseox_0_85',value: 0,order:50});
          }
          else if(baseline_profile_oxygen_sat  >= 85 && baseline_profile_oxygen_sat  < 89){
            profile_data[45]= 0   //(baseox_nan)
            profile_data[46]= 0   //(baseox_(93, 100])
            profile_data[47]= 0   //(baseox_(91, 93])
            profile_data[48]= 0   //(baseox_(89, 91])
            profile_data[49]= 1   //(baseox_(85, 89])
            profile_data[50]= 0   //(baseox_(0, 85])

            options_arr.push({var_name: 'baseox_nan',value: 0,order:45});
            options_arr.push({var_name: 'baseox_93_100',value: 0,order:46});
            options_arr.push({var_name: 'baseox_91_93',value: 0,order:47});
            options_arr.push({var_name: 'baseox_89_91',value: 0,order:48});
            options_arr.push({var_name: 'baseox_85_89',value: 1,order:49});
            options_arr.push({var_name: 'baseox_0_85',value: 0,order:50});
          }
          else if(baseline_profile_oxygen_sat  >= 0 && baseline_profile_oxygen_sat  < 85){
            profile_data[45]= 0   //(baseox_nan)
            profile_data[46]= 0   //(baseox_(93, 100])
            profile_data[47]= 0   //(baseox_(91, 93])
            profile_data[48]= 0   //(baseox_(89, 91])
            profile_data[49]= 0   //(baseox_(85, 89])
            profile_data[50]= 1   //(baseox_(0, 85])

            options_arr.push({var_name: 'baseox_nan',value: 0,order:45});
            options_arr.push({var_name: 'baseox_93_100',value: 0,order:46});
            options_arr.push({var_name: 'baseox_91_93',value: 0,order:47});
            options_arr.push({var_name: 'baseox_89_91',value: 0,order:48});
            options_arr.push({var_name: 'baseox_85_89',value: 0,order:49});
            options_arr.push({var_name: 'baseox_0_85',value: 1,order:50});
          }

          //console.log(triage_data);
          if(status==0){
              if(move_last==1 && this.qno==1){
              //console.log(this.qno);
              this.setState({ screentype: 'MedicalInAttentionScreen' })
              //console.log("API Call");
              _that.validationAndApiParameter('python_triage');
            }
            else{
              this.qno++
              this.setState({ qus_no: this.qno })
              this.setState({ countCheck: [], question: arrnew[this.qno].question,qus_id: arrnew[this.qno].qus_id, options: arrnew[this.qno].options,type: arrnew[this.qno].type, suboption : arrnew[this.qno].suboption,modal_title : arrnew[this.qno].modal_title})
            }
          }
          else if(status==1){
            this.setState({ screentype: 'MedicalAttentionScreen' })
          //  console.log("API Call 2");
            _that.validationAndApiParameter('python_triage');
            //_that.props.navigation.navigate('MedicalAttentionScreen');
          }
      }
    else if(this.qno < arrnew.length-1){

      if(this.qno == 0){
        if(countCheck.indexOf('rws_1')!=-1){
          triage_data[35]=1
          exacerb_data[35]=(1-0.07435799156765044)/0.2623525884333442
          /*options_arr.push({
              var_name: 'rws_1',
              value: 1,
              order:1,
          });*/
          triage_data[42]= 1   //(shofb_0)
          triage_data[44]= 1   //(cough_0)
          triage_data[46]= 1   //(wheez_0)
          triage_data[48]= 1   //(sputum_1)

          exacerb_data[42]=(1-0.4722115753162131)/0.49922720624340333
          exacerb_data[44]=(1-0.47719432732847833)/0.4994796305095929
          exacerb_data[46]=(1-0.7366807205825987)/0.44043414547977755
          exacerb_data[48]=(1-0.4465312380222307)/0.4971328710642304

          if(triage_data[25]==1){
            triage_data[52]= 1   //(cur_dyspnea_1)
            triage_data[53]= 0   //(cur_dyspnea_2)
            triage_data[54]= 0   //(cur_dyspnea_3)  Breathlessness during activity (same order as in the question)
            triage_data[55]= 0   //(cur_dyspnea_4)
            triage_data[56]= 0   //(cur_dyspnea_5)

            exacerb_data[52]=(1-0.09083940206975853)/0.28738059277092337
            exacerb_data[53]=(0-0.16558068225373707)/0.3717038067038378
            exacerb_data[54]=(0-0.23955538520505942)/0.42681213973399934
            exacerb_data[55]=(0-0.23380605596013798)/0.4232502618504834
            exacerb_data[56]=(0-0.27021847451130704)/0.44407257351033186

            triage_data[25]=1   // base_dyspnea_1
            triage_data[26]=0   //base_dyspnea_2
            triage_data[27]=0
            triage_data[28]=0
            triage_data[29]=0

            exacerb_data[25]=(1-0.11881947106170947)/0.3235759638142566
            exacerb_data[26]=(0-0.18321195860482944)/0.3868401954670839
            exacerb_data[27]=(0-0.29091605979302415)/0.4541848808002373
            exacerb_data[28]=(0-0.22844001533154465)/0.41982755355844414
            exacerb_data[29]=(0.1786124952088923)/0.38302750784264267
          }
          else if(triage_data[26]==1){
            triage_data[52]= 0   //(cur_dyspnea_1)
            triage_data[53]= 1   //(cur_dyspnea_2)
            triage_data[54]= 0   //(cur_dyspnea_3)  Breathlessness during activity (same order as in the question)
            triage_data[55]= 0   //(cur_dyspnea_4)
            triage_data[56]= 0   //(cur_dyspnea_5)

            exacerb_data[52]=(0-0.09083940206975853)/0.28738059277092337
            exacerb_data[53]=(1-0.16558068225373707)/0.3717038067038378
            exacerb_data[54]=(0-0.23955538520505942)/0.42681213973399934
            exacerb_data[55]=(0-0.23380605596013798)/0.4232502618504834
            exacerb_data[56]=(0-0.27021847451130704)/0.44407257351033186

            triage_data[25]=0
            triage_data[26]=1
            triage_data[27]=0
            triage_data[28]=0
            triage_data[29]=0

            exacerb_data[25]=(0-0.11881947106170947)/0.3235759638142566
            exacerb_data[26]=(1-0.18321195860482944)/0.3868401954670839
            exacerb_data[27]=(0-0.29091605979302415)/0.4541848808002373
            exacerb_data[28]=(0-0.22844001533154465)/0.41982755355844414
            exacerb_data[29]=(0.1786124952088923)/0.38302750784264267
          }
          else if(triage_data[27]==1){
            triage_data[52]= 0   //(cur_dyspnea_1)
            triage_data[53]= 0   //(cur_dyspnea_2)
            triage_data[54]= 1   //(cur_dyspnea_3)  Breathlessness during activity (same order as in the question)
            triage_data[55]= 0   //(cur_dyspnea_4)
            triage_data[56]= 0   //(cur_dyspnea_5)

            exacerb_data[52]=(0-0.09083940206975853)/0.28738059277092337
            exacerb_data[53]=(0-0.16558068225373707)/0.3717038067038378
            exacerb_data[54]=(1-0.23955538520505942)/0.42681213973399934
            exacerb_data[55]=(0-0.23380605596013798)/0.4232502618504834
            exacerb_data[56]=(0-0.27021847451130704)/0.44407257351033186

            triage_data[25]=0
            triage_data[26]=0
            triage_data[27]=1
            triage_data[28]=0
            triage_data[29]=0

            exacerb_data[25]=(0-0.11881947106170947)/0.3235759638142566
            exacerb_data[26]=(0-0.18321195860482944)/0.3868401954670839
            exacerb_data[27]=(1-0.29091605979302415)/0.4541848808002373
            exacerb_data[28]=(0-0.22844001533154465)/0.41982755355844414
            exacerb_data[29]=(0.1786124952088923)/0.38302750784264267
          }
          else if(triage_data[28]==1){
            triage_data[52]= 0   //(cur_dyspnea_1)
            triage_data[53]= 0   //(cur_dyspnea_2)
            triage_data[54]= 0   //(cur_dyspnea_3)  Breathlessness during activity (same order as in the question)
            triage_data[55]= 1   //(cur_dyspnea_4)
            triage_data[56]= 0   //(cur_dyspnea_5)

            exacerb_data[52]=(0-0.09083940206975853)/0.28738059277092337
            exacerb_data[53]=(0-0.16558068225373707)/0.3717038067038378
            exacerb_data[54]=(0-0.23955538520505942)/0.42681213973399934
            exacerb_data[55]=(1-0.23380605596013798)/0.4232502618504834
            exacerb_data[56]=(0-0.27021847451130704)/0.44407257351033186

            triage_data[25]=0
            triage_data[26]=0
            triage_data[27]=0
            triage_data[28]=1
            triage_data[29]=0

            exacerb_data[25]=(0-0.11881947106170947)/0.3235759638142566
            exacerb_data[26]=(0-0.18321195860482944)/0.3868401954670839
            exacerb_data[27]=(0-0.29091605979302415)/0.4541848808002373
            exacerb_data[28]=(1-0.22844001533154465)/0.41982755355844414
            exacerb_data[29]=(0.1786124952088923)/0.38302750784264267
          }
          else if(triage_data[29]==1){
            triage_data[52]= 0   //(cur_dyspnea_1)
            triage_data[53]= 0   //(cur_dyspnea_2)
            triage_data[54]= 0   //(cur_dyspnea_3)  Breathlessness during activity (same order as in the question)
            triage_data[55]= 0   //(cur_dyspnea_4)
            triage_data[56]= 1   //(cur_dyspnea_5)

            exacerb_data[52]=(0-0.09083940206975853)/0.28738059277092337
            exacerb_data[53]=(0-0.16558068225373707)/0.3717038067038378
            exacerb_data[54]=(0-0.23955538520505942)/0.42681213973399934
            exacerb_data[55]=(0-0.23380605596013798)/0.4232502618504834
            exacerb_data[56]=(1-0.27021847451130704)/0.44407257351033186

            triage_data[25]=0
            triage_data[26]=0
            triage_data[27]=0
            triage_data[28]=0
            triage_data[29]=1

            exacerb_data[25]=(0-0.11881947106170947)/0.3235759638142566
            exacerb_data[26]=(0-0.18321195860482944)/0.3868401954670839
            exacerb_data[27]=(0-0.29091605979302415)/0.4541848808002373
            exacerb_data[28]=(0-0.22844001533154465)/0.41982755355844414
            exacerb_data[29]=(1.1786124952088923)/0.38302750784264267
          }

        }
        else {
          triage_data[35]=0
          exacerb_data[35]=(0-0.07435799156765044)/0.2623525884333442
          /*options_arr.push({
              var_name: 'rws_1',
              value: 0,
              order:1,
          });*/
        }

        if(countCheck.indexOf('rws_2')!=-1){
          triage_data[36]=1
          exacerb_data[36]=(1-0.3123802223073975)/0.4634639349707574

        }
        else {
          triage_data[36]=0
          exacerb_data[36]=(0-0.3123802223073975)/0.4634639349707574

        }

        if(countCheck.indexOf('rws_3')!=-1){
          triage_data[37]=1
          exacerb_data[37]=(1-0.30663089306247604)/0.4610947716925278

        }
        else {
          triage_data[37]=0
          exacerb_data[37]=(0-0.30663089306247604)/0.4610947716925278

        }

        if(countCheck.indexOf('rws_4')!=-1){
          triage_data[38]=1
          exacerb_data[38]=(1-0.30663089306247604)/0.46109477169252794

        }
        else {
          triage_data[38]=0
          exacerb_data[38]=(0-0.30663089306247604)/0.46109477169252794

        }

        // if rws_1 (No selected)
        if(countCheck.indexOf('rws_1')!=-1){
          triage_data[39]= 1   //(cmtolw_nan)
          exacerb_data[39]=(1-0.0778075891146033)/0.267868565141146
        }
        else{
          triage_data[39]= 0   //(cmtolw_nan)
          exacerb_data[39]=(0-0.0778075891146033)/0.267868565141146
        }

        triage_data[40]= 0   //(cmtolw_0)
        triage_data[41]= 0   //(cmtolw_1)

        exacerb_data[40]=(0-0.6910693752395554)/0.46205247953621675
        exacerb_data[41]=(0-0.23112303564584133)/0.4215509198657885

      }
      else if(this.qno == 2){
          //Cough
          //console.log(countCheck)
          //console.log(countCheck.indexOf('cough'))

        if(countCheck.indexOf('cough')!=-1){
          triage_data[44]= 0  // (cough_0)    | Cough, same categories as Short of Breath			| Compound | =
          triage_data[45]= 1   //(cough_1)  	|								| Compound | "Please select


          exacerb_data[44]=(0-0.47719432732847833)/0.4994796305095929
          exacerb_data[45]=(1-0.5228056726715217)/0.4994796305095929
        }
        else {
          triage_data[44]= 1  // (cough_0)    | Cough, same categories as Short of Breath			| Compound | =
          triage_data[45]= 0   //(cough_1)  	|								| Compound | "Please select

          exacerb_data[44]=(1-0.47719432732847833)/0.4994796305095929
          exacerb_data[45]=(0-0.5228056726715217)/0.4994796305095929
        }

        // Wheezing
        if(countCheck.indexOf('wheez')!=-1){
          triage_data[46]= 0  //(wheez_0)    | Wheezing, same categories as Short of Breath				| Compound | which of the
          triage_data[47]= 1   //(wheez_1)  	|									| Compound | following are


          exacerb_data[46]=(0-0.7366807205825987)/0.44043414547977755
          exacerb_data[47]=(1-0.2633192794174013)/0.44043414547977755
        }
        else {
          triage_data[46]= 1  // (wheez_0)    | Wheezing, same categories as Short of Breath				| Compound | which of the
          triage_data[47]= 0   // (wheez_1)  	|									| Compound | following are

          exacerb_data[46]=(1-0.7366807205825987)/0.44043414547977755
          exacerb_data[47]=(0-0.2633192794174013)/0.44043414547977755
        }

        // mucus/phlegm discoloration  mucus/phlegm volume
        if(countCheck.indexOf('mucus_discoloration')!=-1 && countCheck.indexOf('mucus_volume')!=-1){
          triage_data[48]= 0   //(sputum_1)   | NONE of Mucus/Phlegm discoloration/volume selected		| Compound | worse than usual?"
          triage_data[49]= 0   //(sputum_2)   | Mucus/Phlegm volume selected 					| Compound | question in Am I OK
          triage_data[50]= 0   //(sputum_3)   | Mucus/Phlegm discoloration selected				| Compound | process
          triage_data[51]= 1   //(sputum_4)   | BOTH discoloration AND volume selected			| Compound |


          exacerb_data[48]=(0-0.4465312380222307)/0.4971328710642304
          exacerb_data[49]=(0-0.1766960521272518)/0.38141127053339063
          exacerb_data[50]=(0-0.19126101954771943)/0.393294090915801
          exacerb_data[51]=(1-0.18551169030279802)/0.3887121082032264

        }else if(countCheck.indexOf('mucus_discoloration')!=-1){
          triage_data[48]= 0   //(sputum_1)   | NONE of Mucus/Phlegm discoloration/volume selected		| Compound | worse than usual?"
          triage_data[49]= 0   //(sputum_2)   | Mucus/Phlegm volume selected 					| Compound | question in Am I OK
          triage_data[50]= 1   //(sputum_3)   | Mucus/Phlegm discoloration selected				| Compound | process
          triage_data[51]= 0   //(sputum_4)   | BOTH discoloration AND volume selected			| Compound |


          exacerb_data[48]=(0-0.4465312380222307)/0.4971328710642304
          exacerb_data[49]=(0-0.1766960521272518)/0.38141127053339063
          exacerb_data[50]=(1-0.19126101954771943)/0.393294090915801
          exacerb_data[51]=(0-0.18551169030279802)/0.3887121082032264

        }else if(countCheck.indexOf('mucus_volume')!=-1){
          triage_data[48]= 0   //(sputum_1)   | NONE of Mucus/Phlegm discoloration/volume selected		| Compound | worse than usual?"
          triage_data[49]= 1   //(sputum_2)   | Mucus/Phlegm volume selected 					| Compound | question in Am I OK
          triage_data[50]= 0   //(sputum_3)   | Mucus/Phlegm discoloration selected				| Compound | process
          triage_data[51]= 0   //(sputum_4)   | BOTH discoloration AND volume selected			| Compound |


          exacerb_data[48]=(0-0.4465312380222307)/0.4971328710642304
          exacerb_data[49]=(1-0.1766960521272518)/0.38141127053339063
          exacerb_data[50]=(0-0.19126101954771943)/0.393294090915801
          exacerb_data[51]=(0-0.18551169030279802)/0.3887121082032264
        }else{
          triage_data[48]= 1   //(sputum_1)   | NONE of Mucus/Phlegm discoloration/volume selected		| Compound | worse than usual?"
          triage_data[49]= 0   //(sputum_2)   | Mucus/Phlegm volume selected 					| Compound | question in Am I OK
          triage_data[50]= 0   //(sputum_3)   | Mucus/Phlegm discoloration selected				| Compound | process
          triage_data[51]= 0   //(sputum_4)   | BOTH discoloration AND volume selected			| Compound |


          exacerb_data[48]=(1-0.4465312380222307)/0.4971328710642304
          exacerb_data[49]=(0-0.1766960521272518)/0.38141127053339063
          exacerb_data[50]=(0-0.19126101954771943)/0.393294090915801
          exacerb_data[51]=(0-0.18551169030279802)/0.3887121082032264
        }

        //Runny nose, sore throat or cold symptoms
        if(countCheck.indexOf('runny_nose')!=-1){
          triage_data[57]=1
          exacerb_data[57]=(1-0.12533537753928709)/0.3310988080262015

        }
        else {
          triage_data[57]=0
          exacerb_data[57]=(0-0.12533537753928709)/0.3310988080262015
        }

        //Waking up at night from breathing  symptoms
        if(countCheck.indexOf('respiratory_symptoms')!=-1){
          triage_data[58]=1
          exacerb_data[58]=(1-0.15638175546186278)/0.363216880142607

        }
        else {
          triage_data[58]=0
          exacerb_data[58]=(0-0.15638175546186278)/0.363216880142607
        }
//console.log(triage_data)
      }
      if(this.qno == 3){
        //shofb_1
        if(countCheck.indexOf('shofb_1')!=-1){
          triage_data[42]= 0   //(shofb_0)  | not selected = NOT worse		Short of Breath			| Compound |
          triage_data[43]= 1   //(shofb_1)  | selected = worse	 						| Compound | *
          //options_arr.push({var_name: 'shofb_1',value: 1,order:4 });
          exacerb_data[42]=(0-0.4722115753162131)/0.49922720624340333
          exacerb_data[43]=(1-0.5277884246837868)/0.49922720624340333
        }
        else {
          triage_data[42]= 1   //(shofb_0)  | not selected = NOT worse		Short of Breath			| Compound |
          triage_data[43]= 0   //(shofb_1)  | selected = worse	 						| Compound | *
          //options_arr.push({var_name: 'shofb_1',value: 0,order:4 });
          exacerb_data[42]=(1-0.4722115753162131)/0.49922720624340333
          exacerb_data[43]=(0-0.5277884246837868)/0.49922720624340333
        }

      }

        this.setState({ countCheck: [] })
        this.qno++
        this.setState({ qus_no: this.qno })
        if(this.qno==3 && countCheck.indexOf('shofb') == -1){
          this.qno++
          this.setState({ qus_no: this.qno })
          //this.setState({ screentype: 'MedicalInAttentionScreen' })
          //_that.validationAndApiParameter('python_triage');
        }
        console.log(this.qno)
        this.setState({ countCheck: [], question: arrnew[this.qno].question, qus_id: arrnew[this.qno].qus_id,options: arrnew[this.qno].options, type: arrnew[this.qno].type, suboption : arrnew[this.qno].suboption,modal_title : arrnew[this.qno].modal_title})

      }
    else {
      if(this.qno == 4){
          if(countCheck.indexOf('cur_dyspnea_1')!=-1){
            triage_data[52]= 1   //(cur_dyspnea_1)
            triage_data[53]= 0   //(cur_dyspnea_2)
            triage_data[54]= 0   //(cur_dyspnea_3)  Breathlessness during activity (same order as in the question)
            triage_data[55]= 0   //(cur_dyspnea_4)
            triage_data[56]= 0   //(cur_dyspnea_5)

            exacerb_data[52]=(1-0.09083940206975853)/0.28738059277092337
            exacerb_data[53]=(0-0.16558068225373707)/0.3717038067038378
            exacerb_data[54]=(0-0.23955538520505942)/0.42681213973399934
            exacerb_data[55]=(0-0.23380605596013798)/0.4232502618504834
            exacerb_data[56]=(0-0.27021847451130704)/0.44407257351033186

            triage_data[25]=1   // base_dyspnea_1
            triage_data[26]=0   //base_dyspnea_2
            triage_data[27]=0
            triage_data[28]=0
            triage_data[29]=0

            exacerb_data[25]=(1-0.11881947106170947)/0.3235759638142566
            exacerb_data[26]=(0-0.18321195860482944)/0.3868401954670839
            exacerb_data[27]=(0-0.29091605979302415)/0.4541848808002373
            exacerb_data[28]=(0-0.22844001533154465)/0.41982755355844414
            exacerb_data[29]=(0.1786124952088923)/0.38302750784264267
          }
          else if(countCheck.indexOf('cur_dyspnea_2')!=-1){
            triage_data[52]= 0   //(cur_dyspnea_1)
            triage_data[53]= 1   //(cur_dyspnea_2)
            triage_data[54]= 0   //(cur_dyspnea_3)  Breathlessness during activity (same order as in the question)
            triage_data[55]= 0   //(cur_dyspnea_4)
            triage_data[56]= 0   //(cur_dyspnea_5)

            exacerb_data[52]=(0-0.09083940206975853)/0.28738059277092337
            exacerb_data[53]=(1-0.16558068225373707)/0.3717038067038378
            exacerb_data[54]=(0-0.23955538520505942)/0.42681213973399934
            exacerb_data[55]=(0-0.23380605596013798)/0.4232502618504834
            exacerb_data[56]=(0-0.27021847451130704)/0.44407257351033186

            triage_data[25]=0
            triage_data[26]=1
            triage_data[27]=0
            triage_data[28]=0
            triage_data[29]=0

            exacerb_data[25]=(0-0.11881947106170947)/0.3235759638142566
            exacerb_data[26]=(1-0.18321195860482944)/0.3868401954670839
            exacerb_data[27]=(0-0.29091605979302415)/0.4541848808002373
            exacerb_data[28]=(0-0.22844001533154465)/0.41982755355844414
            exacerb_data[29]=(0.1786124952088923)/0.38302750784264267
          }
          else if(countCheck.indexOf('cur_dyspnea_3')!=-1){
            triage_data[52]= 0   //(cur_dyspnea_1)
            triage_data[53]= 0   //(cur_dyspnea_2)
            triage_data[54]= 1   //(cur_dyspnea_3)  Breathlessness during activity (same order as in the question)
            triage_data[55]= 0   //(cur_dyspnea_4)
            triage_data[56]= 0   //(cur_dyspnea_5)

            exacerb_data[52]=(0-0.09083940206975853)/0.28738059277092337
            exacerb_data[53]=(0-0.16558068225373707)/0.3717038067038378
            exacerb_data[54]=(1-0.23955538520505942)/0.42681213973399934
            exacerb_data[55]=(0-0.23380605596013798)/0.4232502618504834
            exacerb_data[56]=(0-0.27021847451130704)/0.44407257351033186

            triage_data[25]=0
            triage_data[26]=0
            triage_data[27]=1
            triage_data[28]=0
            triage_data[29]=0

            exacerb_data[25]=(0-0.11881947106170947)/0.3235759638142566
            exacerb_data[26]=(0-0.18321195860482944)/0.3868401954670839
            exacerb_data[27]=(1-0.29091605979302415)/0.4541848808002373
            exacerb_data[28]=(0-0.22844001533154465)/0.41982755355844414
            exacerb_data[29]=(0.1786124952088923)/0.38302750784264267
          }
          else if(countCheck.indexOf('cur_dyspnea_4')!=-1){
            triage_data[52]= 0   //(cur_dyspnea_1)
            triage_data[53]= 0   //(cur_dyspnea_2)
            triage_data[54]= 0   //(cur_dyspnea_3)  Breathlessness during activity (same order as in the question)
            triage_data[55]= 1   //(cur_dyspnea_4)
            triage_data[56]= 0   //(cur_dyspnea_5)

            exacerb_data[52]=(0-0.09083940206975853)/0.28738059277092337
            exacerb_data[53]=(0-0.16558068225373707)/0.3717038067038378
            exacerb_data[54]=(0-0.23955538520505942)/0.42681213973399934
            exacerb_data[55]=(1-0.23380605596013798)/0.4232502618504834
            exacerb_data[56]=(0-0.27021847451130704)/0.44407257351033186

            triage_data[25]=0
            triage_data[26]=0
            triage_data[27]=0
            triage_data[28]=1
            triage_data[29]=0

            exacerb_data[25]=(0-0.11881947106170947)/0.3235759638142566
            exacerb_data[26]=(0-0.18321195860482944)/0.3868401954670839
            exacerb_data[27]=(0-0.29091605979302415)/0.4541848808002373
            exacerb_data[28]=(1-0.22844001533154465)/0.41982755355844414
            exacerb_data[29]=(0.1786124952088923)/0.38302750784264267
          }
          else if(countCheck.indexOf('cur_dyspnea_5')!=-1){
            triage_data[52]= 0   //(cur_dyspnea_1)
            triage_data[53]= 0   //(cur_dyspnea_2)
            triage_data[54]= 0   //(cur_dyspnea_3)  Breathlessness during activity (same order as in the question)
            triage_data[55]= 0   //(cur_dyspnea_4)
            triage_data[56]= 1   //(cur_dyspnea_5)

            exacerb_data[52]=(0-0.09083940206975853)/0.28738059277092337
            exacerb_data[53]=(0-0.16558068225373707)/0.3717038067038378
            exacerb_data[54]=(0-0.23955538520505942)/0.42681213973399934
            exacerb_data[55]=(0-0.23380605596013798)/0.4232502618504834
            exacerb_data[56]=(1-0.27021847451130704)/0.44407257351033186

            triage_data[25]=0
            triage_data[26]=0
            triage_data[27]=0
            triage_data[28]=0
            triage_data[29]=1

            exacerb_data[25]=(0-0.11881947106170947)/0.3235759638142566
            exacerb_data[26]=(0-0.18321195860482944)/0.3868401954670839
            exacerb_data[27]=(0-0.29091605979302415)/0.4541848808002373
            exacerb_data[28]=(0-0.22844001533154465)/0.41982755355844414
            exacerb_data[29]=(1.1786124952088923)/0.38302750784264267
          }
        }

      console.log(triage_data);
      this.setState({ screentype: 'MedicalInAttentionScreen' })
    //  console.log("API CALL")
      _that.validationAndApiParameter('python_triage');


      //console.log(triage_data);
      }

  }

  _answer(status,ans){
    if(this.state.type=="multiple"){
      if(status == false){
        ans_track.push(ans);
      }
      else{
        var index = ans_track.indexOf(ans);
        if (index !== -1) ans_track.splice(index, 1);
        //ans_track.splice(-1,1);
      }
    }
    else if(this.state.type=="single"){
      ans_track.splice(-1,1);
      ans_track.push(ans);
    }

    current_ans=ans;
    this.setState({ countCheck: ans_track })
    //console.log(this.state.countCheck)
  }

  back_click() {
    const resetAction = NavigationActions.navigate({ routeName: 'StartScreen'})
    _that.props.navigation.dispatch(resetAction);
  }


  render() {
    const headerProp = {
      title: 'Assessing your health!',
      screens: 'AmIOkQusScreen',
    };

    let _this = this
      const currentOptions = this.state.options
      const options = Object.keys(currentOptions).map( function(k) {
        if(k==current_ans){status=true}else{status=false}
        return (  <View key={k} style={{margin:5, }}>
          { _this.state.type=='multiple' ?
            <Animbutton onColor={"#2c808f"} effect={"pulse"}  _onPress={(status) => _this._answer(status,k)} text={currentOptions[k]} />
          :
            <AnimSinglebutton onColor={"#2c808f"} effect={"pulse"} status={status} _onPress={(status) => _this._answer(status,k)} text={currentOptions[k]} />
          }

        </View>)
      });

      var DropDown=<View style={{margin:5, }}>
       {/* <Text style={styles.dropdowntext}>Oxygen Saturation </Text> */}
        <Picker PickerData={this.state.items1}
         selectedValue={85}
         labelIcon={oxy_icon}
         styleImage={styles.imageContainer}
         placeholder={"Select Your Oxygen Saturation"}
         title={"Oxygen Saturation"}
         getTxt={(val,label)=>this.setState({oxygen_sat:val})}/>

         <View style={{ paddingVertical: AppSizes.ResponsiveSize.Padding(2) }} />
         {/*   <Text style={styles.dropdowntext}>Heart Rate </Text>*/}
           <Picker PickerData={this.state.items2}
           selectedValue={75}
           labelIcon={heartRate_icon}
           styleImage={styles.imageContainer}
           placeholder={"Select Your Heart Rate"}
           title={"Heart Rate"}
           getTxt={(val,label)=>this.setState({heart_rate: val})}/>

         <View style={{ paddingVertical: AppSizes.ResponsiveSize.Padding(2) }} />
           {/* <Text style={styles.dropdowntext}>Temperature </Text>*/}
           <Picker PickerData={this.state.items3}
            selectedValue={11}
            labelIcon={temp_icon}
            styleImage={styles.imageContainer}
            placeholder={"Select Your Temperature"}
            title={"Temperature"}
            getTxt={(val,label)=>this.setState({temperature: label,temperature_val: val,})}/>
        {/*
         <View style={{ paddingVertical: AppSizes.ResponsiveSize.Padding(2) }} />
          <Text style={styles.dropdowntext}>FEV1 </Text>
         <Picker PickerData={this.state.items4}
          selectedValue={0}
          label={'Select Your FEV1'}
          getTxt={(val,label)=>this.setState({fev: val})}/>
          */}
      </View>;

      if(this.state.qus_no!=0){
          headerBack=<TouchableOpacity style={styles.menuWrapper} onPress={this.prev}>
                  <Image style={styles.image} source={back_icon}/>
                  </TouchableOpacity>;
        }
        else{
          headerBack=<TouchableOpacity style={styles.menuWrapper} onPress={this.back_click}>
                  <Image style={styles.image} source={back_icon}/>
                  </TouchableOpacity>;
  	  }

    return (
      <View style={styles.container}>
      <LinearGradient colors={['#48c3d5','#3fafc0','#369aaa','#287b88']}>

        <MyStatusBar barStyle="light-content"  backgroundColor="#4eb4c4"/>
          <View style={styles.appBar} >
              <View style={{ flex:1,flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', }}>
                <View style={styles.imageContainer}>
                {headerBack}
                </View>

                <View style={{ marginTop:0,width:'60%',justifyContent: 'center',alignItems:'center' }}>
                    <Text style={styles.headertitle}>Assessing your health!</Text>
                </View>

                <View style={{width:'15%',flexDirection: 'row',justifyContent: 'flex-end'}}>
        					<TouchableOpacity style={styles.qusWrapper} onPress={() => { this.Show_Custom_Alert(true) }}>
                      <Image style={styles.image} source={qus_icon}/>
                    </TouchableOpacity>
                </View>

              </View>
          </View>


      </LinearGradient>
      <View style={[styles.content,this.state.Modal_Visibility ? {backgroundColor: 'rgba(0,0,0,0.4)',zIndex:5} : '#ffffff']}>

      <ScrollView style={{paddingTop: 10}}>

      <View style={styles.container1}>
            <Text style={styles.heading}>{this.state.question}</Text>
            <View style={styles.hr}></View>
      </View>

      <View style={styles.container2}>
          { this.state.qus_no != 1 ? options : DropDown }
      </View>

      <View style={styles.container3}>
        <TouchableOpacity style={styles.btn} activeOpacity={.6} onPress={this.next}>
          <CommonButton label='Next'/>
          </TouchableOpacity>
      </View>
    </ScrollView>
    </View>
        <Spinner visible={this.state.isVisible}  />

        <Modal visible={this.state.Modal_Visibility}
          transparent={true}
          animationType={"fade"}
          onRequestClose={ () => { this.Show_Custom_Alert(!this.state.Modal_Visibility)} } >
              <View style={{ flex:1, alignItems: 'center', justifyContent: 'center',zIndex:20,backgroundColor:'#000000a3', }}>
              <View style={styles.Alert_Main_View}>
                  <View style={{ flex:1,flexDirection:'column',alignItems: 'center', justifyContent: 'center',}}>
<View style={{height:'15%',width:AppSizes.screen.width/10,paddingTop:'6%',marginBottom:8}}>
                        <TouchableOpacity style={{width:'100%',height:'100%'}} onPress={this.playTrack}>
                          <Image style={styles.image} source={this.state.audioImg}/>
                        </TouchableOpacity>
                      </View>
                      <View style={{height:'70%',flexDirection:'row',alignItems: 'center', justifyContent: 'center',paddingBottom:'2%',}}>
                        <ScrollView contentContainerStyle={styles.modal}>
                          <Text style={styles.Alert_Message}>
                            {this.state.modal_title}
                          </Text>
                        </ScrollView>
                      </View>
                  </View>
                  <View style={{width:'100%',flex:0.2,  backgroundColor: '#ebebeb',borderBottomLeftRadius:20,borderBottomRightRadius:20,}}>
                      <TouchableOpacity style={styles.buttonStyle} activeOpacity={0.7}
                        onPress={() => { this.Show_Custom_Alert(!this.state.Modal_Visibility)} }  >
                            <Text style={styles.TextStyle}> CANCEL </Text>
                      </TouchableOpacity>
                  </View>
                </View>
              </View>
          </Modal>

      </View>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
    backgroundColor: '#F5FCFF',
  },
  content: {
      flex: 1,
  },
  statusBar: {
    height: AppSizes.statusBarHeight,
  },
  appBar: {
      height: AppSizes.navbarHeight,
      justifyContent:'center',
      ...Platform.select({
      android: {
      paddingTop:AppSizes.ResponsiveSize.Padding(4),
      },
    }),
    },
  headertitle:{
    color:'#ffffff',
    fontSize:(Platform.OS === 'ios') ? AppSizes.ResponsiveSize.Sizes(14) :AppSizes.ResponsiveSize.Sizes(16),
    fontWeight:'bold',
    letterSpacing:1,
    textAlign:'center'
  },
  imageContainer:{
    width:'20%'
  },
  menuWrapper: {
    width:'60%',
    height:'60%',
    marginLeft:'30%',
  },
  image: {
    flex: 1,
    width: undefined,
    height: undefined,
    resizeMode:'contain'
  },
  container1:{
    flex:2,
    flexDirection:'column',
    alignItems:'center',
    justifyContent:'center'
  },
  heading:{
    fontSize:AppSizes.ResponsiveSize.Sizes(18),
    color:'#000',
    justifyContent:'center',
    alignItems:'center',
    fontWeight:'600',
    width:'89%',

  },
  hr:{
    width:50,
    height:3,
    backgroundColor:'#000',
    marginTop:10,
},
  container2:{
    flex:6,
    flexDirection:'column',
    padding: AppSizes.ResponsiveSize.Padding(5),
    width:AppSizes.screen.width,

  },
  container3:{
    flex:2,
    justifyContent:'flex-start',
    alignItems:'center',

  },

  dropdowntext:{
    fontSize:AppSizes.ResponsiveSize.Sizes(14),
    fontWeight:'500'
  },
  ansfield:{
    fontSize:AppSizes.ResponsiveSize.Sizes(13),
    color:'#5a5a5a',
  },
  btn:{
    width:'100%',
    marginBottom:20
  },
  imageContainer:{
    flex:1,
    alignItems: 'flex-start',
    //backgroundColor:'red',
  },
  qusWrapper: {
    width:'40%',
    height:'40%',
    marginRight:'30%',
    marginTop:'15%'
  },
  image: {
    flex: 1,
    width: undefined,
    height: undefined,
    resizeMode:'contain'
  },
  Alert_Main_View:{
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#ffffff',
    height:'40%',
    width: '80%',
    borderRadius:20,
    flexDirection:'column',
    justifyContent: 'center',
    alignItems: 'center',
  },

  Alert_Title:{
    fontSize: AppSizes.ResponsiveSize.Sizes(25),
    color: "#000",
    textAlign: 'center',
    padding: 10,
    height: '28%'
  },
    Alert_Message:{
      fontSize: AppSizes.ResponsiveSize.Sizes(18),
      color: "#000",
      textAlign: 'center',
      padding: 10,
    },
    buttonStyle: {
      width: '100%',
      height: '100%',
      justifyContent: 'center',
      alignItems: 'center'
    },
    TextStyle:{
      color:'#000',
      textAlign:'center',
      fontSize: AppSizes.ResponsiveSize.Sizes(18),
    },
});
