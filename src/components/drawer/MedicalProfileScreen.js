import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Image,
    TouchableHighlight,
    ScrollView,
    Dimensions,
    TextInput,
    TouchableOpacity,
    Alert,
    Keyboard,
    NetInfo,
    Platform,
    AsyncStorage,
    StatusBar
} from 'react-native';
import * as stateActions from '../../stateActions';
import { connect } from 'react-redux';
import { NavigationActions } from 'react-navigation'
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Spinner from 'react-native-loading-spinner-overlay';
import LinearGradient from 'react-native-linear-gradient';
import Header from '../common/signupHeader'
import Spacer from '../common/Spacer'
import * as commonFunctions from '../../utils/CommonFunctions'
import CommonButton from '../common/CommonButton'
import FloatingLabel from 'react-native-floating-labels';
import Orientation from 'react-native-orientation'

var Constant = require('../../api/ApiRules').Constant;
var WebServices = require('../../api/ApiRules').WebServices;

const logo = require('../../themes/Images/logo.png')

let _that
export default class MedicalProfileScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
          isVisible: false,
        },
        _that = this;
    }

    componentDidMount(){
      Orientation.lockToPortrait();
    }


  continuebtn(){
      AsyncStorage.setItem('screenName', JSON.stringify('MedicalFormScreen'));
	     _that.props.navigation.navigate('MedicalFormScreen');
  }


    static navigationOptions = {
        header: null,
    }
    updateAlert = (visible) => {
          this.setState({Modal_Visibility: !this.state.Modal_Visibility});
    }

    render() {
      const headerProp = {
        title: 'Medical Profile',
        screens: 'signupPolicies',
        qus_content:'',
        qus_audio:'https://houseofvirtruve.com/audio/home.mp3'
      };
        return (
          <View style={styles.wrapper}>
          <Header info={headerProp} navigation={_that.props.navigation} updateAlert={this.updateAlert}/>
          <View style={{flex:.2}}></View>
          <View style={[styles.titleContainer,this.state.Modal_Visibility ? {backgroundColor: 'rgba(0,0,0,0.4)',zIndex:5} : '#ffffff']}>
          <Text allowFontScaling={false} style={styles.headertitle}>Let’s set up your health profile!</Text>
          <TouchableOpacity activeOpacity={.6} onPress={this.continuebtn} >
                  <CommonButton label='Continue'/>
              </TouchableOpacity>
        </View>


          </View>
        );
    }
}

const styles = {
  wrapper: {
    flex: 1,
    flexDirection:'column',
    backgroundColor:'#ffffff'
  },
titleContainer:{
  flex:1,
  justifyContent:'flex-start',
},

headertitle:{
  textAlign:'center',
  color:AppColors.primary,
  fontSize:AppSizes.ResponsiveSize.Sizes(18),
  fontWeight:'500',
  letterSpacing:1,
},

}
