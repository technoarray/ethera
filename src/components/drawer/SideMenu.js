import PropTypes from 'prop-types';
import React, {Component} from 'react';
import {AsyncStorage,StyleSheet,ScrollView, Text, View,TouchableOpacity, Image,Linking,Alert,ImageBackground} from 'react-native';
import {NavigationActions} from 'react-navigation';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import * as commonFunctions from '../../utils/CommonFunctions'
var Constant = require('../../api/ApiRules').Constant;

var close_icon = require( '../../themes/Images/cross_120.png')
var background_image = require( '../../themes/Images/sidebar_image.png')


class SideMenu extends Component {
  constructor(props) {
      super(props)
      this.state = {
        currentScreen: 'HomeScreen',
        UserName:'',
      };
      _that = this;
  }
  componentDidMount() {
    AsyncStorage.getItem('loggedIn').then((value) => {
        var loggedIn = JSON.parse(value);
        if (loggedIn) {
            AsyncStorage.getItem('UserData').then((UserData) => {
                const data = JSON.parse(UserData)
                //console.log(val)
                var name=data.username;
                _that.setState({UserName:  name});
            })
        }
    })
}

navigateToScreen = (route) => () => {
  if(route=="LogoutScreen"){
    AsyncStorage.removeItem('loggedIn');
    AsyncStorage.removeItem('UserData');
    AsyncStorage.removeItem('vitals');
    AsyncStorage.removeItem('screenName');
    //AsyncStorage.removeItem('vitaldate');
    AsyncStorage.removeItem('user_email');
    _that.props.navigation.navigate('MainScreen');
  }
  else{
    this.setState({currentScreen: route});
    const navigateAction = NavigationActions.navigate({
      routeName: route,
    });
    _that.props.navigation.dispatch(navigateAction);
  }
}

  menu_close() {
    _that.props.navigation.closeDrawer();
  }



  render () {

    return (
      <View style={styles.container}>
      <ImageBackground style={ styles.imgBackground } resizeMode='cover' source={background_image}>
          <View style={{ marginTop:'10%', marginLeft:'5%',alignSelf: 'flex-start',}}>
            <TouchableOpacity onPress={this.menu_close}>
                <Image source={close_icon} style={{resizeMode: 'contain', width: 50, height: 50, }} />
            </TouchableOpacity>
            </View>

            <View style={{ margin:'2%',height:'5%'}}>
            {/*<Text style={[styles.navtitle,{textTransform:'uppercase',}]}>Hello, {this.state.UserName}</Text>*/}
            </View>

            <View style={styles.navSectionStyle}>
              {/*<View style={[styles.navBorderStyle,{borderBottomColor:'#76cadd',borderTopWidth:2, borderTopColor:'#76cadd'}]} >*/}

              <View style={[styles.navBorderStyle,(this.state.currentScreen === 'HomeScreen' ? styles.navactive : {} )]} >
                <Text allowFontScaling={false} style={[styles.navItemStyle,(this.state.currentScreen === 'HomeScreen' ? styles.textactive : {} )]} onPress={this.navigateToScreen('HomeScreen')}>
                  Home
                  </Text>
                </View>
                <View style={[styles.navBorderStyle,(this.state.currentScreen === 'AccountScreen' ? styles.navactive : {} )]} >
                  <Text allowFontScaling={false} style={[styles.navItemStyle,(this.state.currentScreen === 'AccountScreen' ? styles.textactive : {} )]} onPress={this.navigateToScreen('AccountScreen')}>
                    Account
                  </Text>
                </View>
                <View style={[styles.navBorderStyle,(this.state.currentScreen === 'AboutScreen' ? styles.navactive : {} )]} >
                  <Text allowFontScaling={false} style={[styles.navItemStyle,(this.state.currentScreen === 'AboutScreen' ? styles.textactive : {} )]} onPress={this.navigateToScreen('AboutScreen')}>
                    About
                    </Text>
                </View>
                <View style={[styles.navBorderStyle,(this.state.currentScreen === 'SettingScreen' ? styles.navactive : {} )]} >
                  <Text allowFontScaling={false} style={[styles.navItemStyle,(this.state.currentScreen === 'SettingScreen' ? styles.textactive : {} )]} onPress={this.navigateToScreen('SettingScreen')}>
                    Settings
                  </Text>
                </View>
                <View style={[styles.navBorderStyle,(this.state.currentScreen === 'PoliciesScreen' ? styles.navactive : {} )]} >
                  <Text allowFontScaling={false} style={[styles.navItemStyle,(this.state.currentScreen === 'PoliciesScreen' ? styles.textactive : {} )]} onPress={this.navigateToScreen('PoliciesScreen')}>
                  Policies
                  </Text>
                </View>
                <View style={[styles.navBorderStyle,(this.state.currentScreen === 'LogoutScreen' ? styles.navactive : {} )]} >
                  <Text allowFontScaling={false} style={[styles.navItemStyle,(this.state.currentScreen === 'LogoutScreen' ? styles.textactive : {} )]} onPress={this.navigateToScreen('LogoutScreen')}>
                  Logout
                  </Text>
                </View>

            </View>
            </ImageBackground>
          </View>
    );
  }
}

SideMenu.propTypes = {
  navigation: PropTypes.object
};


export default SideMenu;

const styles = StyleSheet.create({
  container: {
      flex:1,
      backgroundColor: '#386b98'
    },
    imgBackground: {
        width: '100%',
        height: '100%',
        flex: 1
    },
    navactive:{
      backgroundColor:'#45aabd',
    },
    textactive:{
      fontWeight:'bold',
    },
    navtitle:{
      textAlign:'center',
      fontSize: AppSizes.ResponsiveSize.Sizes(20),
      fontWeight:'bold',
      color:'#ffffff'
    },
    navItemStyle: {
      //marginBottom:10,
      textAlign:'center',
      fontSize: AppSizes.ResponsiveSize.Sizes(15),
      textTransform:'uppercase',
      color:'#ffffff'
    },
    navBorderStyle: {
       padding:'5%'
    },
    navSectionStyle: {
        marginTop:'3%',
    },
    sectionHeadingStyle: {
      paddingVertical: 10,
      paddingHorizontal: 5
    },


});
