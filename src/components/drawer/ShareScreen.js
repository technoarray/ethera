import React, { Component } from 'react';
import {StyleSheet,Text,View,Image,TouchableOpacity,Platform,Alert,ScrollView,TextInput,Button} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions, StackActions  } from 'react-navigation';
import Spacer from '../common/Spacer'
import Header from '../common/signupHeader'
import CommonButton from '../common/CommonButton'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import * as commonFunctions from '../../utils/CommonFunctions'
import FloatingLabel from 'react-native-floating-labels';
import FloatingLabelBox from '../common/FloatingLabelBox'
import Orientation from 'react-native-orientation'

var Constant = require('../../api/ApiRules').Constant;

/* Images */
var share_screen_icon = require('../../themes/Images/share_screen.png');
var logo = require('../../themes/Images/logo.png');
var email_icon = require('../../themes/Images/email.png');

var help_audio=require('../../themes/sound/ShareHelp.mp3')


export default class ShareScreen extends Component {
  constructor (props) {
      super(props)
      this.state = {
      isVisible: false,
      Modal_Visibility: false
    },
    _that = this;
  }

  componentDidMount(){
    Orientation.lockToPortrait();
  }

	closebtn(){
    const resetAction = StackActions.reset({
          index: 0,
          actions: [NavigationActions.navigate({ routeName: 'StartScreen' })],
      });
  _that.props.navigation.dispatch(resetAction);
	}

  updateAlert = () => {
        this.setState({Modal_Visibility: !this.state.Modal_Visibility});
  }

  render() {
    const headerProp = {
      title: 'Share',
      screens: 'ShareScreen',
      qus_content:'You can share a read only view of your profile and charted data by entering an email. The email will have a link that will expire after 30 minutes.',
      qus_audio:help_audio
    };


    return (
      <View style={styles.container}>
      <Header info={headerProp} navigation={_that.props.navigation} updateAlert={this.updateAlert}/>
      <View style={[styles.content,this.state.Modal_Visibility ? {backgroundColor: 'rgba(0,0,0,0.4)',zIndex:5} : '#ffffff']}>

        <KeyboardAwareScrollView
            innerRef={() => {return [this.refs.email]}} >
        <View style={styles.container1}>
          <View style={{ flexDirection: 'column',alignItems:'center',width:'80%',marginTop:AppSizes.ResponsiveSize.Padding(3), }}>
              <View style={styles.boxiconcontainer}>
                <Image source={share_screen_icon} style={styles.boxicon} />
              </View>
          </View>
        </View>

        <View style={styles.container2}>
          <FloatingLabelBox labelIcon={email_icon}>
              <FloatingLabel
                labelStyle={styles.labelInput}
                inputStyle={styles.input}
                style={styles.formInput}
                value={this.state.email}
                ref="email"
                autoCapitalize = 'none'
                placeholder='john@email.com'
                returnKeyType={ "next"}
                keyboardType={ 'default'}
                onChangeText={email=> this.setState({email})}
                keyboardType={ 'email-address'}
              >Email Address</FloatingLabel>
            </FloatingLabelBox>


          </View>
          <View style={styles.container3}>

            <Text allowFontScaling={false} style={styles.share_text}>Invite Your Physician or Caregivers to Access Your Iterex Record</Text>
          </View>
  </KeyboardAwareScrollView>
    </View>
      </View>


    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
	backgroundColor:'#ffffff',
  },
  container1:{
    flex:4,
    flexDirection:'column',
    alignItems:'center',
    //backgroundColor:'green'
  },
  boxiconcontainer:{
    width:AppSizes.screen.width/2,
    height:AppSizes.screen.width/2,
    justifyContent:'center',

  },
  boxicon:{
    width:null,
    height:null,
    flexGrow:1
  },
  container2:{
    flex:3,
    //backgroundColor:'red'
  },
  labelInput: {
    color: AppColors.contentColor,
    //fontWeight:'300',
    //fontSize:AppSizes.ResponsiveSize.Sizes(13),
  },
  input: {
    borderWidth: 0,
    color: AppColors.contentColor,
    fontWeight:'300',
    fontSize:AppSizes.ResponsiveSize.Sizes(15),
  },
  formInput: {
    borderWidth: 0,
    borderColor: '#333',
  },
  container3:{
    flex:4,
    //backgroundColor:'gray',
    padding:AppSizes.ResponsiveSize.Padding(5),
    alignItems:'center'

  },
  share_text:{
    fontSize:AppSizes.ResponsiveSize.Sizes(13),
    color: AppColors.contentColor,
    textAlign:'center',

  }
});
