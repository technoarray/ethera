import React, { Component } from 'react';
import {StatusBar,AsyncStorage,StyleSheet,Text,View,Image,TouchableOpacity,ScrollView,Alert,Dimensions,Picker,WebView,Platform, ActivityIndicator} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions, StackActions  } from 'react-navigation';
import LinearGradient from 'react-native-linear-gradient';
//import Header from '../common/signupHeader'
import * as commonFunctions from '../../utils/CommonFunctions'
import Swiper from 'react-native-swiper';
import Spinner from 'react-native-loading-spinner-overlay';
import Orientation from 'react-native-orientation'

var Constant = require('../../api/ApiRules').Constant;
var WebServices = require('../../api/ApiRules').WebServices;
let _that

const close_blue = require('../../themes/Images/close_blue.png')

export default class AsthmaCharts extends Component {
    constructor (props) {
      super(props)
      this.state = {
        uid:'',
        title:'Charts',
        isVisible: false,
        Modal_Visibility: false,
        orientation: ''
      },
      _that = this;
    }


  componentDidMount() {
    StatusBar.setHidden(true);
      AsyncStorage.getItem('loggedIn').then((value) => {
        var loggedIn = JSON.parse(value);
        if (loggedIn) {
            AsyncStorage.getItem('UserData').then((UserData) => {
                const data = JSON.parse(UserData)
                var uid=data.id;
                var type=this.props.navigation.state.params.vital_type;
                _that.setState({
                  uid:  uid,
                  chart_url: Constant.AsthmaCharts+"?uid="+uid+"&type="+type
                });
            })

            Orientation.unlockAllOrientations();
            Orientation.lockToLandscape();
            this.getOrientation();

          Dimensions.addEventListener( 'change', () =>
          {
            this.getOrientation();
          });
        }
    })
  }

  getOrientation = () =>
  {
    if( this.refs.rootView )
    {
        if( Dimensions.get('window').width < Dimensions.get('window').height )
        {
          this.setState({ orientation: 'portrait' });
        }
        else
        {
          this.setState({ orientation: 'landscape' });
        }
    }
  }

  updateAlert = (visible) => {
        this.setState({Modal_Visibility: !this.state.Modal_Visibility});
  }

  onSwipe = (index) => {
   if(index==0){
      this.setState({title:'Oxygen Saturation'})
    }
    else if(index==1){
      this.setState({title:'Heart Rate'})
    }
    else if(index==2){
      this.setState({title:'Temperature'})
    }
  }

  closebtn(){
    const resetAction = StackActions.reset({
          index: 0,
          actions: [NavigationActions.navigate({ routeName: 'StartScreen' })],
      });
      _that.props.navigation.dispatch(resetAction);
  }

  ActivityIndicatorLoadingView() {
    return (
        <ActivityIndicator
          color='#fff'
          size='large'
          style={styles.ActivityIndicatorStyle}
        />
      );
    }


  render() {
    const headerProp = {
      title: this.state.title,
      screens: 'ChartScreen',
    };

    return (
      <View ref = "rootView" style={styles.container}>
        <TouchableOpacity activeOpacity={.6} onPress={this.closebtn} style={styles.boxiconcontainer}>
        <Image source={close_blue} style={styles.boxicon} />
      </TouchableOpacity>

      <WebView
        style={styles.WebViewStyle}
        source={{uri: this.state.chart_url}}
        bounces={true}
        javaScriptEnabledAndroid={true}
        javaScriptEnabled={true}
        renderLoading={this.ActivityIndicatorLoadingView}
        startInLoadingState={true}
        scalesPageToFit
        mixedContentMode={'compatibility'}
      />
      <Spinner visible={this.state.isVisible}  />
    </View>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
  },

  content: {
      ...Platform.select({
      android: {
      paddingTop:AppSizes.ResponsiveSize.Padding(3),
      },
    }),
    },

slide1: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  slide2: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  slide3: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  boxiconcontainer:{
    width:'100%',
    alignItems:'flex-end',
    backgroundColor:'#ffffff'
  },
  boxicon:{
    width:AppSizes.screen.width/8,
    height:AppSizes.screen.width/8,
    flexGrow:1
  },
});
