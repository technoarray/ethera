import React, { Component } from 'react';
import {AsyncStorage,StyleSheet,Text,View,Image,TouchableOpacity,Platform,Alert,ScrollView,TextInput,Button,StatusBar,AppState,} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions } from 'react-navigation'
import Spacer from '../common/Spacer'
import Header from '../common/signupHeader'
import CommonButton from '../common/CommonButton'
import Swiper from 'react-native-swiper';
import Animbutton from './animAnsbutton'
import AnimSinglebutton from './animSingleAnsOptions';
import { jsonData } from '../data/Question';
import { pickerData } from '../data/pickerData';
import LinearGradient from 'react-native-linear-gradient';
import Spinner from 'react-native-loading-spinner-overlay';
import Picker from 'react-native-q-picker';
import Orientation from 'react-native-orientation'
import * as commonFunctions from '../../utils/CommonFunctions'

var Constant = require('../../api/ApiRules').Constant;
var WebServices = require('../../api/ApiRules').WebServices;

/* Images */
//var thankyou_icon = require('../../themes/Images/thank-you.png');
var logo = require('../../themes/Images/logo.png');
var back_icon = require( '../../themes/Images/left-whitearrow.png')

var current_ans=''
let ans_track = []

const MyStatusBar = ({backgroundColor, ...props}) => (
  <View style={[styles.statusBar, { backgroundColor }]}>
    <StatusBar translucent backgroundColor={backgroundColor} {...props} />
  </View>
)

let _that
export default class MedicalQusScreen extends Component {
  constructor (props) {
      super(props)
      this.qno = 0


      const jdata = jsonData.medicalqus.qus3
      arrnew = Object.keys(jdata).map( function(k) { return jdata[k] });
      this.state = {
        question : arrnew[this.qno].question,
        options : arrnew[this.qno].options,
        suboption : arrnew[this.qno].suboption,
        type : arrnew[this.qno].type,
        countCheck : [],
        qus_no : 0,
        uid : '',
        isVisible: false,
        Modal_Visibility: false,
        status:false,
    },
    _that = this;
  }

  componentDidMount() {
    Orientation.lockToPortrait();
    AsyncStorage.getItem('screenName').then((screenName) => {
      var screenName = JSON.parse(screenName);
      _that.setState({screenName:screenName})
    })

    console.log(this.state.status);

    AsyncStorage.getItem('loggedIn').then((value) => {
        var loggedIn = JSON.parse(value);
        if (loggedIn) {
            AsyncStorage.getItem('UserData').then((UserData) => {
                const data = JSON.parse(UserData)
                console.log(data)
                _that.setState({
                  uid:data.id,
                });
            })
        }
    })
  }

  updateAlert = () => {
        this.setState({Modal_Visibility: !this.state.Modal_Visibility});
  }

  next= () =>{

    const { countCheck} = this.state
    if(this.qno!=3 && this.state.countCheck == 0){
      Alert.alert("Invalid Value","Missing or blank value is not allowed")
    }
    else{
      if(countCheck.indexOf('asthma')!=-1){
        this.setState({
          primary:'asthma',
          countCheck:[],
          status:false
        })
        AsyncStorage.setItem('screenName', JSON.stringify('MedicalAsthmaQusScreen'));
        _that.props.navigation.navigate('MedicalAsthmaQusScreen');
      }
      else{
        this.setState({
          primary:'copd',
          countCheck:[],
          status:false
        })
        AsyncStorage.setItem('screenName', JSON.stringify('MedicalQusScreen'));
        _that.props.navigation.navigate('MedicalQusScreen');
      }
    }
  }


  _answer(status,ans){
    if(this.state.type=="single"){
      ans_track.splice(-1,1);
      ans_track.push(ans);
    }
    current_ans=ans;
    this.setState({ countCheck: ans_track })
  }



back_btn(){
  _that.props.navigation.navigate("MedicalFormSelectedScreen");
}


  render() {
    //console.log(this.state.python_result);
    const headerProp = {
      title: 'Medical Profile setup!',
      screens: 'MedicalQusScreen',
    };
    let _this = this
      const currentOptions = this.state.options
      const options = Object.keys(currentOptions).map( function(k) {
        if(k==current_ans){status=true}else{status=false}
        return (  <View key={k} style={{margin:5, }}>
          { _this.state.type=='multiple' ?
            <Animbutton onColor={"#2c808f"} effect={"pulse"}  _onPress={(status) => _this._answer(status,k)} text={currentOptions[k]} />
          :
            <AnimSinglebutton onColor={"#2c808f"} effect={"pulse"} status={status} _onPress={(status) => _this._answer(status,k)} text={currentOptions[k]} />
          }

        </View>)
      });

          headerBack=<TouchableOpacity style={styles.menuWrapper} onPress={this.back_btn}>
                    <Image style={styles.image} source={back_icon}/>
                    </TouchableOpacity>;

    return (
      <View style={styles.container}>
      <LinearGradient colors={['#48c3d5','#3fafc0','#369aaa','#287b88']}>
        <MyStatusBar barStyle="light-content"  backgroundColor="#4eb4c4"/>
        <View style={styles.appBar} >
          <View style={{ flex:1,flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', }}>

            <View style={styles.imageContainer}>
            </View>

            <View style={{ marginTop:0,width:'60%',justifyContent: 'center',alignItems:'center' }}>
              <Text allowFontScaling={false} style={styles.headertitle}>Medical Profile setup!</Text>
            </View>

            <View style={{width:'15%',flexDirection: 'row',justifyContent: 'flex-end'}}>

            </View>

          </View>
        </View>
      </LinearGradient>

      <ScrollView style={{backgroundColor: '#F5FCFF',paddingTop: 10}}
        ref={(c) => {this.scroll = c}} >
    		<View style={styles.container1}>
            <Text allowFontScaling={false} style={styles.heading}>  {this.state.question} </Text>
            <View style={styles.hr}></View>
        </View>
        <View style={styles.container2}>
              { this.state.qus_no != 6 ? options : DropDown }
        </View>
        <View style={styles.container3}>
          <TouchableOpacity style={styles.btn} activeOpacity={.6} onPress={this.next}>
            <CommonButton label='Next'/>
            </TouchableOpacity>
        </View>

      </ScrollView>

      <Spinner visible={this.state.isVisible}  />

    </View>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
  },
  statusBar: {
    height: AppSizes.statusBarHeight,
  },
  appBar: {
      height: AppSizes.navbarHeight,
      justifyContent:'center',
      ...Platform.select({
      android: {
      paddingTop:AppSizes.ResponsiveSize.Padding(4),
      },
    }),
    },
  headertitle:{
    color:'#ffffff',
    fontSize:AppSizes.ResponsiveSize.Sizes(16),
    fontWeight:'bold',
    letterSpacing:1,
  },
  imageContainer:{
    width:'20%'
  },
  menuWrapper: {
    width:'60%',
    height:'60%',
    marginLeft:'30%',
  },
  image: {
    flex: 1,
    width: undefined,
    height: undefined,
    resizeMode:'contain'
  },
  container1:{
    flex:2,
    flexDirection:'column',
    alignItems:'center',
    justifyContent:'center'
  },
  heading:{
    fontSize:AppSizes.ResponsiveSize.Sizes(18),
    color:'#000',
    justifyContent:'center',
    alignItems:'center',
    fontWeight:'600',
    width:'89%',

  },
  hr:{
    width:50,
    height:3,
    backgroundColor:'#000',
    marginTop:10,

  },
  container2:{
    flex:6,
    flexDirection:'column',
    width:AppSizes.screen.width,
   paddingLeft:AppSizes.ResponsiveSize.Padding(5),
   paddingRight:AppSizes.ResponsiveSize.Padding(5),
   paddingTop:AppSizes.ResponsiveSize.Padding(2),
  },
  container3:{
    flex:2,
    justifyContent:'flex-start',
    alignItems:'center',

  },
  ans:{
    borderBottomWidth:1,
    borderColor:'#000',
    paddingTop:5,
    paddingBottom:10,
    width:'90%',
    marginBottom:25,

  },
  ansfield:{
    fontSize:AppSizes.ResponsiveSize.Sizes(13),
    color:'#5a5a5a',
  },
  btn:{
    width:'100%',
    marginBottom:20
  }
});
