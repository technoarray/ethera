import React, { Component } from 'react';
import {StyleSheet,Text,View,Image,TouchableOpacity,Platform,Alert,ScrollView} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions } from 'react-navigation'
import Spacer from '../common/Spacer'
import Header from '../common/Header'
import Orientation from 'react-native-orientation';
import * as commonFunctions from '../../utils/CommonFunctions'

var Constant = require('../../api/ApiRules').Constant;

/* Images */
var map = require( '../../themes/Images/map_410.png')

export default class AboutScreen extends Component {
  constructor (props) {
      super(props)
      this.state = {
      isVisible: false,
      Modal_Visibility: false
    },
    _that = this;
  }

  componentDidMount(){
    Orientation.lockToPortrait();
  }

  updateAlert = () => {
        this.setState({Modal_Visibility: !this.state.Modal_Visibility});
  }

  website_link() {
    Alert.alert(
        'ITEREX',
        'You will be redirected to your default browser, to visit our Website',
        [
          {text: 'Cancel', onPress: () => console.log('Cancel Button Pressed'), style: 'cancel'},
          {text: 'OK', onPress: () => Linking.openURL(Constant.SITE_URL)},

        ]

      )
  }

  render() {
    const headerProp = {
      title: 'ABOUT',
      screens: 'AboutScreen',
      qus_content:'',
      qus_audio:'https://houseofvirtruve.com/audio/about.mp3'
    };

    return (
      <View style={styles.container}>
        <Header info={headerProp} navigation={_that.props.navigation} updateAlert={this.updateAlert}/>
        <View style={[styles.content,this.state.Modal_Visibility ? {backgroundColor: 'rgba(0,0,0,0.4)',zIndex:5} : '#ffffff']}>

            <View style={[styles.box2,{height:'20%'}]}>
              <View style={{width:'100%',height:'100%',paddingTop:'5%'}}>
                <Image source={map} style={styles.image}/>
              </View>
            </View>

            <View style={[styles.box1,{height:'10%'}]}>
              <Text allowFontScaling={false} style={styles.title}>ITEREX</Text>
            </View>

            <View style={[styles.box3,{height:'50%'}]}>
            <ScrollView>
              <Text allowFontScaling={false} style={styles.textcontent}>Iterex Therapeutics is dedicated to revolutionizing at-home care of chronic illnesses through early, cost effective, personalized technology support. The chronic anxiety and traumatic symptom flare-ups that patients confront everyday are frequently unnecessary and avoidable. Iterex’s technology gives consumers a chance to catch symptom flare-ups early and get the right medical help early using simple, on-demand health applications.

              The Iterex team consists of best-in-class medical specialists, data scientists, and clinical researchers. They are passionate about making applications that are accurate, consumer tested, and subject to rigorous scientific and market scrutiny.</Text>

              </ScrollView>
            </View>
            <View style={[styles.box1,styles.about_padding,{height:'15%'}]}>
            <Text allowFontScaling={false} style={styles.texturl} onPress={this.website_link}>
              {Constant.SITE_URL}
            </Text>
            </View>
        </View>
    </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
  },

  content: {
      flex: 1,
      flexDirection: 'column',
      height:'70%',
      padding:'5%'
  },
  title:{
    color:AppColors.primary,
    textAlign:'center',
    fontSize:AppSizes.ResponsiveSize.Sizes(20),
    fontWeight:'600'
  },
  texturl :{
    textAlign:'center',
    fontWeight:'bold',
    fontSize:AppSizes.ResponsiveSize.Sizes(18),
    color:AppColors.primary,
  },

  box1: {
  //  backgroundColor: '#fff'
  },
  box2: {
  //  backgroundColor: '#fff'
  },
  box3: {
  //  backgroundColor: '#fff'
  },
  image: {
    flex: 1,
    width: undefined,
    height: undefined,
    resizeMode:'contain'
  },

 textcontent:{
   color:AppColors.contentColor,
   textAlign:'center',
   fontSize:AppSizes.ResponsiveSize.Sizes(13),
   lineHeight: AppSizes.ResponsiveSize.Sizes(13 * 1.70),
   fontWeight:'300'
},

about_padding : {
  paddingTop:'5%'
},

});
