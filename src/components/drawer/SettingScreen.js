import React, { Component } from 'react';
import {StyleSheet,Text,View,Image,TouchableOpacity,Platform,Alert,Button} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions } from 'react-navigation'
import SettingsList from 'react-native-settings-list';
import Orientation from 'react-native-orientation'
import Spacer from '../common/Spacer'
import Header from '../common/Header'
import * as commonFunctions from '../../utils/CommonFunctions'

var Constant = require('../../api/ApiRules').Constant;

/* Images */
var email_icon = require( '../../themes/Images/email.png');
var bell_icon = require( '../../themes/Images/bell.png');
var opt_icon = require( '../../themes/Images/otp-output.png');

export default class SettingScreen extends Component {
  constructor (props) {
    super(props)
    this.state = {
      isVisible: false,
      Modal_Visibility: false,
      email_status:'ON',
      push_notification_status:'ON'
    },
    _that = this;
  }

  componentDidMount(){
    Orientation.lockToPortrait();
  }

  updateAlert = () => {
    this.setState({Modal_Visibility: !this.state.Modal_Visibility});
  }

  render() {
    const headerProp = {
      title: 'SETTINGS',
      screens: 'SettingScreen',
      qus_content:'',
      qus_audio:'https://houseofvirtruve.com/audio/settings.mp3'
    };

    return (

    <View style={styles.container}>
      <Header info={headerProp} navigation={_that.props.navigation} updateAlert={this.updateAlert}/>
      <View style={[styles.content,this.state.Modal_Visibility ? {backgroundColor: 'rgba(0,0,0,0.4)',zIndex:5} : '#ffffff']}>
        <Text style={styles.subtitle}>Notification</Text>
        <View style={styles.mainbox}>
            <View style={styles.icon}>
                  <Image style={styles.cicon} source={email_icon}/>
            </View>
            <View style={styles.textbox}>
                  <Text style={styles.setting_title}>Email</Text>
            </View>
        </View>
        <View style={styles.mainbox}>
            <View style={styles.icon}>
                  <Image style={styles.cicon} source={bell_icon}/>
            </View>
            <View style={styles.textbox}>
                  <Text style={styles.setting_title}>Push Notification</Text>
            </View>
        </View>
      </View>
    </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
  },

  content: {
      flex: 1,
      backgroundColor: '#ffffff',
      height:'70%',
      paddingLeft:'5%',
      paddingRight:'5%',
      paddingTop:15
  },
title:{
  textAlign:'center',
  color:AppColors.contentColor,
  fontSize:AppSizes.ResponsiveSize.Sizes(14),
  paddingBottom:'3%'
},
subtitle : {
  color:AppColors.contentColor,
  textAlign:'center',
  fontSize:AppSizes.ResponsiveSize.Sizes(18),
  fontWeight:'500',
  marginBottom:15
},
imageContainer:{
  width:'6%',
},
iconWrapper: {
  width:'100%',
  height:'100%',

},
image: {
  flex: 1,
  width: undefined,
  height: undefined,
  resizeMode:'contain'
},
setting_title:{
  color:AppColors.contentColor,
  color:'black',
  fontSize:AppSizes.ResponsiveSize.Sizes(14),
},
borderBottom:{
  borderBottomWidth:1,
  borderBottomColor:'#a9a9a9'
},
mainbox:{
  borderBottomWidth:1,
  borderColor:'#000',
  flexDirection:'row',
  paddingTop:8,
  marginBottom:15,
  paddingBottom:8
},
icon:{
  width:20,
  marginRight:15
},
texttbox:{
  width:'80%'
},
cicon:{
  width:20,
  height:20
}

});
