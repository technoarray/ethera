import React, { Component } from 'react';
import {AsyncStorage,StyleSheet,Text,View,Image,TouchableOpacity,Platform,Alert,ScrollView,TextInput,Button,StatusBar} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions, StackActions  } from 'react-navigation';
import Spacer from '../common/Spacer'
import Header from '../common/questionsHeader'
import CommonButton from '../common/CommonButton'
import Swiper from 'react-native-swiper';
import Animbutton from './animAnsbutton'
import AnimSinglebutton from './animSingleAnsOptions';
import RNPickerSelect from 'react-native-picker-select';
import { jsonData } from '../data/Question';
import { pickerData } from '../data/pickerData';
import LinearGradient from 'react-native-linear-gradient';
import Orientation from 'react-native-orientation'

import * as commonFunctions from '../../utils/CommonFunctions'

var Constant = require('../../api/ApiRules').Constant;

/* Images */
//var thankyou_icon = require('../../themes/Images/thank-you.png');
var logo = require('../../themes/Images/logo.png');
var back_icon = require( '../../themes/Images/left-arrow.png')


let arrnew = []
let ans_track = []
var current_ans=''

const MyStatusBar = ({backgroundColor, ...props}) => (
  <View style={[styles.statusBar, { backgroundColor }]}>
    <StatusBar translucent backgroundColor={backgroundColor} {...props} />
  </View>
);
export default class InfoQusScreen extends Component {
  constructor (props) {
      super(props)
      this.qno = this.props.navigation.state.params.qus_no
      this.score = 0
      this.type ='qus'
      this.inputRefs = {};
      const jdata = jsonData.infoqus.qus1
      arrnew = Object.keys(jdata).map( function(k) { return jdata[k] });
      this.state = {
        question : arrnew[this.qno].question,
        options : arrnew[this.qno].options,
        suboption : arrnew[this.qno].suboption,
        type : arrnew[this.qno].type,
        countCheck : [],
        value:'',
        qus_no : 0,
        isVisible: false,
        Modal_Visibility: false,
        status:'ans',
    },
    _that = this;
  }

  componentDidMount(){
    Orientation.lockToPortrait();
  }

  updateAlert = () => {
        this.setState({Modal_Visibility: !this.state.Modal_Visibility});
  }

  prev= () =>{

    //console.log(this.qno)
    if(this.qno > 0){
      this.qno--
      this.setState({ qus_no: this.qno })
      this.setState({ question: arrnew[this.qno].question, options: arrnew[this.qno].options, type: arrnew[this.qno].type,suboption : arrnew[this.qno].suboption})
    }
  }
  next= () =>{
    current_ans=0
    //ans_track =[];
    //console.log(this.qno)
    const { countCheck} = this.state
    //console.log('count'+countCheck)

    if(this.state.countCheck.length <= 0){
      Alert.alert("Invalid Value","Missing or blank value is not allowed")
    }
    else {
      if(this.state.value=="option6"){
        const resetAction = StackActions.reset({
              index: 0,
              actions: [NavigationActions.navigate({ routeName: 'StartScreen' })],
          });
      _that.props.navigation.dispatch(resetAction);
      }
      else{
        AsyncStorage.getItem('UserData').then((UserData) => {
            const data = JSON.parse(UserData)
            var type=data.account_type;
            if(type=='asthma'){
              _that.props.navigation.navigate('Thanku', {message: 'Thanks for providing feedback that will be used to improve your experience'});
            }
            else{
              _that.props.navigation.navigate('ThankYouScreen', {message: 'Thanks for providing feedback that will be used to improve your experience'});
            }
        })

      }
      //this.qno++
      //this.setState({ qus_no: this.qno })
      //this.setState({ countCheck: [], question: arrnew[this.qno].question, options: arrnew[this.qno].options, type: arrnew[this.qno].type, suboption : arrnew[this.qno].suboption})

    }

  }
  _answer(status,ans){
    //ans_track.push(ans);
    console.log(ans)
    //ans_track[this.state.qus_no] = new Array(ans);
    this.setState({ countCheck: ans })
    this.setState({ value: ans })
    //console.log(this.state.countCheck)
    current_ans=ans;
    this.setState({ status: 'ans' })

  }

  render() {
    const headerProp = {
      title: 'Answer Question',
      screens: 'HomeScreen',
      qus_content:'',
      qus_audio:'https://houseofvirtruve.com/audio/home.mp3'
    };

    let _this = this

      const currentOptions = this.state.options
      const options = Object.keys(currentOptions).map( function(k) {
          if(k==current_ans){status=true}else{status=false}
        return (  <View key={k} style={{margin:5, }}>
          { _this.state.type=='multiple' ?
            <Animbutton onColor={"#2c808f"} effect={"pulse"} status={_this.state.status} _onPress={(status) => _this._answer(status,k)} text={currentOptions[k]} />
          :
            <AnimSinglebutton onColor={"#2c808f"} effect={"pulse"} status={status} _onPress={(status) => _this._answer(status,k)} text={currentOptions[k]} />
          }
        </View>)
      });

      if(this.state.qus_no!=0){
          headerBack=<TouchableOpacity style={styles.menuWrapper} onPress={this.prev}>
                  <Image style={styles.image} source={back_icon}/>
                  </TouchableOpacity>;
        }
        else{
  		  headerBack=<Text></Text>;
  	  }

    return (
      <View style={styles.container}>
      <Header info={headerProp} navigation={_that.props.navigation} updateAlert={this.updateAlert}/>

     <View style={[styles.content,this.state.Modal_Visibility ? {backgroundColor: 'rgba(0,0,0,0.4)',zIndex:5} : '#ffffff']}>
    </View>

        <ScrollView style={{backgroundColor: '#F5FCFF',paddingTop: 10}}>

		<View style={styles.container1}>
          <Text allowFontScaling={false} style={styles.heading}>  {this.state.question} </Text>
          <View style={styles.hr}></View>
      </View>
      <View style={styles.container2}>
            { options }
      </View>


      <View style={styles.container3}>
        <TouchableOpacity style={styles.btn} activeOpacity={.6} onPress={this.next}>
          <CommonButton label='Next'/>
          </TouchableOpacity>
      </View>

      </ScrollView>

      </View>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
  },
  statusBar: {
    height: AppSizes.statusBarHeight,
  },
  appBar: {
      height: AppSizes.navbarHeight,
      justifyContent:'center',
    },
  headertitle:{
    color:'#ffffff',
    fontSize:AppSizes.ResponsiveSize.Sizes(16),
    fontWeight:'bold',
    letterSpacing:1,
  },
  imageContainer:{
    width:'20%'
  },
  menuWrapper: {
    width:'60%',
    height:'60%',
    marginLeft:'30%',
  },
  image: {
    flex: 1,
    width: undefined,
    height: undefined,
    resizeMode:'contain'
  },
  container1:{
    flex:2,
    flexDirection:'column',
    alignItems:'center',
    justifyContent:'center'
  },
  heading:{
    fontSize:AppSizes.ResponsiveSize.Sizes(18),
    color:'#000',
    justifyContent:'center',
    alignItems:'center',
    fontWeight:'600',
    width:'89%',

  },
  hr:{
    width:50,
    height:3,
    backgroundColor:'#000',
    marginTop:10,
},
  container2:{
    flex:6,
    flexDirection:'column',
    padding: AppSizes.ResponsiveSize.Padding(5),
    width:AppSizes.screen.width,

  },
  container3:{
    flex:2,
    justifyContent:'flex-start',
    alignItems:'center',

  },

  dropdowntext:{
    fontSize:AppSizes.ResponsiveSize.Sizes(14),
    fontWeight:'500'
  },
  ansfield:{
    fontSize:AppSizes.ResponsiveSize.Sizes(13),
    color:'#5a5a5a',
  },
  btn:{
    width:'100%',
    marginBottom:20
  }
});

const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
        fontSize: AppSizes.ResponsiveSize.Sizes(14),
        paddingTop: AppSizes.ResponsiveSize.Padding(5),
        paddingHorizontal: AppSizes.ResponsiveSize.Padding(5),
        paddingBottom: AppSizes.ResponsiveSize.Padding(5),
        borderWidth: 1,
        borderColor: 'gray',
        borderRadius: 4,
        backgroundColor: 'white',
        color: 'black',
    },
});
