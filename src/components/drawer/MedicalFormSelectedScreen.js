import React, { Component } from 'react';
import {StyleSheet,Text,View,Image,TouchableOpacity,Platform,ScrollView,Alert,AsyncStorage,Keyboard} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions } from 'react-navigation'
import Spacer from '../common/Spacer'
import Header from '../common/signupHeader'
import ToggleBox from '../common/ToggleBox'
import CommonButton from '../common/CommonButton'
import LinearGradient from 'react-native-linear-gradient';
import FloatingLabelBox from '../common/FloatingLabelBox'
import FloatingLabel from 'react-native-floating-labels';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Spinner from 'react-native-loading-spinner-overlay';
import { pickerData } from '../data/pickerData';
import Picker from 'react-native-q-picker';
import * as commonFunctions from '../../utils/CommonFunctions'
import Orientation from 'react-native-orientation'

var Constant = require('../../api/ApiRules').Constant;
var WebServices = require('../../api/ApiRules').WebServices;


const logo = require('../../themes/Images/logo.png')
const height_icon = require('../../themes/Images/height.png')
const age_icon = require('../../themes/Images/calender.png')
const gender_icon = require('../../themes/Images/gender.png')
const weight_icon = require('../../themes/Images/weight.png')

let _that

var age_data = [];

//heart_data
for( var j = 18; j <=100; j++){
  var numbers = {
      name: j+' ',
      id: j
  };
  age_data.push(numbers);
}

export default class MedicalFormSelectedScreen extends Component {
    constructor(props) {
        super(props)
		 this.inputRefs = {};
        this.state = {
          uid:'',
    			birth_date:'',
          age_data: age_data,
    			height:'',
          heightlabel:'',
          height_data:pickerData.height_data,
    			weight:'',
          isVisible: false,
    		  gender:'',
          genderlabel:'',
    		  items: [
                    {
                        name: 'Female',
                        id: 0
                    },
                    {
                        name: 'Male',
                        id: 1
                    },
                ],
            },
        _that = this;
    }

    componentDidMount() {
      Orientation.lockToPortrait();
      AsyncStorage.getItem('loggedIn').then((value) => {
          var loggedIn = JSON.parse(value);
          if (loggedIn) {
              AsyncStorage.getItem('UserData').then((UserData) => {
                  const data = JSON.parse(UserData)
                  console.log(data)
                  var uid=data.id;
                  var height=parseInt(data.height);
                  var height_feet=commonFunctions.toFeet(height)
                  var gender=parseInt(data.gender);
                  var birth_date=parseInt(data.birth_date);


                  genderlabel='';
                  if(data.gender==0){
                    genderlabel='Female';
                  }
                  else if(data.gender==1){
                    genderlabel='Male';
                  }

                  _that.setState({
                    uid :  uid,
                    gender :gender,
                    genderlabel : genderlabel,
                    birth_date : birth_date,
                    height : height,
                    heightlabel:height_feet,
                    weight : data.weight,
                  });

                  console.log('uid='+_that.state.uid)
                  console.log('height='+_that.state.height)
                  console.log('heightlabel='+_that.state.heightlabel)
              })

          }
      })
    }
  userLogin() {
    _that.props.navigation.navigate('LoginScreen');
  }

  continueForm() {
    _that.validationAndApiParameter()
  }

  validationAndApiParameter() {
        const { method, gender,birth_date,height,weight, isVisible } = this.state
        //console.log(gender+" "+birth_date+" "+height+" "+weight);

        if (gender.length <= 0) {
            Alert.alert('','Please enter your gender!');
        }else if (birth_date.length <= 0) {
            Alert.alert('','Please enter your Age!');
        }else if (weight.length <= 0) {
            Alert.alert('','Please enter Weight!');
        }/*else if ((height.indexOf(' ') >= 0 || height.length <= 0)) {
            Alert.alert('','Please enter Height!');
        }*/else {
				//_that.props.navigation.navigate('MedicalQusScreen');

        var data = {
            uid: this.state.uid,
            gender:gender,
            birth_date:birth_date,
            height:height,
            weight:weight
          };

            console.log(data);

              _that.setState({
                      isVisible: true
              });
              this.postToApiCalling('POST', 'persoanlInfo', Constant.URL_UserPersonalInfo, data);
        }

  }

  postToApiCalling(method, apiKey, apiUrl, data) {

     new Promise(function(resolve, reject) {
          if (method == 'POST') {
              resolve(WebServices.callWebService(apiUrl, data));
          } else {
              resolve(WebServices.callWebService_GET(apiUrl, data));
          }
      }).then((jsonRes) => {
        _that.setState({ isVisible: false })

          if ((!jsonRes) || (jsonRes.code == 0)) {

          _that.setState({ isVisible: false })
          setTimeout(()=>{
              Alert.alert(jsonRes.message);
          },200);

          } else {
              if (jsonRes.code == 1) {
                AsyncStorage.setItem('screenName', JSON.stringify('MedicalQusSelectionScreen'));
                AsyncStorage.setItem('UserData', JSON.stringify(jsonRes.data));
                _that.props.navigation.navigate('MedicalQusSelectionScreen');
              }
          }
      }).catch((error) => {
          console.log("ERROR" + error);
          _that.setState({ isVisible: false })

          setTimeout(()=>{
              Alert.alert("Server issue");
          },200);
      });
  }


    static navigationOptions = {
        header: null,
    }

    updateAlert = (visible) => {
          this.setState({Modal_Visibility: !this.state.Modal_Visibility});
    }

  render() {
    //console.log(this.state.height)
    const headerProp = {
      title: 'Medical Form',
      screens: 'signupPolicies',
      qus_content:'',
      qus_audio:'https://houseofvirtruve.com/audio/home.mp3'
    };

    return (

      <View style={styles.container}>
        <Header info={headerProp} navigation={_that.props.navigation} updateAlert={this.updateAlert}/>
        <View style={[styles.content,this.state.Modal_Visibility ? {backgroundColor: 'rgba(0,0,0,0.4)',zIndex:5} : '#ffffff']}>
        <KeyboardAwareScrollView
            innerRef={() => {return [this.refs.weight]}} >
            <View style={{padding:AppSizes.ResponsiveSize.Padding(3)}} />


            <View style={{padding:AppSizes.ResponsiveSize.Padding(2)}} />
            <View style={styles.formContainer}>
              { this.state.birth_date != "" ?
                <Picker PickerData={this.state.items}
                labelIcon={gender_icon}
                styleImage={styles.imageContainer}
                placeholder={"Select your gender"}
                title={"Gender"}
                selectedValue={this.state.gender}
                defaultLabel={this.state.genderlabel}
                getTxt={(val,label)=>this.setState({gender:val})}/>
                : null }
            </View>

            <View style={{padding:AppSizes.ResponsiveSize.Padding(2)}} />
            <View style={styles.formContainer}>
                { this.state.birth_date != "" ?
                <Picker PickerData={this.state.age_data}
                labelIcon={age_icon}
                styleImage={styles.imageContainer}
                placeholder={"Select your age"}
                title={"Age"}
                selectedValue={this.state.birth_date}
                defaultLabel={this.state.birth_date}
                getTxt={(val,label)=>this.setState({birth_date:val})}/>
                : null }
            </View>

          <View style={{padding:AppSizes.ResponsiveSize.Padding(2)}} />
          <View style={styles.formContainer}>
          { this.state.height != "" ?
              <Picker PickerData={this.state.height_data}
              labelIcon={height_icon}
              styleImage={styles.imageContainer}
              placeholder={"Select your height"}
              title={"Height"}
              selectedValue={this.state.height}
              defaultLabel={this.state.heightlabel}
              getTxt={(val,label)=>this.setState({height:val})}/>
              : null }
          </View>

          <View style={{padding:AppSizes.ResponsiveSize.Padding(2)}} />
        <FloatingLabelBox labelIcon={weight_icon}>
          <FloatingLabel
              labelStyle={styles.labelInput}
              inputStyle={styles.input}
              style={styles.formInput}
              value={this.state.weight}
              placeholder='Body Weight (lbs)'
              ref="weight"
              autoCapitalize = 'none'
              keyboardType={ 'numeric'}
              returnKeyType = { 'done' }
              onChangeText={weight=> this.setState({weight})}
            >Weight</FloatingLabel>
        </FloatingLabelBox>

        </KeyboardAwareScrollView>



        <TouchableOpacity activeOpacity={.6} onPress={this.continueForm} style={{paddingTop:AppSizes.ResponsiveSize.Padding(5),}}>
            <CommonButton label='Continue'/>
        </TouchableOpacity>
      </View>
        <Spinner visible={this.state.isVisible}  />

    </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
  },

  content: {
      flex: 1,
      backgroundColor: '#ffffff',
      height:'70%',
      //paddingLeft:AppSizes.ResponsiveSize.Padding(5),
      //paddingRight:AppSizes.ResponsiveSize.Padding(5),
  },

  title:{
    textAlign:'center',
    fontSize:AppSizes.ResponsiveSize.Sizes(14),
    paddingTop:'3%'
  },

  ToggalContainer: {
    backgroundColor: '#fff',
    borderBottomWidth: 2,
    borderBottomColor:'#000'
  },

  ToggleInner: {
  height: 100,
  alignItems: 'center',
  justifyContent: 'center',
  backgroundColor: '#ebebeb'
},

labelInput: {
  color: AppColors.contentColor,
  //fontWeight:'300',
  fontSize:AppSizes.ResponsiveSize.Sizes(13),
},
input: {
  borderWidth: 0,
  color: AppColors.contentColor,
  fontWeight:'300',
  fontSize:AppSizes.ResponsiveSize.Sizes(15),
},
formInput: {
  borderWidth: 0,
  borderColor: '#333',
},
titleContainer1: {
  width:'92%',
  flex:1,
  flexDirection: 'column',
  justifyContent: 'center',
  alignItems: 'center',
  borderBottomWidth:1,
  borderBottomColor:'#000'
},
formContainer:{
  flexDirection: 'row',
//  paddingLeft:AppSizes.ResponsiveSize.Padding(5),
//  paddingRight:AppSizes.ResponsiveSize.Padding(5),
},
imageContainer:{
  flex:1.3,
  alignItems: 'flex-end',
  //backgroundColor:'red',
},
});
