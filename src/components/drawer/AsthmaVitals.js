import React, { Component } from 'react';
import {AsyncStorage,StyleSheet,Text,View,Image,TouchableOpacity,Platform,Alert,ScrollView,TextInput,Button,StatusBar} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions, StackActions  } from 'react-navigation';
import Spacer from '../common/Spacer'
import Spinner from 'react-native-loading-spinner-overlay';
import Header from '../common/signupHeader'
import CommonButton from '../common/CommonButton'
import Swiper from 'react-native-swiper';
import Animbutton from './animAnsbutton'
import AnimSinglebutton from './animSingleAnsOptions';
import { jsonData } from '../data/Question';
import { pickerData } from '../data/pickerData';
import Picker from 'react-native-picker';
import Orientation from 'react-native-orientation'
import FloatingLabelBox from '../common/FloatingLabel'
import FloatingLabel from 'react-native-floating-labels';
import LinearGradient from 'react-native-linear-gradient';
import * as commonFunctions from '../../utils/CommonFunctions'

var Constant = require('../../api/ApiRules').Constant;
var WebServices = require('../../api/ApiRules').WebServices;

/* Images */
//var thankyou_icon = require('../../themes/Images/thank-you.png');
var logo = require('../../themes/Images/logo.png');
var back_icon = require( '../../themes/Images/left-arrow.png')
var oxy_icon = require('../../themes/Images/oxy.png');
var temp_icon = require('../../themes/Images/temp.png');
var heartRate_icon = require('../../themes/Images/heart_rate.png');
var peak_before = require('../../themes/Images/inhealer.png');

var help_audio=require('../../themes/sound/VitalHelp.mp3')

var current_date
let _that
let triage_data = []
let vital_data = []
let final_vital_data = []
let final_med_data = []
let python_result=[]
let options_arr=[]
let vitals=[]
let vitals_api=[]
const MyStatusBar = ({backgroundColor, ...props}) => (
  <View style={[styles.statusBar, { backgroundColor }]}>
    <StatusBar translucent backgroundColor={backgroundColor} {...props} />
  </View>
);

var oxygen_data = [];
var heart_data = [];
var fev_data= [];
var temperature_data= [];

//heart_data
heart_data.push({name: 'None', id:0})
for( var j = 30; j <151; j++){
  var numbers = {
      name: j+' bpm',
      id: j
  };
  heart_data.push(numbers);
}

//fev_data
for( var k = 0; k <7; k++){
  var numbers = {
      name: k+'L',
      id: k
  };
  fev_data.push(numbers);
}

export default class VitalScreen extends Component {
  constructor (props) {
      super(props)

      this.state = {
        uid:'',
        isVisible: false,
        Modal_Visibility: false,
        status:'ans',
        oxygen_sat: 0,
        heart_rate: 0,
        items2: pickerData.heart_data,
        temperature: 0,
        items3: pickerData.temprature,
        temperature_val:'',
        screentype:'',
        fev: 0,
        items4: fev_data,
        python_result:[],
        baseline_heart_rate:'',
        baseline_oxygen_sat:'',
        api:0,
        peak:0,
        isshow1:false,
        isshow2:false,
        isshow3:false
  },
    _that = this;
  }

componentDidMount() {
  Orientation.lockToPortrait();
  AsyncStorage.getItem('loggedIn').then((value) => {
    var loggedIn = JSON.parse(value);
    if (loggedIn) {
     AsyncStorage.getItem('UserData').then((UserData) => {
          const data = JSON.parse(UserData)
          console.log(data)
          var uid=data.id;
          _that.setState({uid:  uid});
      })
    }
  })
}

validationAndApiParameter(apiKey,variable) {
  if(apiKey=='saveVital'){
    var data = {
        uid: this.state.uid,
        variable:JSON.stringify(variable)
    };
    console.log(data);
    _that.setState({isVisible: true});
    this.postToApiCalling('POST', apiKey, Constant.URL_saveVital,data);
  }
  else if(apiKey=='saveRandom'){
    var vital = [{var_name:'vital',value:1}]
    var json_options_arr = JSON.stringify(vital);

    var data = {
      uid: this.state.uid,
      variable : json_options_arr,
    };
    console.log(data);
    _that.setState({isVisible: true});
    this.postToApiCalling('POST', apiKey, Constant.URL_saveRandom,data);
  }
}

postToApiCalling(method, apiKey, apiUrl, data) {
  //console.log(apiUrl +" "+ apiKey)
    new Promise(function(resolve, reject) {
        if (method == 'POST') {
            resolve(WebServices.callWebService(apiUrl, data));
        } else {
          //console.log('callWebService_POST')
            resolve(WebServices.callWebService_POST(apiUrl, data));
        }
    }).then((jsonRes) => {
        //console.log(jsonRes)
          _that.setState({isVisible: false});
        if(apiKey=="python_vital"){
          _that.apiSuccessfullResponse(apiKey, jsonRes)
        }
        else{
          if ((!jsonRes) || (jsonRes.code == 0)) {
            setTimeout(() => {
                  Alert.alert(jsonRes.message);
              }, 200);
          } else {
              if (jsonRes.code == 1) {
                  _that.apiSuccessfullResponse(apiKey, jsonRes)
              }
          }
        }
    }).catch((error) => {
        console.log("ERROR" + error);
        _that.setState({ isVisible: false })
        setTimeout(() => {
            Alert.alert("Server Error");
        }, 200);
    })
}

apiSuccessfullResponse(apiKey, jsonRes) {
  console.log(jsonRes);
  if(apiKey=='saveVital'){
    _that.validationAndApiParameter('saveRandom')
  }
  if(apiKey=='saveRandom'){
    const resetAction = StackActions.reset({
          index: 0,
          actions: [NavigationActions.navigate({ routeName: 'AsthmaToDo' })],
    });
    _that.props.navigation.dispatch(resetAction);
  }
}

  updateAlert = () => {
        this.setState({Modal_Visibility: !this.state.Modal_Visibility});
  }

  next= () =>{
    var variable=[]
    var date = new Date().getDate();
    var month = new Date().getMonth() + 1;
    var year = new Date().getFullYear();
    current_date=year + '-' + month + '-' + date
    console.log(current_date);

    console.log('heart',this.state.heart_rate);
    console.log('peak',this.state.peak);
    console.log('temp',this.state.temperature);

    if(this.state.heart_rate!=0){
      variable.push({var_name:"VTL_BM_P",value:this.state.heart_rate})
    }
    if(this.state.peak!=0){
      variable.push({var_name:"VTL_BM_PEF",value:this.state.peak})
    }
    if(this.state.temperature!=0){
      variable.push({var_name:"VTL_BM_TMP",value:this.state.temperature})
    }
    console.log(variable.length);
    if(variable.length>0){
      variable.push({var_name:"VTL_BM_TMP",value:this.state.temperature})
      _that.validationAndApiParameter('saveVital',variable)
    }
    else{
      _that.props.navigation.navigate('AsthmaToDo')
    }
  }

  check_data(val,label,type){
    console.log('val',val);
    console.log('label',label);
    console.log(type);
    const { baseline_oxygen_sat,baseline_heart_rate } = this.state
    if(type=="heart_rate"){
      if(val=='None'){
        this.setState({heart_rate: 0})
      }
      else{
        this.setState({heart_rate: val})
      }

    }
    if(type=="temperature"){
      this.setState({temperature: label})
    }
  }

  temperature() {
      Picker.init({
          pickerData: this.state.items3,
          onPickerConfirm: pickedValue => {
              this.setState({temperature:Number(pickedValue)})
          },
          onPickerCancel: pickedValue => {
              console.log('area', pickedValue);
          },
          onPickerSelect: pickedValue => {
              console.log('area', pickedValue);
          }
      });
      Picker.show();
  }

  heart() {
      Picker.init({
          pickerData: this.state.items2,
          onPickerConfirm: pickedValue => {
              var picked=pickedValue.toString()
              var ar=picked.split(" ")
              var data=ar[0]
              this.setState({heart_rate:Number(data)})
          },
          onPickerCancel: pickedValue => {
              console.log('area', pickedValue);
          },
          onPickerSelect: pickedValue => {
              console.log('area', pickedValue);
          }
      });
      Picker.show();
  }


  render() {
    const headerProp = {
      title: 'Assessing your health!',
      screens: 'AsthmaVitals',
      qus_content:'Sit down and relax for at least two minutes before taking the measurements. If you regularly on oxygen, make sure you are wearing your oxygen mask and that the amount of oxygen flowing is set to the amount prescribed by your doctor. Use your pulse oximeter device following its instructions and measure your heart rate and oxygen saturation. Select the values that correspond to those displayed in your device. Use your thermometer to measure your temperature and select its value from the scroll wheel. Click “Next” when you are done!',
      qus_audio:help_audio
    };


      var DropDown=<View style={{margin:5}}>

         <TouchableOpacity onPress={()=>this.heart()} style={{flexDirection:'row',paddingTop:20,paddingBottom:10}}>
           <View style={styles.icon}>
               <Image style={styles.cicon} source={heartRate_icon}/>
           </View>
           <View style={styles.texttbox}>
             <Text style={{color:'#000',fontSize:AppSizes.ResponsiveSize.Sizes(18)}}>Heart Rate</Text>
             {this.state.heart_rate>0?<Text style={{color:'#000',fontSize:AppSizes.ResponsiveSize.Sizes(15)}}>{this.state.heart_rate} bmp</Text>:<Text style={{color:'#9d9d9d',fontSize:AppSizes.ResponsiveSize.Sizes(15)}}>Select Your Heart Rate</Text>}
           </View>
         </TouchableOpacity>
         <View style={{width:'100%',height:1,backgroundColor:'#000'}}/>

          <TouchableOpacity onPress={()=>this.temperature()} style={{flexDirection:'row',paddingTop:20,paddingBottom:10}}>
            <View style={styles.icon}>
                <Image style={styles.cicon} source={temp_icon}/>
            </View>
            <View style={styles.texttbox}>
              <Text style={{color:'#000',fontSize:AppSizes.ResponsiveSize.Sizes(18)}}>Temperature</Text>
              {this.state.temperature>0?<Text style={{color:'#000',fontSize:AppSizes.ResponsiveSize.Sizes(15)}}>{this.state.temperature}</Text>:<Text style={{color:'#9d9d9d',fontSize:AppSizes.ResponsiveSize.Sizes(15)}}>Select Your Temperature</Text>}
            </View>
          </TouchableOpacity>
          <View style={{width:'100%',height:1,backgroundColor:'#000',}}/>
           <FloatingLabelBox labelIcon={peak_before} >
            <FloatingLabel
                labelStyle={styles.labelInput}
                inputStyle={styles.input}
                style={styles.formInput}
                value={this.state.weight}
                placeholder='Enter Peak Expiratory Flow'
                ref="peak"
                autoCapitalize = 'none'
                keyboardType={ 'numeric'}
                returnKeyType = { "next" }
                onChangeText={peak=> this.setState({peak:(peak/1000).toString()})}
              >Peak Expiratory Flow</FloatingLabel>
           </FloatingLabelBox>
      </View>;



    return (
      <View style={styles.container}>
      <Header info={headerProp} navigation={_that.props.navigation} updateAlert={this.updateAlert}/>
      <View style={[styles.content,this.state.Modal_Visibility ? {backgroundColor: 'rgba(0,0,0,0.4)',zIndex:5} : '#ffffff']}>

      <ScrollView style={{paddingTop: 10}}>

      <View style={styles.container2}>
          { DropDown }
      </View>

      <View style={styles.container3}>
        <TouchableOpacity style={styles.btn} activeOpacity={.6} onPress={this.next}>
          <CommonButton label='Submit'/>
          </TouchableOpacity>
      </View>
    </ScrollView>
    </View>

      <Spinner visible={this.state.isVisible}  />
    </View>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
    backgroundColor:'#ffffff'
  },
  content: {
      flex: 1,
      height:'100%',
      paddingLeft:'0%',
      paddingRight:'0%',
      paddingBottom:'5%',
      //backgroundColor:'red'
  },

  image: {
    flex: 1,
    width: undefined,
    height: undefined,
    resizeMode:'contain'
  },
  container1:{
    flex:2,
    flexDirection:'column',
    alignItems:'center',
    justifyContent:'center'
  },
  heading:{
    fontSize:AppSizes.ResponsiveSize.Sizes(18),
    color:'#000',
    justifyContent:'center',
    alignItems:'center',
    fontWeight:'600',
    width:'89%',

  },
  hr:{
    width:50,
    height:3,
    backgroundColor:'#000',
    marginTop:10,
},
  container2:{
    flex:6,
    flexDirection:'column',
    padding: AppSizes.ResponsiveSize.Padding(5),
    width:AppSizes.screen.width,

  },
  labelInput: {
    color: AppColors.contentColor,
    //fontWeight:'300',
    fontSize:AppSizes.ResponsiveSize.Sizes(13),
  },
  input: {
    borderWidth: 0,
    color: AppColors.contentColor,
    fontWeight:'300',
    fontSize:AppSizes.ResponsiveSize.Sizes(15),
  },
  formInput: {
    borderWidth: 0,
    borderColor: '#333',
    marginTop:20,
    marginBottom:10
  }
  ,
  container3:{
    flex:2,
    justifyContent:'flex-start',
    alignItems:'center',

  },

  dropdowntext:{
    fontSize:AppSizes.ResponsiveSize.Sizes(14),
    fontWeight:'500'
  },
  ansfield:{
    fontSize:AppSizes.ResponsiveSize.Sizes(13),
    color:'#5a5a5a',
  },
  btn:{
    width:'100%',
    marginBottom:20
  },
  imageContainer:{
    flex:1,
    alignItems: 'flex-start',
    //backgroundColor:'red',
  },
  icon:{
    width:'10%',
    marginRight:10,
    alignItems:'center',
    justifyContent:'center',
  },
  texttbox:{
    width:'90%'
  },
  cicon:{
    width:30,
    height:30
  }
});
