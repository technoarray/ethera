import React, { Component } from 'react';
import {AsyncStorage,StyleSheet,Text,View,Image,TouchableOpacity,Platform,ScrollView,TextInput,Alert,Keyboard} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions } from 'react-navigation'
import LinearGradient from 'react-native-linear-gradient';
import Header from '../common/signupHeader'
import * as commonFunctions from '../../utils/CommonFunctions'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import {RadioGroup, RadioButton} from 'react-native-flexi-radio-button'
import CommonButton from '../common/CommonButton'
import FlipToggle from 'react-native-flip-toggle-button';
import { jsonData } from '../data/Question';
import FloatingLabelBox from '../common/FloatingLabelBox'
import FloatingLabel from 'react-native-floating-labels';
import { pickerData } from '../data/pickerData';
import Picker from 'react-native-picker';
import Orientation from 'react-native-orientation'
import Spinner from 'react-native-loading-spinner-overlay';

var Constant = require('../../api/ApiRules').Constant;
var WebServices = require('../../api/ApiRules').WebServices;

let _that
let options_arr=[]
let python_result=[]
var diagnosis=[]
var medication=[]
var inhaler='0'
var low='0'
var medium='0'
var high='0'
var longg='0'
var combined='0'
var injectible='0'
var age
var weight
var height

var arrow_icon=require('../../themes/Images/arrow_icon_196.png');
var age_icon = require('../../themes/Images/calender.png');
var weight_icon = require('../../themes/Images/weight.png');
var height_icon = require('../../themes/Images/height.png');
var edit_icon = require('../../themes/Images/edit-pin.png');
var edit_icon1 = require('../../themes/Images/edit.png');
var gender_icon = require('../../themes/Images/gender.png');

var help_audio=require('../../themes/sound/HealthProfileHelp.mp3')

var age_data = [];
//gender_data
var gender_data=['Male','Female']
//heart_data
for( var j = 18; j <=100; j++){
  age_data.push(j.toString());
}

export default class AsthmaHealthProfile extends Component {
    constructor (props) {
      super(props)
      this.state = {
        steroid_user:false,
        daily_activities:false,
        admitted_hosp:false,
        admitted_icu:false,
        smoke:false,
        live_alone:false,
        isVisible: false,
        conditions :[],
        risk_factor :[],
        Modal_Visibility: false,
        tab_conditions:true,
        tab_vba:false,
        height:this.props.navigation.state.params.height,
        height_data:pickerData.height_data,
        age_data: age_data,
        item:["Female","Male"],
        birth_date:this.props.navigation.state.params.age,
        uid: '',
        weight:this.props.navigation.state.params.weight,
        gender:'',
        act_score:'',
        inhaler:'',
        low:'',
        medium:'',
        high:'',
        longg:'',
        combined:'',
        injectible:'',
        inhaler_sec:false,
        low_sec:false,
        medium_sec:false,
        high_sec:false,
        longg_sec:false,
        combined_sec:false,
        injectible_sec:false
    },
    _that = this;
  }

  componentDidMount() {
    Orientation.lockToPortrait();
    AsyncStorage.getItem('loggedIn').then((value) => {
        var loggedIn = JSON.parse(value);
        if (loggedIn) {
            AsyncStorage.getItem('UserData').then((UserData) => {
                const data = JSON.parse(UserData)
                console.log(data);

                var uid=data.id;
                _that.setState({
                  uid:  uid,
                });
                _that.validationAndApiParameter('getData',uid)
            })
        }
    })
  }

  vbaUpdate=()=>{
    _that.validationAndApiParameter('saveData');
  }

  validationAndApiParameter(apiname,uid) {
      if(apiname == 'getData'){
        var data = {
            uid: uid
        };
        console.log(data);
        _that.setState({isVisible: true});
        this.postToApiCalling('POST', 'getData', Constant.URL_asthmaProfileVariable,data);
      }
      else if (apiname=='saveData') {
        var error=0;
        const { oxygen_therapy, daily_activities, smoke, live_alone,height,weight,birth_date,isVisible } = this.state
        if(height.length <= 0){
          Alert.alert("Error","Height is required");
          error=1;
        }
        if(weight.length <= 0){
          Alert.alert("Error","Weight is required");
          error=1;
        }
        if(birth_date.length <= 0){
          Alert.alert("Error","Age is required");
          error=1;
        }

        if(error==0){
          if(this.state.smoke){
            options_arr.push({var_name: 'PRF_BH_SMK',value: 1})
          }
          else{
            options_arr.push({var_name: 'PRF_BH_SMK',value: 0})
          }
          if(this.state.live_alone){
            options_arr.push({var_name: 'CBY_DOM_LA',value: 1})
          }
          else{
            options_arr.push({var_name: 'CBY_DOM_LA',value: 0})
          }
          if(this.state.admitted_icu){
            options_arr.push({var_name: 'OUTP_ICU',value: 1})
          }
          else{
            options_arr.push({var_name: 'OUTP_ICU',value: 0})
          }
          if(this.state.admitted_hosp){
            options_arr.push({var_name: 'OUTP_HOSP',value: 1})
          }
          else{
            options_arr.push({var_name: 'OUTP_HOSP',value: 0})
          }
          if(this.state.daily_activities){
            options_arr.push({var_name: 'CBY_DOM_ASS',value: 1})
          }
          else{
            options_arr.push({var_name: 'CBY_DOM_ASS',value: 0})
          }
          if(this.state.steroid_user){
            options_arr.push({var_name: 'CND_STEROIDS',value: 1})
          }
          else{
            options_arr.push({var_name: 'CND_STEROIDS',value: 0})
          }
          if(this.state.gender=='Male'){
            options_arr.push({var_name:'BL_GENDER_MALE',value:1})
            options_arr.push({var_name:'BL_GENDER_FEMALE',value:0})

          }
          else{
            options_arr.push({var_name:'BL_GENDER_MALE',value:0})
            options_arr.push({var_name:'BL_GENDER_FEMALE',value:1})
          }
          options_arr.push({var_name:'BL_AGE',value:this.state.birth_date})
          options_arr.push({var_name:'BL_WEIGHT',value:this.state.weight})
          options_arr.push({var_name:'OUTP_HEIGHT',value:this.state.height})


          var json_options_arr = JSON.stringify(options_arr);

          var data = {
            uid: this.state.uid,
            variable : json_options_arr,
          };
          console.log(data)
          _that.setState({isVisible: true});
          this.postToApiCalling('POST', apiname, Constant.URL_asthmaVariables,data);
        }
      }
      else if (apiname='getComborboties') {
        var data = {
          uid: this.state.uid,
        };
        console.log(data)
        _that.setState({isVisible: true});
        this.postToApiCalling('POST', apiname, Constant.URL_GetComborboties,data);
      }
    }

  postToApiCalling(method, apiKey, apiUrl, data) {
    //console.log(apiUrl)
      new Promise(function(resolve, reject) {
          if (method == 'POST') {
              resolve(WebServices.callWebService(apiUrl, data));
          } else {
              resolve(WebServices.callWebService_GET(apiUrl, data));
          }
      }).then((jsonRes) => {
          //console.log(jsonRes)
            _that.setState({ isVisible: false })
          if ((!jsonRes) || (jsonRes.code == 0)) {
            setTimeout(() => {
                  Alert.alert(jsonRes.message);
              }, 200);
          } else {
              if (jsonRes.code == 1) {
                  _that.apiSuccessfullResponse(apiKey, jsonRes)
              }
          }
      }).catch((error) => {
          console.log("ERROR" + error);
          _that.setState({ isVisible: false })
          setTimeout(() => {
              Alert.alert("Server Error");
          }, 200);
      })
  }

  apiSuccessfullResponse(apiKey, jsonRes) {
    if(apiKey == 'getData'){
      console.log(jsonRes);
      diagnosis=jsonRes.diagnosis
      medication=jsonRes.medication

      this.setState({
        birth_date:jsonRes.profile.BL_AGE,
        weight:jsonRes.profile.BL_WEIGHT,
        height:jsonRes.profile.OUTP_HEIGHT,
        diagnosis:diagnosis,
        medication:medication,
        act_score:jsonRes.profile.act_score
      });
      if(jsonRes.profile.BL_GENDER_FEMALE=='1'){
        this.setState({gender:'Female'})
      }
      if(jsonRes.profile.BL_GENDER_MALE=='1'){
        this.setState({gender:'Male'})
      }

      if(jsonRes.profile.CBY_DOM_LA=='1'){
        this.setState({live_alone:true})
      }
      else{
        this.setState({live_alone:false})
      }

      if(jsonRes.profile.CND_STEROIDS=='1'){
        this.setState({steroid_user:true})
      }
      else{
        this.setState({steroid_user:false})
      }

      if(jsonRes.profile.PRF_BH_SMK=='1'){
        this.setState({smoke:true})
      }
      else{
        this.setState({smoke:false})
      }

      if(jsonRes.profile.OUTP_ICU=='1'){
        this.setState({admitted_icu:true})
      }
      else{
        this.setState({admitted_icu:false})
      }

      if(jsonRes.profile.CBY_DOM_ASS=='1'){
        this.setState({daily_activities:true})
      }
      else{
        this.setState({daily_activities:false})
      }

      if(jsonRes.profile.OUTP_HOSP=='1'){
        this.setState({admitted_hosp:true})
      }
      else{
        this.setState({admitted_hosp:false})
      }
      _that.validationAndApiParameter('getComborboties')
    }
    if(apiKey=='saveData'){
      _that.validationAndApiParameter('getData',this.state.uid)
    }
    if(apiKey=='getComborboties'){
      console.log(jsonRes);
      if(jsonRes.medical.inhaler == '1'){
        this.setState({inhaler_sec:true})
        this.setState({inhaler:'Albuterol Inhaler or Rescue Nebulizer'})
      }
      else{
        this.setState({inhaler_sec:false})
      }

      if(jsonRes.medical.low == '1'){
        this.setState({low_sec:true})
        this.setState({low:'Low-dose Inhaled steroid'})
      }
      else{
        this.setState({low_sec:false})
      }

      if(jsonRes.medical.medium == '1'){
        this.setState({medium_sec:true})
        this.setState({medium:'Medium-dose inhaled steroid'})
      }
      else{
        this.setState({medium_sec:false})
      }

      if(jsonRes.medical.high == '1'){
        this.setState({high_sec:true})
        this.setState({high:'High-dose inhaled steroid'})
      }
      else{
        this.setState({high_sec:false})
      }

      if(jsonRes.medical.longg == '1'){
        this.setState({longg_sec:true})
        this.setState({longg:'Long Acting Bronchodilator'})
      }
      else{
        this.setState({longg_sec:false})
      }

      if(jsonRes.medical.combined == '1'){
        this.setState({combined_sec:true})
        this.setState({combined:'Combined Steroid + Bronchodilator'})
      }
      else{
        this.setState({combined_sec:false})
      }

      if(jsonRes.medical.injectible == '1'){
        this.setState({injectible_sec:true})
        this.setState({injectible:'Injectible Medication/Biologic'})
      }
      else{
        this.setState({injectible_sec:false})
      }
    }
  }

  updateAlert = (visible) => {
        this.setState({Modal_Visibility: !this.state.Modal_Visibility});
  }

  onSelect(index, value){
    this.setState({
      text: `Selected index: ${index} , value: ${value}`
    })
  }

  chartbtn1(){
    _that.props.navigation.navigate('AsthmaCharts', {vital_type:'pef_sat' });
  }

  chartbtn2(){
    _that.props.navigation.navigate('AsthmaCharts', {vital_type:'heart_rate' });
  }

  chartbtn3(){
    _that.props.navigation.navigate('AsthmaCharts', {vital_type:'temp' });
  }

 conditionSave=()=>{
   _that.props.navigation.navigate('MedicalQusScreen');
 }

 conditionFunc(qus,arr,qus_arr){
   if(qus=='CND_CKD'||qus=='CBY_ANE'||qus=='CND_PL_COPD'||qus=='CND_CD_CHF'||qus=='CND_GERD'||qus=='CND_CD_CAD'||qus=='CND_HYP'||qus=='CND_DIA'){
       _that.props.navigation.navigate('MedicalAsthmaQueScreen', {ques: 0,ques_title:arr,qus_label:qus_arr})
   }
   else if(qus=='inhaler'||qus=='low'||qus=='medium'||qus=='hign'||qus=='long' || qus=='combined' || qus=='injectible'){
       _that.props.navigation.navigate('MedicalAsthmaQueScreen', {ques: 2,ques_name:qus,qus_label:''})
   }else if (qus =='MED_COMP_25' || qus == 'MED_COMP_50' || qus == 'MED_COMP_75' || qus=='MED_COMP_100') {
     _that.props.navigation.navigate('MedicalAsthmaQueScreen', {ques: 4,ques_name:qus,qus_label:''})
   }
 }
  activeTab(title){
    return (
      <LinearGradient colors={['#36c4d8', '#2aa5b4', '#198391', '#15767d']} style={styles.tab_linear}>
      <Text allowFontScaling={false} style={[styles.textbtn,styles.tab_activeText]}>{title}</Text>
      </LinearGradient>
    );
  }

  inactiveTab(title){
    return (
      <LinearGradient colors={['#ebebeb', '#ebebeb', '#ebebeb', '#ebebeb']} style={styles.tab_linear}>
      <Text allowFontScaling={false} style={styles.textbtn}>{title}</Text>
      </LinearGradient>
    );
  }

  gender() {
        Picker.init({
            pickerData: this.state.item,
            onPickerConfirm: pickedValue => {
              console.log(pickedValue.toString());
              if(pickedValue=='Female'){
                this.setState({gender:'0'})
              }
              else{
                this.setState({gender:'1'})
              }
            },
            onPickerCancel: pickedValue => {
                console.log('area', pickedValue);
            },
            onPickerSelect: pickedValue => {
                console.log('area', pickedValue);
            }
        });
        Picker.show();
    }
    age() {
        Picker.init({
            pickerData: this.state.age_data,
            onPickerConfirm: pickedValue => {
                this.setState({birth_date:pickedValue.toString()})
            },
            onPickerCancel: pickedValue => {
                console.log('area', pickedValue);
            },
            onPickerSelect: pickedValue => {
                //Picker.select(['山东', '青岛', '黄岛区'])
                console.log('area', pickedValue);
            }
        });
        Picker.show();
    }
    height() {
        Picker.init({
            pickerData: this.state.height_data,
            onPickerConfirm: pickedValue => {
                this.setState({height:pickedValue.toString()})
            },
            onPickerCancel: pickedValue => {
                console.log('area', pickedValue);
            },
            onPickerSelect: pickedValue => {
                //Picker.select(['山东', '青岛', '黄岛区'])
                console.log('area', pickedValue);
            }
        });
        Picker.show();
    }

  render() {
    const headerProp = {
      title: 'Health Profile',
      screens: 'AsthmaHealthProfile',
      qus_content:'Maintain the medical history related to your condition. \n\n Click “Update” to change your Profile, conditions and flip the switches to update the risk factors.\n\n Access your charts through “Access charts” link or by clicking on the icons on the measurements.',
      qus_audio:help_audio
    };

    let _this = this
      const conditions = diagnosis
      const med=medication
      var answer='';
      var arr=[];
      var qus_arr=[];

      const medications =Object.keys(med).map( function(k) {
        if(med[k]== 'MED_COMP_25'){answer='Less than 25% of the time'; arr.push('Less than 25% of the time');qus_arr.push('MED_COMP_25')}
        else if(med[k]== 'MED_COMP_50'){answer='Between 25% and 50% of the time'; arr.push('Between 25% and 50% of the time');qus_arr.push('MED_COMP_50')}
        else if(med[k]== 'MED_COMP_75'){answer='Between 50% and 75% of the time'; arr.push('Between 50% and 75% of the time');qus_arr.push('MED_COMP_75')}
        else if(med[k]== 'MED_COMP_100'){answer='Greater than 75% of the time'; arr.push('Greater than 75% of the time');qus_arr.push('MED_COMP_100')}

        return (
          <TouchableOpacity style={styles.lines} onPress={()=> _that.props.navigation.navigate('Medication')}>
            <Text allowFontScaling={false} style={styles.conditiontext}> • {answer}</Text>
          </TouchableOpacity>
        )
      });

      const conditions_option = Object.keys(conditions).map( function(k) {

        if(conditions[k]== 'CND_GERD'){answer='Acid Reflux'; arr.push('Acid Reflux');qus_arr.push('CND_GERD')}
        else if(conditions[k]== 'CND_CKD'){answer='Chronic Kidney disease'; arr.push('Chronic Kidney disease');qus_arr.push('CND_CKD')}
        else if(conditions[k]== 'CND_PL_COPD'){answer='Chronic Obstructive Pulmonary Disease (COPD)'; arr.push('Chronic Obstructive Pulmonary Disease (COPD)');qus_arr.push('CND_PL_COPD')}
        else if(conditions[k]== 'CND_DIA'){answer='Diabetes'; arr.push('Diabetes');qus_arr.push('CND_DIA')}
        else if(conditions[k]== 'CND_CD_CAD'){answer='Coronary Artery Disease'; arr.push('Coronary Artery Disease');qus_arr.push('CND_CD_CAD')}
        else if(conditions[k]== 'CND_CD_CHF'){answer='Congestive Heart Failure'; arr.push('Congestive Heart Failure');qus_arr.push('CND_CD_CHF')}
        else if(conditions[k]== 'CND_HYP'){answer='High Blood Pressure'; arr.push('High Blood Pressure');qus_arr.push('CND_HYP')}
        else if(conditions[k]== 'CBY_ANE'){answer='Anemia'; arr.push('Anemia');qus_arr.push('CBY_ANE')}

        return (
          <TouchableOpacity style={styles.lines} onPress={()=>_that.props.navigation.navigate('ConditionQus',{qus_no:0})}>
            <Text allowFontScaling={false} style={styles.conditiontext}> • {answer}</Text>
          </TouchableOpacity>
        )
      });


    return (
      <View style={styles.container}>
      <Header info={headerProp} navigation={_that.props.navigation} updateAlert={this.updateAlert}/>
      <View style={[styles.content,this.state.Modal_Visibility ? {backgroundColor: 'rgba(0,0,0,0.4)',zIndex:5} : '#ffffff']}>

      <View style={{flex:1,paddingLeft:AppSizes.ResponsiveSize.Padding(2),paddingRight:AppSizes.ResponsiveSize.Padding(2)}}>
             <View style={{flex:(Platform.OS === 'ios') ? 1.2 :1.2, marginTop:(Platform.OS === 'android') ? AppSizes.ResponsiveSize.Padding(2) : AppSizes.ResponsiveSize.Padding(0),}}>
               <View style={styles.boxDesignthree}>
               {this.props.navigation.state.params.peak_flow!='-'?
                 <TouchableOpacity activeOpacity={.6} onPress={()=>this.chartbtn1()} style={[styles.shadow,styles.boxcontainer1]}>
                       <Text allowFontScaling={false} style={styles.boxnumber}>{this.props.navigation.state.params.peak_flow} L/Min</Text>
                        <Text allowFontScaling={false} style={styles.boxtitlesmall}>Peak Flow </Text>
                  </TouchableOpacity>
               :
                 <TouchableOpacity activeOpacity={.6} onPress={()=>this.chartbtn1()} style={[styles.shadow,styles.boxcontainer1]}>
                       <Text allowFontScaling={false} style={styles.boxnumber}>{this.props.navigation.state.params.peak_flow}</Text>
                        <Text allowFontScaling={false} style={styles.boxtitlesmall}>Peak Flow </Text>
                  </TouchableOpacity>
               }
               {this.props.navigation.state.params.heart_rate!='-'?
               <TouchableOpacity activeOpacity={.6} onPress={()=>this.chartbtn2()} style={[styles.shadow,styles.boxcontainer1]}>
                    <Text allowFontScaling={false} style={styles.boxnumber}>{this.props.navigation.state.params.heart_rate} bpm</Text>
                     <Text allowFontScaling={false} style={styles.boxtitlesmall}>Heart Rate</Text>
                </TouchableOpacity>
               :
               <TouchableOpacity activeOpacity={.6} onPress={()=>this.chartbtn2()} style={[styles.shadow,styles.boxcontainer1]}>
                    <Text allowFontScaling={false} style={styles.boxnumber}>{this.props.navigation.state.params.heart_rate}</Text>
                     <Text allowFontScaling={false} style={styles.boxtitlesmall}>Heart Rate</Text>
                </TouchableOpacity>
               }
               {this.props.navigation.state.params.temperature!='-'?
               <TouchableOpacity activeOpacity={.6} onPress={()=>this.chartbtn3()} style={[styles.shadow,styles.boxcontainer1]}>
                   <Text allowFontScaling={false} style={styles.boxnumber}>{this.props.navigation.state.params.temperature} C</Text>
                    <Text allowFontScaling={false} style={styles.boxtitlesmall}>Temperature</Text>
               </TouchableOpacity>
               :
               <TouchableOpacity activeOpacity={.6} onPress={()=>this.chartbtn3()} style={[styles.shadow,styles.boxcontainer1]}>
                   <Text allowFontScaling={false} style={styles.boxnumber}>{this.props.navigation.state.params.temperature}</Text>
                    <Text allowFontScaling={false} style={styles.boxtitlesmall}>Temperature</Text>
               </TouchableOpacity>
               }
               </View>
             </View>
             <View style={{flex:3}}>
             <ScrollView>

             <View styles={{alignItems:'center',width:'100%'}}>
               <Text allowFontScaling={false} style={styles.boxtitle22}>Profile</Text>

               <View style={styles.border} />

               <View style={styles.formContainer}>
               { this.state.gender != "" ?
                 <View style={{width:'100%'}}>
                    <View style={{marginTop: 10,flexDirection:'row', marginLeft: 10,paddingTop:20,paddingBottom:10}}>
                      <View style={styles.icon}>
                        <Image style={styles.cicon} source={gender_icon}/>
                      </View>
                      <View style={styles.texttbox}>
                        <Text style={{color:'#000',fontSize:AppSizes.ResponsiveSize.Sizes(15)}}>Gender</Text>
                        {this.state.gender.length>0?<Text style={{color:'#000',fontSize:AppSizes.ResponsiveSize.Sizes(15)}}>{this.state.gender}</Text>:<Text style={{color:'#bdc3c7',fontSize:AppSizes.ResponsiveSize.Sizes(15)}}>Select your age</Text>}
                      </View>
                      <TouchableOpacity onPress={()=>this.gender()} style={styles.icon1}>
                          <Image style={styles.cicon} source={edit_icon}/>
                      </TouchableOpacity>
                    </View>
                    <View style={{width:'100%',height:1,backgroundColor:'#000'}}/>
                  </View>
                :
                  null
                }
               </View>

               <View style={styles.formContainer}>
               { this.state.birth_date != "" ?
                 <View style={{width:'100%'}}>
                    <View style={{flexDirection:'row', marginLeft: 10,paddingTop:20,paddingBottom:10}}>
                      <View style={styles.icon}>
                        <Image style={styles.cicon} source={age_icon}/>
                      </View>
                      <View style={styles.texttbox}>
                        <Text style={{color:'#000',fontSize:AppSizes.ResponsiveSize.Sizes(15)}}>Age</Text>
                        {this.state.birth_date.length>0?<Text style={{color:'#000',fontSize:AppSizes.ResponsiveSize.Sizes(15)}}>{this.state.birth_date}</Text>:<Text style={{color:'#bdc3c7',fontSize:AppSizes.ResponsiveSize.Sizes(15)}}>Select your age</Text>}
                      </View>
                      <TouchableOpacity onPress={()=>this.age()} style={styles.icon1}>
                          <Image style={styles.cicon} source={edit_icon}/>
                      </TouchableOpacity>
                    </View>
                    <View style={{width:'100%',height:1,backgroundColor:'#000'}}/>
                  </View>
                :
                  null
                }
               </View>

               <View style={styles.formContainer}>
               { this.state.height != "" ?
                 <View style={{width:'100%'}}>
                    <View style={{flexDirection:'row', marginLeft: 10,paddingTop:20,paddingBottom:10}}>
                      <View style={styles.icon}>
                          <Image style={styles.cicon} source={height_icon}/>
                      </View>
                      <View style={styles.texttbox}>
                        <Text style={{color:'#000',fontSize:AppSizes.ResponsiveSize.Sizes(15)}}>Height</Text>
                        {this.state.height.length>0?<Text style={{color:'#000',fontSize:AppSizes.ResponsiveSize.Sizes(15)}}>{this.state.height}</Text>:<Text style={{color:'#bdc3c7',fontSize:AppSizes.ResponsiveSize.Sizes(15)}}>Enter height</Text>}
                      </View>
                      <TouchableOpacity onPress={()=>this.height()}  style={styles.icon1}>
                          <Image style={styles.cicon} source={edit_icon}/>
                      </TouchableOpacity>
                    </View>
                    <View style={{width:'100%',height:1,backgroundColor:'#000'}}/>
                  </View>
                :
                  null
                }
               </View>

               <View style={{padding:AppSizes.ResponsiveSize.Padding(2)}} />
               <FloatingLabelBox labelIcon={weight_icon}>
                 <FloatingLabel
                     labelStyle={styles.labelInput}
                     inputStyle={styles.input}
                     style={styles.formInput}
                     value={this.state.weight}
                     placeholder='Enter weight'
                     ref="weight"
                     autoCapitalize = 'none'
                     keyboardType={ 'numeric'}
                     returnKeyType = { "next" }
                     onChangeText={weight=> this.setState({weight})}
                   >Weight</FloatingLabel>
               </FloatingLabelBox>

               {/* <TouchableOpacity style={styles.imageContainer1} onPress={this._keyboardDidShow}>
                <Image source={edit_icon} style={styles.editButton}/>
               </TouchableOpacity> */ }
               <View style={{padding:AppSizes.ResponsiveSize.Padding(2)}} />

              </View>

                {/**  End Profile  **/}

             <View styles={{alignItems:'center',width:'100%'}}>
               <Text allowFontScaling={false} style={styles.boxtitle22}>Conditions</Text>

              <View style={styles.border} />
                {conditions_option}
              </View>
              {/**  End Conditions **/}
              {/* Start Act */}
              <View style={{padding:AppSizes.ResponsiveSize.Padding(2)}} />

                <View styles={{alignItems:'center',width:'100%'}}>
                <Text allowFontScaling={false} style={styles.boxtitle22}>ACT Score</Text>
                <View style={styles.border} />
                {this.state.act_score?
                  <TouchableOpacity style={styles.lines} onPress={()=>_this.props.navigation.navigate('ACTQusScreen')}>
                    <Text allowFontScaling={false} style={styles.conditiontext}> • {this.state.act_score}</Text>
                  </TouchableOpacity>
                :
                  null
                }
                </View>

              <View style={{padding:AppSizes.ResponsiveSize.Padding(2)}} />
              {/* end Act */}

              <View styles={{alignItems:'center',width:'100%'}}>
                <Text allowFontScaling={false} style={styles.boxtitle22}>Medications</Text>

               <View style={styles.border} />
                 {this.state.inhaler_sec?
                   <TouchableOpacity style={styles.lines} onPress={()=>_that.props.navigation.navigate('ConditionQus',{qus_no:2})}>
                     <Text allowFontScaling={false} style={styles.conditiontext}> • {this.state.inhaler}</Text>
                   </TouchableOpacity>
                 :
                     null
                 }
                 {this.state.low_sec?
                   <TouchableOpacity style={styles.lines} onPress={()=>_that.props.navigation.navigate('ConditionQus',{qus_no:2})}>
                     <Text allowFontScaling={false} style={styles.conditiontext}> • {this.state.low}</Text>
                   </TouchableOpacity>
                 :
                     null
                 }
                 {this.state.medium_sec?
                   <TouchableOpacity style={styles.lines} onPress={()=>_that.props.navigation.navigate('ConditionQus',{qus_no:2})}>
                     <Text allowFontScaling={false} style={styles.conditiontext}> • {this.state.medium}</Text>
                   </TouchableOpacity>
                 :
                     null
                 }
                 {this.state.high_sec?
                   <TouchableOpacity style={styles.lines} onPress={()=>_that.props.navigation.navigate('ConditionQus',{qus_no:2})}>
                     <Text allowFontScaling={false} style={styles.conditiontext}> • {this.state.high}</Text>
                   </TouchableOpacity>
                 :
                     null
                 }
                 {this.state.longg_sec?
                   <TouchableOpacity style={styles.lines} onPress={()=>_that.props.navigation.navigate('ConditionQus',{qus_no:2})}>
                     <Text allowFontScaling={false} style={styles.conditiontext}> • {this.state.longg}</Text>
                   </TouchableOpacity>
                 :
                     null
                 }
                 {this.state.combined_sec?
                   <TouchableOpacity style={styles.lines} onPress={()=>_that.props.navigation.navigate('ConditionQus',{qus_no:2})}>
                     <Text allowFontScaling={false} style={styles.conditiontext}> • {this.state.combined}</Text>
                   </TouchableOpacity>
                 :
                     null
                 }
                 {this.state.injectible_sec?
                   <TouchableOpacity style={styles.lines} onPress={()=>_that.props.navigation.navigate('ConditionQus',{qus_no:2})}>
                     <Text allowFontScaling={false} style={styles.conditiontext}> • {this.state.injectible}</Text>
                   </TouchableOpacity>
                 :
                     null
                 }
               </View>
               <View style={{padding:AppSizes.ResponsiveSize.Padding(2)}} />

               <View styles={{alignItems:'center',width:'100%'}}>
                <Text allowFontScaling={false} style={styles.boxtitle22}>Medication Adherence</Text>
                <View style={styles.border} />
                {medications}
               </View>
               <View style={{padding:AppSizes.ResponsiveSize.Padding(2)}} />

            <View style={{flex:3,marginTop:AppSizes.ResponsiveSize.Padding(4)}}>
             <Text allowFontScaling={false} style={styles.boxtitle22}>Risk Factor</Text>
             <View style={styles.border} />
               <View style={[styles.boxDesignvba2,styles.shadow]}>
                 <View style={{width:'75%'}}>
                   <Text allowFontScaling={false} style={styles.boxtitle12}>Are you a regular steroid user?</Text>
                 </View>
                   <View style={{width:'25%'}}>
                   <FlipToggle
                     value={this.state.steroid_user}
                     buttonOnColor={"#4eb4c4"}
                     buttonOffColor={"#9e9e9e"}
                     sliderOnColor={"#ffffff"}
                     sliderOffColor={"#ffffff"}
                     buttonWidth={AppSizes.screen.width/5}
                     buttonHeight={AppSizes.screen.width/15}
                     buttonRadius={50}
                     onLabel={'Yes'}
                     offLabel={'No'}
                     onToggle={(value) => {
                       this.setState({ oxygen_therapy: value });
                     }}
                     changeToggleStateOnLongPress={false}
                     onToggleLongPress={() => {
                       console.log('Long Press');
                     }}
                   />
                 </View>
               </View>

               <View style={[styles.boxDesignvba2,styles.shadow]}>
                 <View style={{width:'75%'}}>
                   <Text allowFontScaling={false} style={styles.boxtitle12}>Have you been admitted to ER or Hospital in last year?</Text>
                 </View>
                   <View style={{width:'25%'}}>
                   <FlipToggle
                     value={this.state.admitted_hosp}
                     buttonOnColor={"#4eb4c4"}
                     buttonOffColor={"#9e9e9e"}
                     sliderOnColor={"#ffffff"}
                     sliderOffColor={"#ffffff"}
                     buttonWidth={AppSizes.screen.width/5}
                     buttonHeight={AppSizes.screen.width/15}
                     buttonRadius={50}
                     onLabel={'Yes'}
                     offLabel={'No'}
                     onToggle={(value) => {
                       this.setState({ daily_activities: value });
                     }}
                     changeToggleStateOnLongPress={false}
                     onToggleLongPress={() => {
                       console.log('Long Press');
                     }}
                   />
                 </View>
               </View>

               <View style={[styles.boxDesignvba2,styles.shadow]}>
                 <View style={{width:'75%'}}>
                   <Text allowFontScaling={false} style={styles.boxtitle12}>Have you been admitted to ICU for Asthma in last year?</Text>
                 </View>
                   <View style={{width:'25%'}}>
                   <FlipToggle
                     value={this.state.admitted_icu}
                     buttonOnColor={"#4eb4c4"}
                     buttonOffColor={"#9e9e9e"}
                     sliderOnColor={"#ffffff"}
                     sliderOffColor={"#ffffff"}
                     buttonWidth={AppSizes.screen.width/5}
                     buttonHeight={AppSizes.screen.width/15}
                     buttonRadius={50}
                     onLabel={'Yes'}
                     offLabel={'No'}
                     onToggle={(value) => {
                       this.setState({ daily_activities: value });
                     }}
                     changeToggleStateOnLongPress={false}
                     onToggleLongPress={() => {
                       console.log('Long Press');
                     }}
                   />
                 </View>
               </View>

               <View style={[styles.boxDesignvba2,styles.shadow]}>
                 <View style={{width:'75%'}}>
                   <Text allowFontScaling={false} style={styles.boxtitle12}>Do you perform low level of daily acivity?</Text>
                 </View>
                   <View style={{width:'25%'}}>
                   <FlipToggle
                     value={this.state.daily_activities}
                     buttonOnColor={"#4eb4c4"}
                     buttonOffColor={"#9e9e9e"}
                     sliderOnColor={"#ffffff"}
                     sliderOffColor={"#ffffff"}
                     buttonWidth={AppSizes.screen.width/5}
                     buttonHeight={AppSizes.screen.width/15}
                     buttonRadius={50}
                     onLabel={'Yes'}
                     offLabel={'No'}
                     onToggle={(value) => {
                       this.setState({ daily_activities: value });
                     }}
                     changeToggleStateOnLongPress={false}
                     onToggleLongPress={() => {
                       console.log('Long Press');
                     }}
                   />
                 </View>
               </View>

               <View style={[styles.boxDesignvba2,styles.shadow]}>
                 <View style={{width:'75%'}}>
                   <Text allowFontScaling={false} style={styles.boxtitle12}>Are you a smoker?</Text>
                 </View>
                   <View style={{width:'25%'}}>
                   <FlipToggle
                     value={this.state.smoke}
                     buttonOnColor={"#4eb4c4"}
                     buttonOffColor={"#9e9e9e"}
                     sliderOnColor={"#ffffff"}
                     sliderOffColor={"#ffffff"}
                     buttonWidth={AppSizes.screen.width/5}
                     buttonHeight={AppSizes.screen.width/15}
                     buttonRadius={50}
                     onLabel={'Yes'}
                     offLabel={'No'}
                     onToggle={(value) => {
                       this.setState({ smoke: value });
                     }}
                     changeToggleStateOnLongPress={false}
                     onToggleLongPress={() => {
                       console.log('Long Press');
                     }}
                   />
                 </View>
               </View>

               <View style={[styles.boxDesignvba2,styles.shadow]}>
                 <View style={{width:'75%'}}>
                   <Text allowFontScaling={false} style={styles.boxtitle12}>Do you live alone?</Text>
                 </View>
                   <View style={{width:'25%'}}>
                   <FlipToggle
                     value={this.state.live_alone}
                     buttonOnColor={"#4eb4c4"}
                     buttonOffColor={"#9e9e9e"}
                     sliderOnColor={"#ffffff"}
                     sliderOffColor={"#ffffff"}
                     buttonWidth={AppSizes.screen.width/5}
                     buttonHeight={AppSizes.screen.width/15}
                     buttonRadius={50}
                     onLabel={'Yes'}
                     offLabel={'No'}
                     onToggle={(value) => {
                       this.setState({ live_alone: value });
                     }}
                     changeToggleStateOnLongPress={false}
                     onToggleLongPress={() => {
                       console.log('Long Press');
                     }}
                   />
                 </View>
            </View>
            </View>
        {/**  End Risk Factor **/}


</ScrollView>
</View>
<View style={{flex:1,justifyContent:'center'}}>
<TouchableOpacity activeOpacity={.6} onPress={this.chartbtn}>
  <Text allowFontScaling={false} style={styles.smalltext}>Access Measurement History</Text>
</TouchableOpacity>
<TouchableOpacity activeOpacity={.6} onPress={this.vbaUpdate} >
  <CommonButton label='Update'/>
  </TouchableOpacity>
</View>
      </View>
      <Spinner visible={this.state.isVisible}  />

    </View>
    </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
  },

  content: {
      flex: 1,
      backgroundColor: '#ffffff',
  },
tabContainer:{
  flexDirection:'row',
  alignItems:'center',
  paddingBottom:AppSizes.ResponsiveSize.Padding(1),
  height:'11%',
},

boxDesign1:{
  position: 'relative',
  alignItems:'center',
  marginBottom:AppSizes.ResponsiveSize.Padding(4),
  //backgroundColor:'red'
},
heading:{
  backgroundColor:'#8e8e8e',
  paddingLeft:AppSizes.ResponsiveSize.Padding(2),
  paddingRight:AppSizes.ResponsiveSize.Padding(2),
  width:'100%',
  paddingTop:AppSizes.ResponsiveSize.Padding(.50),
  paddingBottom:AppSizes.ResponsiveSize.Padding(.50),
  textAlign:'center',
  alignItems:'center'
},
boxDesign2:{
  padding:AppSizes.ResponsiveSize.Padding(2),
  paddingTop:AppSizes.ResponsiveSize.Padding(5),
  paddingBottom:AppSizes.ResponsiveSize.Padding(0),
  marginBottom:AppSizes.ResponsiveSize.Padding(4),
  position: 'relative',
  flex:1
},
boxDesignvba2:{
  /*paddingTop:AppSizes.ResponsiveSize.Padding(2),
  paddingBottom:AppSizes.ResponsiveSize.Padding(2),*/
  paddingLeft:AppSizes.ResponsiveSize.Padding(4),
  paddingRight:AppSizes.ResponsiveSize.Padding(4),
  marginBottom:AppSizes.ResponsiveSize.Padding(4),
  flexDirection:'row',
  justifyContent:'center',
  alignItems:'center',
  height:AppSizes.screen.height/10
},
shadow:{
 borderWidth:1,
 borderRadius: 2,
 borderColor: '#fff',
 backgroundColor:'#fff',
 borderColor: '#ddd',
 borderBottomWidth: 1,
 shadowColor: '#999',
 shadowOffset: { width: 0, height: 2 },
 shadowOpacity: 0.8,
 shadowRadius: 2,
 elevation: 0,
 //backgroundColor:'red',


},
boxcontainer100:{
  width:'31%',
  height:'90%',
  alignItems:'center',
  padding:AppSizes.ResponsiveSize.Padding(0),
  justifyContent:'center',

},
boxcontainer1:{
  width:'32%',
  height:'100%',
  justifyContent:'center',
  alignItems:'center',

},
boxcontainer22:{
  width:'100%',
  justifyContent:'center',


},
border:{
  width:30,
  backgroundColor:'#999',
  borderBottomWidth:2,
  alignItems:'center'
},
lines:{
color:AppColors.smalltext,
  borderBottomWidth: 1,
  paddingLeft:AppSizes.ResponsiveSize.Padding(3),
  paddingTop:AppSizes.ResponsiveSize.Padding(1),
  paddingBottom:AppSizes.ResponsiveSize.Padding(1),

},
boxicon:{
  flexGrow:1,
  height:null,
  width:null,
  alignItems: 'center',
  justifyContent:'center',
  resizeMode:'contain',
},
boxtitle1:{
  fontSize: AppSizes.ResponsiveSize.Sizes(16),
  marginBottom:AppSizes.ResponsiveSize.Sizes(4),
  fontWeight:'bold',
  color:'#fff',
  shadowOpacity: 0,
  //backgroundColor:'red'
},
boxtitlesmall:{
  fontSize: AppSizes.ResponsiveSize.Sizes(11),
  fontWeight:'bold',
  color:AppColors.primary,
  textAlign:'center',
  shadowOpacity: 0,

},
boxtitle12:{
  fontSize: AppSizes.ResponsiveSize.Sizes(13),
  fontWeight:'600',
  color:AppColors.smalltext,
  shadowOpacity: 0,

},
boxtitle22:{
  fontSize: AppSizes.ResponsiveSize.Sizes(18),
  fontWeight:'600',
  color:AppColors.smalltext,
  shadowOpacity: 0,
  paddingLeft:AppSizes.ResponsiveSize.Padding(3),
  paddingTop:AppSizes.ResponsiveSize.Padding(1),
  paddingBottom:AppSizes.ResponsiveSize.Padding(1),
  textAlign:'center'
},
boxtitle2:{
  fontSize: AppSizes.ResponsiveSize.Sizes(12),
  fontWeight:'400',
  color:AppColors.smalltext,
  shadowOpacity: 0,
  paddingTop:AppSizes.ResponsiveSize.Padding(0),
  paddingBottom:AppSizes.ResponsiveSize.Padding(0),
  backgroundColor:'#fff',
  //borderBottomWidth:2,
  //borderBottomColor:'#000',
},
bordercon:{
  borderBottomWidth:2,
  borderBottomColor:'#000',
  width:'100%',
},
border:{
  borderBottomWidth:2,
  borderBottomColor:'#a9a9a9',
  width:20,
  marginLeft:'auto',
  marginRight:'auto',
  marginBottom:AppSizes.ResponsiveSize.Padding(2)
},

sideicon:{
  position: 'absolute',
  bottom: '-16%',
  right:'-6%',
  width:AppSizes.screen.width/12,
  height:AppSizes.screen.width/12,
  //width:AppSizes.ResponsiveSize.width/2,
  //height:AppSizes.ResponsiveSize.height/2,

},
boxicon:{
  flexGrow:1,
  height:null,
  width:null,
  alignItems: 'center',
  justifyContent:'center',
  resizeMode:'contain',
},
maintwoflex:{
  flex:1,
  paddingTop:AppSizes.ResponsiveSize.Padding(15),
  paddingBottom:AppSizes.ResponsiveSize.Padding(5)
},
mainoneflex:{
  flex:.4,
  paddingTop:AppSizes.ResponsiveSize.Padding(2.5)
},
mainfourflex:{
  flex:1.7,
  paddingTop:AppSizes.ResponsiveSize.Padding(14),
  paddingBottom:AppSizes.ResponsiveSize.Padding(1)
},
inputfield:{
  width:AppSizes.ResponsiveSize.width,
  borderBottomWidth:0.60,
  borderBottomColor:'#999',
  paddingBottom:AppSizes.ResponsiveSize.Padding(.20)
},
inputtext:{
  marginTop:AppSizes.ResponsiveSize.Padding(3),
  fontSize:AppSizes.ResponsiveSize.Sizes(12),
  fontWeight:'600',
  marginBottom:AppSizes.ResponsiveSize.Padding(0)
},
que:{
  backgroundColor:'red',
  flex:1
},
boxnumber:{
  fontSize:AppSizes.ResponsiveSize.Sizes(13),
  color:AppColors.contentColor,
  fontWeight:'bold',
  textAlign:'center',
  //backgroundColor:'red'
  //marginTop:AppSizes.ResponsiveSize.Padding(-1)
},
spaceBox:{
  marginBottom:'25%'
},
boxDesignthree:{
  //height:AppSizes.screen.height/3,
  flexDirection: 'row',
  //flex:1.5,
  height:'100%',
  flexWrap:'wrap',
  alignItems:'center',
  justifyContent: 'space-around',
  paddingBottom:AppSizes.ResponsiveSize.Padding(1),

  //paddingTop:AppSizes.ResponsiveSize.Padding(1),
  //marginBottom:AppSizes.ResponsiveSize.Padding(8),
  justifyContent:'space-between',
  //backgroundColor:'green'
},
/*formbox:{
    height:AppSizes.screen.height/4
},*/
textbox:{
    height:AppSizes.screen.height/3.5
},
mainbtn:{
  justifyContent:'flex-end',

},
smalltext:{
  textAlign:'center',
  fontSize: AppSizes.ResponsiveSize.Sizes(12),
  fontWeight:'bold',
  color:AppColors.primary,
},
/*extrapad:{
  paddingTop:AppSizes.ResponsiveSize.Padding(14)
}*/
bullet:{
  fontSize:10,
  marginRight:AppSizes.ResponsiveSize.Padding(1)
},
bullettext:{
  paddingLeft:AppSizes.ResponsiveSize.Padding(2)
},
thintext:{
  fontSize:AppSizes.ResponsiveSize.Sizes(6)
},
labelInput: {
  color: AppColors.contentColor,
  //fontWeight:'300',
  //fontSize:AppSizes.ResponsiveSize.Sizes(13),
},
input: {
  borderWidth: 0,
  color: AppColors.contentColor,
  fontWeight:'300',
  fontSize:AppSizes.ResponsiveSize.Sizes(15),
},
formInput: {
  borderWidth: 0,
  borderColor: '#333',
},
formContainer:{
  flexDirection: 'row',
},
imageContainer:{
  flex:1.3,
  alignItems: 'flex-end',
  //backgroundColor:'red',
},
imageContainer1:{
  flex:.1,
  alignItems: 'flex-end',
},
editButton:{
  height:20,
  width:20
},
conditiontext:{
  flex: 1,
  paddingTop:(Platform.OS === 'ios') ? 0 :AppSizes.ResponsiveSize.Padding(2),
  fontSize:Platform.OS === 'ios' ? AppSizes.ResponsiveSize.Sizes(14) : AppSizes.ResponsiveSize.Sizes(16),
  color:AppColors.smalltext,
},
icon:{
  width:'10%',
  marginRight:10,
  alignItems:'center',
  justifyContent:'center',
},
icon1:{
  width:'10%',
  alignItems:'center',
  justifyContent:'center',
},
texttbox:{
  width:'75%'
},

cicon:{
  width:20,
  height:20
}
});
