export const jsonData = {

  /*********  Signup Qus For Medical Qus *********/
  "medicalqus" : {
    "qus1" : {
        "question1" : {
          "suboption" : "no",
          "type" : "multiple",
          "options" : {
            "ast" : "Asthma",
            "arf" : "Acid Reflux",
            "ckd" : "Chronic Kidney disease",
            "dbt" : "Diabetes",
            "cad" : "Coronary Artery Disease",
            "chf" : "Congestive Heart Failure",
            "hbp" : "High Blood Pressure",
            "anm":"Anemia",
            "pul" : "Pulmonary Hypertension",
            },
          "question" : "Please select all the conditions you have been diagnosed with?"
        },
        "question2" : {
          "suboption" : "no",
          "type" : "single",
          "options" : {
              "gold_1" : "Stage 1- Mild(FEV1>=80%normal)",
              "gold_2" : "Stage 2-Moderate(FEV1=50-79% normal)",
              "gold_3" : "Stage 3-Severe(FEV1=30-49% normal)",
              "gold_4" : "Stage 4-Very Severe(FEV1<30% normal)",
              "gold_nan" : "I don’t know"
            },
          "question" : "Please select your COPD gold stage? "
        },
        "question3" : {
          "suboption" : "no",
          "type" : "single",
          "options" : {
              "base_dyspnea_1" : "Breathless only with demanding exercise",
              "base_dyspnea_2" : "Breathless when hurrying on flat ground",
              "base_dyspnea_3" : "Walk slower than people of my same age",
              "base_dyspnea_4" : "Need to stop to breathe after walking about 100 yards",
              "base_dyspnea_5" : "Too breathless to leave the house and dress myself"
            },
          "question" : "Which statement best describes your usual breathlessness during activity? "
        },
        "question4" : {
          "suboption" : "risk_factor",
          "type" : "multiple",
          "options" : {
              "ltou" : "I am on Oxygen therapy",
              "nhp" : "I need help with daily activities",
              "la" : "I live alone",
              "smoker" : "I smoke",
              "hcopd" : "I have been hospitalized due to COPD in the  last 12 months",
            },
          "question" : "Please select which of the following statements apply to you:"
        },
        "question5" : {
          "suboption" : "no",
          "type" : "single",
          "options" : {
            "smoking_yes" : "Yes",
            "smoking_no" : "No",
          },
          "question" : "Have you been trying to quit smoking? "
        },
        "question6" : {
          "suboption" : "no",
          "type" : "single",
          "options" : {

          },
          "question" : "On average how many cigarettes do you smoke daily? "
        },
        "question7" : {
          "suboption" : "no",
            "type" : "single",
          "options" : {
            "option1" : "1",
            "option2" : "2",
            "option3" : "3",
            "option4" : "4",
            "option5" : "5",
            "option6" : "6",
            "option7" : "7",
            "option8" : "8",
            "option9" : "9",
            "option10" : "10",
            "option11" : "11",
            "option12" : "12",
          },
          "question" : "How many months ago was your latest hospitalization due to copd?"
        },
      },

    "qus2" : {
      "question1" : {
        "suboption" : "no",
        "type" : "multiple",
        "options" : {
          "CND_GERD" : "Acid Reflux",
          "CND_CKD" : "Chronic Kidney disease",
          "CND_PL_COPD":"Chronic Obstructive Pulmonary Disease (COPD)",
          "CND_DIA" : "Diabetes",
          "CND_CD_CAD" : "Coronary Artery Disease",
          "CND_CD_CHF" : "Congestive Heart Failure",
          "CND_HYP" : "High Blood Pressure",
          "CBY_ANE":"Anemia",
          },
        "question" : "Please select all the conditions you have been diagnosed with?"
      },
      "question2" : {
        "suboption" : "no",
          "type" : "multiple",
        "options" : {
          "CND_STEROIDS":"Regular Oral Steroid User (3 or more courses of oral steroids in the last year)",
          "CBY_DOM_LA":"Live alone",
          "OUTP_HOSP":"Admitted to ER or Hospital in the last year",
          "OUTP_ICU":"Admitted to ICU for Asthma in the last year",
          "CBY_DOM_ASS":"Low level of daily activity",
          "PRF_BH_SMK":"I smoke"
        },
        "question" : "Please select all statements that apply to you:"
      },
      "question3" : {
        "suboption" : "no",
          "type" : "multiple",
        "options" : {
          "inhaler":"Albuterol Inhaler or Rescue Nebulizer",
          "low":"Low-dose Inhaled steroid",
          "medium":"Medium-dose inhaled steroid",
          "high":"High-dose inhaled steroid",
          "longg":"Long Acting Bronchodilator",
          "combined":"Combined Steroid + Bronchodilator",
          "injectible":"Injectible Medication/Biologic"
        },
        "question" : "Please select your current course of medication:"
      },
      "question4" : {
        "suboption" : "no",
          "type" : "single",
        "options" : {
          "option1":"Yes",
          "option2":"No",
        },
        "question" : "Are you on a 10-30 mg daily dose of Prednisone? "
      },
      "question5" : {
        "suboption" : "no",
          "type" : "single",
        "options" : {

        },
        "question" : "In an average week, how often do you use your rescue inhaler or nebulizer (in times/day):"
      },
    },
    "qus3" : {
      "question1" : {
        "suboption" : "no",
          "type" : "single",
        "options" : {
          "asthma" : "Asthma",
          "copd" : "COPD",
        },
        "question" : "Select Your Iterex Disease Diagnosis? (You can select only one)"
      },
    }
  },
  /*** Am I OK QUs ******/
  "amiokqus" : {
    "qus1" : {
      "question1" : {
        "modal_title":"This is a series of questions for evaluating the severity of your vital signs and symptoms and recommending the most appropriate course of action.\n\nSelect the most applicable choice",
        "suboption" : "no",
        "type" : "single",
        "options" : {
          "rws_1" : "No",
          "rws_2" : "Yes, for the last 24 hours",
          "rws_3" : "Yes, for 1 to 3 days",
          "rws_4" : "Yes, for more than 3 days",
        },
        "question" : "Have you had any recent worsening in any breathing symptoms such as cough, shortness of breath or waking up at night?"
      },
      "question2" : {
        "modal_title":"This is a series of questions for evaluating the severity of your vital signs and symptoms and recommending the most appropriate course of action.\n\nSit down and relax for at least two minutes before taking the measurements. If you regularly on oxygen, make sure you are wearing your oxygen mask and that the amount of oxygen flowing is set to the amount prescribed by your doctor. Use your pulse oximeter device following its instructions and measure your heart rate and oxygen saturation. Select the values that correspond to those displayed in your device. Use your thermometer to measure your temperature and select its value from the scroll wheel. Click “Next” when you are done!",
        "suboption" : "no",
        "type" : "multiple",
        "options" : {
            "option1" : "Oxygen Saturation",
            "option2" : "Heart Rate",
            "option3" : "Temperature",
            "option4" : "FEV1",
          },
        "question" : "While at rest please measure and enter the following"
      },
      "question3" : {
        "modal_title":"This is a series of questions for evaluating the severity of your vital signs and symptoms and recommending the most appropriate course of action.\n\nSelect all that apply to you.",
        "suboption" : "no",
        "type" : "multiple",
        "options" : {
            "shofb" : "Short of breath",
            "cough" : "Cough",
            "wheez" : "Wheezing",
            "mucus_discoloration" : "Mucus/Phlegm discoloration",
            "mucus_volume" : "Mucus/Phlegm volume",
            "respiratory_symptoms" : "Waking up at night from breathing  symptoms",
            "runny_nose" : "Runny nose, sore throat or cold symptoms",
          },
        "question" : "Please select which of the following are worse than usual"
      },
      "question4" : {
        "modal_title":"This is a series of questions for evaluating the severity of your vital signs and symptoms and recommending the most appropriate course of action.\n\nSelect the most applicable choice.",
        "suboption" : "no",
        "type" : "single",
        "options" : {
            "shofb_1" : "Yes",
            "shofb_0" : "No",
          },
        "question" : "Do you feel short of breath while at REST? "
      },
      "question5" : {
        "modal_title":"This is a series of questions for evaluating the severity of your vital signs and symptoms and recommending the most appropriate course of action.\n\nSelect the most applicable choice.",
        "suboption" : "no",
        "type" : "single",
        "options" : {
          "cur_dyspnea_1" : "Breathless only with demanding exercise",
          "cur_dyspnea_2" : "Breathless when hurrying on flat ground",
          "cur_dyspnea_3" : "Walk slower than people of my same age",
          "cur_dyspnea_4" : "Need to stop to breathe after walking about 100 yards",
          "cur_dyspnea_5" : "Too breathless to leave the house and dress myself",
        },
        "question" : "Which statement best describes your breathlessness during activity?"
      },
    }
  },
  /*** AmIOk Asthma QUs ******/
  "amiokasthmaqus" : {
    "qus1" : {
      "question1" : {
        "modal_title":"This is a series of questions for evaluating the severity of your vital signs and symptoms and recommending the most appropriate course of action.\n\nSelect the most applicable choice",
        "suboption" : "no",
        "type" : "single",
        "options" : {
          "SYM_WORSE_N" : "No",
          "SYM_WORSE_LOW" : "Yes, I have been worse for 0-7 days",
          "SYM_WORSE_MEDIUM" : "Yes, I have been worse for 1-4 weeks",
          "SYM_WORSE_HIGH" : "Yes, I have been worse for more than 4 weeks",
        },
        "question" : "Have you had any recent worsening in any breathing symptoms such as cough, shortness of breath or waking up at night?"
      },
      "question2" : {
        "modal_title":"This is a series of questions for evaluating the severity of your vital signs and symptoms and recommending the most appropriate course of action.\n\nSit down and relax for at least two minutes before taking the measurements. If you regularly on oxygen, make sure you are wearing your oxygen mask and that the amount of oxygen flowing is set to the amount prescribed by your doctor. Use your pulse oximeter device following its instructions and measure your heart rate and oxygen saturation. Select the values that correspond to those displayed in your device. Use your thermometer to measure your temperature and select its value from the scroll wheel. Click “Next” when you are done!",
        "suboption" : "no",
        "type" : "multiple",
        "options" : {
            "option1" : "Peak Expiratory Flow (ranges: 0 - 1000 : ind)",
            "option2" : "Heart Rate (range: 30 - 150 ; avg: 80)",
            "option3" : "Temperature (range: 97 - 106 ; avg: 98.6)",
            "option4" : "Respiratory Rate  (0 - 50 ; 18) (Do not show now)",
          },
        "question" : "While at rest please measure and enter the following"
      },
      "question3" : {
        "modal_title":"This is a series of questions for evaluating the severity of your vital signs and symptoms and recommending the most appropriate course of action.\n\nSelect all that apply to you.",
        "suboption" : "no",
        "type" : "multiple",
        "options" : {
            "shofb" : "Short of breath",
            "cough" : "Cough or Wheezing",
            "SYM_ALLERGEN" : "Allergy symptoms",
            "runny_nose" : "Runny nose, sore throat or cold symptoms",
            "rescue_inhaler_usage":"Rescue inhaler/nebulizer usage",
            "wakingup_from_breating_symptoms":"Waking up at night from breathing symptoms",
            "difficulty_performing_usual_activities":"Difficulty performing usual activities"
          },
        "question" : "Which of these symptoms are worse than usual?"
      },

      "question4" : {
        "modal_title":"This is a series of questions for evaluating the severity of your vital signs and symptoms and recommending the most appropriate course of action.\n\nSelect the most applicable choice.",
        "suboption" : "no",
        "type" : "single",
        "options" : {
          "SYM_RIHMORE_N":"No",
          "SYM_RIHMORE_LOW":"Once Per Day Or Less",
          "SYM_RIHMORE_MEDIUM":"2-4 Times Daily",
          "SYM_RIHMORE_HIGH":"More than 4 times per day",
          },
        "question" : "How often have you used your rescue inhaler or nebulizer (in times/day): "
      },

      "question5" : {
        "modal_title":"This is a series of questions for evaluating the severity of your vital signs and symptoms and recommending the most appropriate course of action.\n\nSelect the most applicable choice.",
        "suboption" : "no",
        "type" : "single",
        "options" : {
            "MED_COMP_25" : "Less than 25% of the time",
            "MED_COMP_50" : "Between 25% and 50% of the time",
            "MED_COMP_75" : "Between 50% and 75% of the time",
            "MED_COMP_100" : "Greater than 75% of the time",
          },
        "question" : "Have you been regularly taking your prescribed medications?"
      },
      "question6":{
        "modal_title":"This is a series of questions for evaluating the severity of your vital signs and symptoms and recommending the most appropriate course of action.\n\nSelect the most applicable choice.",
        "suboption" : "no",
        "type" : "single",
        "options" : {
            "SYM_CGH_N" : "No",
            "SYM_CGH_LESS" : "Less Than Usual",
            "SYM_CGH_SAME" : "Same As Usual",
            "SYM_CGH_MORE" : "More Than Usual"
          },
        "question" : "How often do you Cough?"
      },
      "question7":{
        "modal_title":"This is a series of questions for evaluating the severity of your vital signs and symptoms and recommending the most appropriate course of action.\n\nSelect the most applicable choice.",
        "suboption" : "no",
        "type" : "single",
        "options" : {
            "SYM_SOA_N" : "No",
            "SYM_SOA_LESS" : "Less Than Usual",
            "SYM_SOA_SAME" : "Same As Usual",
            "SYM_SOA_MORE" : "More Than Usual"
          },
        "question" : "How often do you feel short of breath?"
      },
      "question8":{
        "modal_title":"This is a series of questions for evaluating the severity of your vital signs and symptoms and recommending the most appropriate course of action.\n\nSelect the most applicable choice.",
        "suboption" : "no",
        "type" : "single",
        "options" : {

          },
        "question" : "Please Measure & Enter Your Temperature"
      },
      "question9":{
        "modal_title":"This is a series of questions for evaluating the severity of your vital signs and symptoms and recommending the most appropriate course of action.\n\nSelect the most applicable choice.",
        "suboption" : "no",
        "type" : "single",
        "options" : {
          "SYM_WAKE_N":"No",
          "SYM_WAKE_LOW":"Once per week or less",
          "SYM_WAKE_MEDIUM":"1-3 times per week.",
          "SYM_WAKE_HIGH":"More than 4 times per week",
          },
        "question" : "How often are your breathing symptoms waking you at night? "
      },
      "question10":{
        "modal_title":"This is a series of questions for evaluating the severity of your vital signs and symptoms and recommending the most appropriate course of action.\n\nSelect the most applicable choice.",
        "suboption" : "no",
        "type" : "single",
        "options" :{
          "SYM_ADL_N":"No",
          "SYM_ADL_LOW":"Once Per Day Or Less",
          "SYM_ADL_MEDIUM":"2-4 times per day",
          "SYM_ADL_HIGH":"More than 4 times per day",
        },
        "question":"How often are your breathing symptoms interfering with your daily activities?"
      }
    }
  },
  /*** HADS QUs ******/
  "hadsqus" : {
    "qus1" : {
      "question1" : {
        "suboption" : "no",
        "type" : "single",
        "options" : {
          "option_0" : "Not at All",
          "option_1" : "From time to time, occasionally",
          "option_2" : "A lot of the time",
          "option_3" : "Most of the time ",
        },
        "question" : "I feel tense or 'wound up':"
      },
      "question2" : {
        "suboption" : "no",
        "type" : "single",
        "options" : {
          "option_0" : "Not at All",
          "option_1" : "A little, but it doesn't worry me",
          "option_2" : "Yes, but not too badly",
          "option_3" : "Very definitely and quite badly",
          },
        "question" : "I get a sort of frightened feeling as if something awful is about to happen:"
      },
      "question3" : {
        "suboption" : "no",
        "type" : "single",
        "options" : {
            "option_0" : "Only occasionally",
            "option_1" : "From time to time, but not too often",
            "option_2" : "A lot of time",
            "option_3" : "A great deal of the time",
          },
        "question" : "Worrying thoughts go through my mind:"
      },
      "question4" : {
        "suboption" : "risk_factor",
        "type" : "single",
        "options" : {
            "option_0" : "Definitely",
            "option_1" : "Usually",
            "option_2" : "Not often",
            "option_3" : "Not at all",
          },
        "question" : "I can sit at ease and feel relaxed:"
      },
      "question5" : {
        "suboption" : "no",
        "type" : "single",
        "options" : {
          "option_0" : "Not at all",
          "option_1" : "Occasionally",
          "option_2" : "Quite often",
          "option_3" : "Very",
        },
        "question" : "I get a sort of frightened feeling like ‘butterflies’ in the stomach:"
      },

      "question6" : {
        "suboption" : "no",
        "type" : "single",
        "options" : {
          "option_0" : "Not at all",
          "option_1" : "Not very much",
          "option_2" : "Quite a lot",
          "option_3" : "Very much indeed",
        },
        "question" : "I feel restless as I have to be on the move:"
      },
      "question7" : {
        "suboption" : "no",
        "type" : "single",
        "options" : {
          "option_0" : "Not at all",
          "option_1" : "Not very often",
          "option_2" : "Quite often",
          "option_3" : "Very often indeed",
        },
        "question" : "I get sudden feelings of panic:"
      },
    }
  },
  /*** COPD QUs ******/
  "copdqus" : {
    "qus1" : {
      "question1" : {
        "title":"On average, during the past week, how often did you feel:",
        "suboption" : "no",
        "type" : "single",
        "options" : {
          "option_0" : "Never",
          "option_1" : "Hardly ever",
          "option_2" : "A few times",
          "option_3" : "Several times",
          "option_4" : "Many times",
          "option_5" : "A great many times",
          "option_6":"Almost all the time",
        },
        "question" : "Short of breath at rest?"
      },
      "question2" : {
        "title":"On average, during the past week, how often did you feel:",
        "suboption" : "no",
        "type" : "single",
        "options" : {
          "option_0" : "Never",
          "option_1" : "Hardly ever",
          "option_2" : "A few times",
          "option_3" : "Several times",
          "option_4" : "Many times",
          "option_5" : "A great many times",
          "option_6" : "Almost all the time",
        },
        "question"  : "Short of breath doing physical activity?"
    },
    "question3" : {
      "title":"On average, during the past week, how often did you feel:",
      "suboption" : "no",
      "type" : "single",
      "options" : {
        "option_0" : "Never",
        "option_1" : "Hardly ever",
        "option_2" : "A few times",
        "option_3" : "Several times",
        "option_4" : "Many times",
        "option_5" : "A great many times",
        "option_6" : "Almost all the time",
      },
      "question"  : "Concerned about getting a cold, or your breathing getting worse?"
   },
    "question4" : {
      "title":"On average, during the past week, how often did you feel:",
      "suboption" : "no",
      "type" : "single",
      "options" : {
        "option_0" : "Never",
        "option_1" : "Hardly ever",
        "option_2" : "A few times",
        "option_3" : "Several times",
        "option_4" : "Many times",
        "option_5" : "A great many times",
        "option_6" : "Almost all the time",
      },
      "question"  : "Depressed (down) because of your breathing problems?"
  },


      "question5" : {
        "title":"In general, during the past week, how much of the time:",
        "suboption" : "no",
        "type" : "single",
        "options" : {
          "option_0" : "Never",
          "option_1" : "Hardly ever",
          "option_2" : "A few times",
          "option_3" : "Several times",
          "option_4" : "Many times",
          "option_5" : "A great many times",
          "option_6":"Almost all the time",
        },
        "question" : "Did you cough?"
      },
      "question6" : {
        "title":"In general, during the past week, how much of the time:",
        "suboption" : "no",
        "type" : "single",
        "options" : {
          "option_0" : "Never",
          "option_1" : "Hardly ever",
          "option_2" : "A few times",
          "option_3" : "Several times",
          "option_4" : "Many times",
          "option_5" : "A great many times",
          "option_6" : "Almost all the time",
        },
        "question"  : "Did you produce phlegm?"
    },
    "question7" : {
      "title":"On average, during the past week, how limited were you in these activities because of your breathing problems:",
      "suboption" : "no",
      "type" : "single",
      "options" : {
        "option_0" : "Not limited at all",
        "option_1" : "Very slightly limited",
        "option_2" : "Slightly limited",
        "option_3" : "Moderately limited",
        "option_4" : "Very limited",
        "option_5" : "Extremely limited ",
        "option_6" : "Totally limited / or unable to do",
      },
      "question" : "Strenuous physical activities (such as climbing stairs, hurrying, doing sports)?"
    },
    "question8" : {
      "title":"On average, during the past week, how limited were you in these activities because of your breathing problems:",
      "suboption" : "no",
      "type" : "single",
      "options" : {
        "option_0" : "Not limited at all",
        "option_1" : "Very slightly limited",
        "option_2" : "Slightly limited",
        "option_3" : "Moderately limited",
        "option_4" : "Very limited",
        "option_5" : "Extremely limited ",
        "option_6" : "Totally limited / or unable to do",
        },
      "question"  : "Moderate physical activities (such as walking, housework, carrying things)?"
    },
    "question9" : {
      "title":"On average, during the past week, how limited were you in these activities because of your breathing problems:",
      "suboption" : "no",
      "type" : "single",
      "options" : {
        "option_0" : "Not limited at all",
        "option_1" : "Very slightly limited",
        "option_2" : "Slightly limited",
        "option_3" : "Moderately limited",
        "option_4" : "Very limited",
        "option_5" : "Extremely limited ",
        "option_6" : "Totally limited / or unable to do",
        },
      "question"  : "Social activities (such as talking, being with children, visiting friends / relatives)?"
      },
    "question10" : {
      "title":"On average, during the past week, how limited were you in these activities because of your breathing problems:",
      "suboption" : "no",
      "type" : "single",
      "options" : {
        "option_0" : "Not limited at all",
        "option_1" : "Very slightly limited",
        "option_2" : "Slightly limited",
        "option_3" : "Moderately limited",
        "option_4" : "Very limited",
        "option_5" : "Extremely limited ",
        "option_6" : "Totally limited / or unable to do",
        },
      "question"  : "Daily activities at home (such as dressing, washing yourself)?"
    },

    },
  },
  /*** ACT QUs ******/
  "actqus" : {
    "qus1" : {
      "question1" : {
        "suboption" : "no",
        "type" : "single",
        "options" : {
          "option_1" : "All of the time",
          "option_2" : "Most of the time",
          "option_3" : "Some of the time",
          "option_4" : "A little of the time",
          "option_5" : "None of the time",
        },
        "question" : "In the past 4 weeks, how much of the time did your asthma keep you from getting as much done at work, school or at home?"
      },
      "question2" : {
        "suboption" : "no",
        "type" : "single",
        "options" : {
          "option_1" : "More than once a day",
          "option_2" : "Once a day",
          "option_3" : "3 to 6 times a week",
          "option_4" : "Once or twice a week",
          "option_5" : "Not at all",
          },
        "question" : "During the past 4 weeks, how often have you had shortness of breath?"
      },
      "question3" : {
        "suboption" : "no",
        "type" : "single",
        "options" : {
            "option_1" : "4 or more nights a week",
            "option_2" : "2 to 3 nights a week",
            "option_3" : "Once a week",
            "option_4" : "Once or twice",
            "option_5" : "Not at all",
          },
        "question" : "During the past 4 weeks, how often did your asthma symptoms (wheezing, coughing, shortness of breath, chest tightness or pain) wake you up at night or earlier than usual in the morning?"
      },
      "question4" : {
        "suboption" : "risk_factor",
        "type" : "single",
        "options" : {
            "option_1" : "3 or more times a day",
            "option_2" : "1 or 2 times per day",
            "option_3" : "2 or 3 times per week",
            "option_4" : "Once a week or less",
            "option_5" : "Not at all",
          },
        "question" : "During the past 4 weeks, how often have you used your rescue inhaler or nebulizer medication (such as albuterol)?"
      },
      "question5" : {
        "suboption" : "no",
        "type" : "single",
        "options" : {
          "option_1" : "Not controlled at all",
          "option_2" : "Poorly controlled",
          "option_3" : "Somewhat controlled ",
          "option_4" : "Well controlled",
          "option_5" : "Completely controlled",
        },
        "question" : "How would you rate your asthma control during the past 4 weeks?"
      },
    }
  },
  /**  Info qus**/
  "infoqus" : {
      "qus1" : {
          "question1" : {
            "suboption" : "no",
              "type" : "single",
            "options" : {
              "option1" : "Yes, I will get immediate medical attention",
              "option2" : "No, I am just testing the app",
              "option3" : "No, I made a mistake entering my information",
              "option4" : "No, I don't agree with the recommendation",
              "option5" : "Other",
              "option6" : "Skip",
            },
            "question" : "Do you plan to follow the 'Am I Ok' recommendation?"
          },
          "question2" : {
            "suboption" : "no",
            "type" : "single",
            "options" : {
              "option1" : "Yes, I will call my doctor",
              "option2" : "No, I am just testing the app",
              "option3" : "No, I made a mistake entering my information",
              "option4" : "No, I don't agree with the recommendation",
              "option5" : "Other",
              "option6" : "Skip",
            },
            "question" : "Do you plan to follow the 'Am I Ok' recommendation?"
          },
          "question3" : {
            "suboption" : "no",
            "type" : "single",
            "options" : {
              "option1" : "Yes, I don't feel that I need medical help at this time",
              "option2" : "No, I am experiencing symptoms that I am Concerned about",
              "option3" : "No, I feel like I should call my doctor",
              "option4" : "No, I feel like I need immediate medical care",
              "option5" : "Other",
              "option6" : "Skip",
            },
            "question" : "Do you agree with the 'Am I Ok' recommendation?"
          },

        }
      },
}
