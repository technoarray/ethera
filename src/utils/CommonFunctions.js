export function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
};

export function validatePassword(pass){
  var reg = /[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/;
  return reg.test(pass);
}

export function validatePasswordLength(pass){
  var reg = /^.{10,}$/;
  return reg.test(pass);
}

export function validatePassworddigit(pass){
  var reg = /\d/;
  return reg.test(pass);
}
export function validatePasswordspecial(pass){
  var reg = /[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/;
  return reg.test(pass);
}
export function validatePasswordlower(pass){
  var reg = /[a-z]/g;
  return reg.test(pass);
}
export function validatePasswordupper(pass){
  var reg = /[A-Z]/g;
  return reg.test(pass);
}
export function validateRemovearr(arr,errormsg){
  var index = arr.indexOf(errormsg);
  if (index > -1) {
    if(arr.length==1){
      arr=[]
    }
    else{
      arr.splice(index, 1);
    }
  }
  return arr;
}

export function getAge(DOB) {
    var today = new Date();
    var birthDate = new Date(DOB);
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age--;
    }
    return age;
}

export function toFeet(n) {
  console.log(n)
      var realFeet = (n / 12);
      var feet = Math.floor(realFeet);
      var inches = (n % 12);
      var inches = Math.floor(inches);
      return feet + "ft " + inches + 'in';
    }

export function getAdjustedFontSize(size,deviceHeight){
  if(deviceHeight===568) {size= size }
   else if(deviceHeight===667) {size= size*1.17 }
   else if(deviceHeight===736) {size= size*1.29 }
   else if(deviceHeight===1024){size= size*1.8 }
  return size ;
}


export function alertMessage(alertMessage) {
    Alert.alert(
        'eThera',
        alertMessage, [
            { text: 'OK', onPress: () => console.log('OK Pressed') },
        ], { cancelable: false }
    )
}
